package info.batcloud.fanli.background.task.scheduler;

import info.batcloud.fanli.core.domain.utam.req.TbkOrderFetchRequest;
import info.batcloud.fanli.core.enums.TkOrderPayStatus;
import info.batcloud.fanli.core.enums.TkOrderQueryType;
import info.batcloud.fanli.core.enums.TkOrderScene;
import info.batcloud.fanli.core.service.TaobaoUnionService;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Date;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Component
public class TaobaoUnionScheduler {

    @Inject
    private TaobaoUnionService taobaoUnionService;

    private static final Logger logger = LoggerFactory.getLogger(TaobaoUnionService.class);

    private ThreadPoolExecutor threadPoolExecutor;

    public TaobaoUnionScheduler() {
        threadPoolExecutor = new ThreadPoolExecutor(3, 10, 5, TimeUnit.MINUTES, new LinkedBlockingQueue<>());
    }

    @Scheduled(cron = "${cron.taobaoUnion.commissionOrderFetch}")
    public void commissionOrderFetch() {
        logger.info("同步淘宝订单，按照创建时间");
        threadPoolExecutor.execute(() -> {
            Date endTime = new Date();
            Date now = new Date();
            Date startTime = DateUtils.addMinutes(now, -5);
            TbkOrderFetchRequest request = new TbkOrderFetchRequest();
            request.setQueryType(TkOrderQueryType.CREATE_TIME);
            request.setScene(TkOrderScene.MEMBER);
            request.setEndTime(endTime);

            //抓取5分钟之前的订单
            request.setStartTime(startTime);
            taobaoUnionService.fetchOrder(request);

            TbkOrderFetchRequest request1 = new TbkOrderFetchRequest();
            request1.setQueryType(TkOrderQueryType.CREATE_TIME);
            request1.setScene(TkOrderScene.CHANNEL);
            request1.setEndTime(new Date());
            //抓取5分钟之前的订单
            request1.setStartTime(startTime);
            taobaoUnionService.fetchOrder(request1);

            TbkOrderFetchRequest request2 = new TbkOrderFetchRequest();
            request2.setQueryType(TkOrderQueryType.CREATE_TIME);
            request.setScene(TkOrderScene.COMMON);
            request2.setEndTime(new Date());
            //抓取5分钟之前的订单
            request2.setStartTime(startTime);
            taobaoUnionService.fetchOrder(request2);
        });
        logger.info("淘宝订单同步完成，按照创建时间");
    }

    @Scheduled(cron = "${cron.taobaoUnion.commissionOrderFinishFetch}")
    public void commissionOrderFinishFetch() {
        logger.info("同步淘宝订单，按照结算时间");
        threadPoolExecutor.execute(() -> {
            Date now = new Date();
            Date startTime = DateUtils.addMinutes(now, -5);
            TbkOrderFetchRequest request = new TbkOrderFetchRequest();
            request.setQueryType(TkOrderQueryType.SETTLE_TIME);
            request.setPayStatus(TkOrderPayStatus.SETTLED);
            request.setScene(TkOrderScene.MEMBER);
            request.setEndTime(now);
            //抓取5分钟之前的订单
            request.setStartTime(startTime);
            taobaoUnionService.fetchOrder(request);

            TbkOrderFetchRequest request1 = new TbkOrderFetchRequest();
            request1.setQueryType(TkOrderQueryType.SETTLE_TIME);
            request1.setPayStatus(TkOrderPayStatus.SETTLED);
            request1.setScene(TkOrderScene.CHANNEL);
            request1.setEndTime(now);
            //抓取5分钟之前的订单
            request1.setStartTime(startTime);
            taobaoUnionService.fetchOrder(request1);

            TbkOrderFetchRequest request2 = new TbkOrderFetchRequest();
            request2.setQueryType(TkOrderQueryType.SETTLE_TIME);
            request2.setPayStatus(TkOrderPayStatus.SETTLED);
            request.setScene(TkOrderScene.COMMON);
            request2.setEndTime(now);
            //抓取5分钟之前的订单
            request2.setStartTime(startTime);
            taobaoUnionService.fetchOrder(request2);
        });
        logger.info("淘宝订单同步完成，按照结算时间");
    }

}
