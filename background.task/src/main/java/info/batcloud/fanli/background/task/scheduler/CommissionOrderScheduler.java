package info.batcloud.fanli.background.task.scheduler;

import info.batcloud.fanli.core.service.CommissionOrderService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class CommissionOrderScheduler {

    @Inject
    private CommissionOrderService commissionOrderService;

    //每个5分钟结算一次
    @Scheduled(cron = "${cron.commissionOrderSettle}")
    public void commissionOrderSettle() {
        commissionOrderService.settle();
    }

}
