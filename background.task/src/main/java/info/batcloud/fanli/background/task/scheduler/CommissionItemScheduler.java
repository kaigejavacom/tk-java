package info.batcloud.fanli.background.task.scheduler;

import info.batcloud.fanli.core.service.CommissionItemService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class CommissionItemScheduler {

    @Inject
    private CommissionItemService commissionItemService;

    /**
     * 每天3点钟清理
     * */
    @Scheduled(cron = "0 0 3 * * ?")
    public void clean() {
        commissionItemService.clean();
    }

}
