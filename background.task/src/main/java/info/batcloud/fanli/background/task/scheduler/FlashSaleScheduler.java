package info.batcloud.fanli.background.task.scheduler;

import info.batcloud.fanli.core.service.FlashSaleRemindService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.inject.Inject;


@Component
public class FlashSaleScheduler {

    @Inject
    private FlashSaleRemindService flashSaleRemindService;


    @Scheduled(cron = "0 */5 * * * ?")
    public void detectRemind() {
        flashSaleRemindService.scheduleRemind();
    }

}
