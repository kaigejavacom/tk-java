package info.batcloud.laxiaoke.open.domain.jd.union;

import java.util.List;

public class GoodsCategoryResult {

    private List<GoodsCategory> data;

    public List<GoodsCategory> getData() {
        return data;
    }

    public void setData(List<GoodsCategory> data) {
        this.data = data;
    }

}
