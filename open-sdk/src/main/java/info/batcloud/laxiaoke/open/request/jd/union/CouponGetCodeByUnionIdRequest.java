package info.batcloud.laxiaoke.open.request.jd.union;

import info.batcloud.laxiaoke.open.AbstractLxkRequest;
import info.batcloud.laxiaoke.open.response.jd.union.CouponGetCodeByUnionIdResponse;

public class CouponGetCodeByUnionIdRequest extends AbstractLxkRequest<CouponGetCodeByUnionIdResponse> {

    private String couponUrl;
    private String materialIds;
    private long unionId;
    private long positionId;
    private String pid;

    public String getCouponUrl() {
        return couponUrl;
    }

    public void setCouponUrl(String couponUrl) {
        this.couponUrl = couponUrl;
    }

    public String getMaterialIds() {
        return materialIds;
    }

    public void setMaterialIds(String materialIds) {
        this.materialIds = materialIds;
    }

    public long getUnionId() {
        return unionId;
    }

    public void setUnionId(long unionId) {
        this.unionId = unionId;
    }

    public long getPositionId() {
        return positionId;
    }

    public void setPositionId(long positionId) {
        this.positionId = positionId;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    @Override
    public String getUri() {
        return "/jd-union-api/coupon-get-code-by-unionid";
    }

    @Override
    public Class<CouponGetCodeByUnionIdResponse> getResponseClass() {
        return CouponGetCodeByUnionIdResponse.class;
    }
}
