package info.batcloud.laxiaoke.open.domain.jd.union;

public class ServicePromotionGoodsInfo {

    private long cid;
    private long cid2;
    private long cid3;
    private float commissionRatioPc;
    private float commissionRatioWl;
    private long endDate;
    private String goodsName;
    private String imgUrl;
    private long inOrderCount;
    private int isFreeFreightRisk;
    private int isFreeShipping;
    private int isJdSale;
    private int isSeckill;
    private String materialUrl;
    private long shopId;
    private long skuId;
    private long startDate;
    private float unitPrice;
    private float wlUnitPrice;

    public long getCid() {
        return cid;
    }

    public void setCid(long cid) {
        this.cid = cid;
    }

    public long getCid2() {
        return cid2;
    }

    public void setCid2(long cid2) {
        this.cid2 = cid2;
    }

    public long getCid3() {
        return cid3;
    }

    public void setCid3(long cid3) {
        this.cid3 = cid3;
    }

    public float getCommissionRatioPc() {
        return commissionRatioPc;
    }

    public void setCommissionRatioPc(float commissionRatioPc) {
        this.commissionRatioPc = commissionRatioPc;
    }

    public float getCommissionRatioWl() {
        return commissionRatioWl;
    }

    public void setCommissionRatioWl(float commissionRatioWl) {
        this.commissionRatioWl = commissionRatioWl;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public long getInOrderCount() {
        return inOrderCount;
    }

    public void setInOrderCount(long inOrderCount) {
        this.inOrderCount = inOrderCount;
    }

    public int getIsFreeFreightRisk() {
        return isFreeFreightRisk;
    }

    public void setIsFreeFreightRisk(int isFreeFreightRisk) {
        this.isFreeFreightRisk = isFreeFreightRisk;
    }

    public int getIsFreeShipping() {
        return isFreeShipping;
    }

    public void setIsFreeShipping(int isFreeShipping) {
        this.isFreeShipping = isFreeShipping;
    }

    public int getIsJdSale() {
        return isJdSale;
    }

    public void setIsJdSale(int isJdSale) {
        this.isJdSale = isJdSale;
    }

    public int getIsSeckill() {
        return isSeckill;
    }

    public void setIsSeckill(int isSeckill) {
        this.isSeckill = isSeckill;
    }

    public String getMaterialUrl() {
        return materialUrl;
    }

    public void setMaterialUrl(String materialUrl) {
        this.materialUrl = materialUrl;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public long getSkuId() {
        return skuId;
    }

    public void setSkuId(long skuId) {
        this.skuId = skuId;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public float getWlUnitPrice() {
        return wlUnitPrice;
    }

    public void setWlUnitPrice(float wlUnitPrice) {
        this.wlUnitPrice = wlUnitPrice;
    }
}
