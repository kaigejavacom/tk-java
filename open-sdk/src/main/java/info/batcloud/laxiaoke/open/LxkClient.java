package info.batcloud.laxiaoke.open;

public interface LxkClient {

    <T extends LxkResponse> T execute(LxkRequest<T> request);

}
