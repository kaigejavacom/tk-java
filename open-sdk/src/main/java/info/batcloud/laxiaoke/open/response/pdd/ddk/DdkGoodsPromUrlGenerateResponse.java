package info.batcloud.laxiaoke.open.response.pdd.ddk;

import info.batcloud.laxiaoke.open.LxkResponse;
import info.batcloud.pdd.sdk.response.ddk.GoodsPromUrlGenerateResponse;

public class DdkGoodsPromUrlGenerateResponse extends LxkResponse<GoodsPromUrlGenerateResponse> {
}
