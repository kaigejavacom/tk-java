package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum FanliOrderStatus implements EnumTitle {


    WAIT_BACKFILL, WAIT_VERIFY, REJECTED, WAIT_FX, FINISHED;

    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }

}
