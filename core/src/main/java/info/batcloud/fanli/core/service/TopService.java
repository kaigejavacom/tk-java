package info.batcloud.fanli.core.service;

public interface TopService {

    String generateAuthUrl(String state, String view) ;

}
