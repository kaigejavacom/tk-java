package info.batcloud.fanli.core.settings;

import java.io.Serializable;

public class OpenWeiboSetting implements Serializable {

    private String shortUrlSource;

    public String getShortUrlSource() {
        return shortUrlSource;
    }

    public void setShortUrlSource(String shortUrlSource) {
        this.shortUrlSource = shortUrlSource;
    }
}
