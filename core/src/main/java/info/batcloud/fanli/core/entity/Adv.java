package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.operation.center.domain.Page;
import info.batcloud.fanli.core.operation.center.domain.PageJump;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Adv {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Date createTime;

    private Date startTime;

    private Date endTime;

    private boolean valid;

    private boolean deleted;

    private String pic;

    private int picWidth;

    private int picHeight;

    @Enumerated(EnumType.STRING)
    private Page page;
    private String params;

    @ManyToOne(cascade = CascadeType.REFRESH)
    private Crowd crowd;

    public Crowd getCrowd() {
        return crowd;
    }

    public void setCrowd(Crowd crowd) {
        this.crowd = crowd;
    }

    public int getPicWidth() {
        return picWidth;
    }

    public void setPicWidth(int picWidth) {
        this.picWidth = picWidth;
    }

    public int getPicHeight() {
        return picHeight;
    }

    public void setPicHeight(int picHeight) {
        this.picHeight = picHeight;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public String getPageTitle() {
        return page == null ? null : page.getTitle();
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
