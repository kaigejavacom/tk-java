package info.batcloud.fanli.core.constants;

public class TaobaoClientConstants {

    public static final String TAOBAO_CLIENT_ITEM_MANAGEMENT = "TAOBAO_CLIENT_ITEM_MANAGEMENT";
    public static final String TAOBAO_CLIENT_ITEM_TBK = "TAOBAO_CLIENT_ITEM_TBK";
    public static final String TAOBAO_CLIENT_CONFIG_TBK = "TAOBAO_CLIENT_CONFIG_TBK";
}
