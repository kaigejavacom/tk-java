package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.operation.center.domain.Page;

import java.util.Map;

public interface MobilePushService {

    void pushPage(Platform platform, String title, String content, Page page, String pageQueryString);
    void push(Platform platform, String title, String content, Map<String, Object> data);
    void pushToAccount(String account, Platform platform, String title, String content, Map<String, Object> data);
    void pushToAccount(String account, Platform platform, String title, String content, Page page, String pageQueryString);

    void pushToAccount(String account, String title, String content, Page page, String pageQueryString);

    enum Platform {
        IOS,ANDROID
    }
}
