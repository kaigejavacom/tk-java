package info.batcloud.fanli.core.item.collection;

import com.ctospace.archit.common.pagination.Paging;
import com.taobao.api.ApiException;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.TbkDgMaterialOptionalRequest;
import com.taobao.api.response.TbkDgMaterialOptionalResponse;
import info.batcloud.fanli.core.dto.CommissionItemFetchDTO;
import info.batcloud.fanli.core.dto.ShopcatDTO;
import info.batcloud.fanli.core.constants.TaobaoClientConstants;
import info.batcloud.fanli.core.enums.CommissionItemStatus;
import info.batcloud.fanli.core.enums.EcomPlat;
import info.batcloud.fanli.core.enums.ItemSelectionType;
import info.batcloud.fanli.core.helper.CouponHelper;
import info.batcloud.fanli.core.service.ShopcatService;
import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.settings.TaobaoUnionSetting;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.text.ParseException;
import java.util.*;
import java.util.function.Consumer;

@Service
public class TbkDgMaterialOptionalSelectionCollector extends AbstractItemSelectionCollector<TbkDgMaterialOptionalCollectContext> {

    private static final Logger logger = LoggerFactory.getLogger(TbkDgMaterialOptionalSelectionCollector.class);

    @Inject
    @Qualifier(TaobaoClientConstants.TAOBAO_CLIENT_ITEM_TBK)
    private TaobaoClient taobaoClient;

    @Inject
    private SystemSettingService systemSettingService;

    @Inject
    private ShopcatService shopcatService;

    @Override
    public CollectResult collect(TbkDgMaterialOptionalCollectContext context, Consumer<Paging<CommissionItemFetchDTO>> pagingConsumer) {

        TbkDgMaterialOptionalRequest req = new TbkDgMaterialOptionalRequest();
        BeanUtils.copyProperties(context, req);
        req.setPageSize(context.getPageSize() != null ? context.getPageSize() : 20L);
        CollectResult result = new CollectResult();
        TaobaoUnionSetting taobaoUnionSetting = systemSettingService.findActiveSetting(TaobaoUnionSetting.class);
        int page = context.getStartPage();
        if (context.getPlatform() != null) {
            req.setPlatform(context.getPlatform().value);
        }
        if (context.getSort() != null) {
            req.setSort(context.getSort().name().toLowerCase());
        }
        req.setIsTmall(context.getTmall());
        req.setIsOverseas(context.getOverseas());
        if(context.getNeedFreeShipment() != null && context.getNeedFreeShipment()) {
            req.setNeedFreeShipment(context.getNeedFreeShipment());
        }
        if(context.getHasCoupon() != null && context.getHasCoupon()) {
            req.setHasCoupon(true);
        }
        if(context.getMaterialId() != null) {
            req.setMaterialId(Long.valueOf(context.getMaterialId()));
        }
        if(context.getStartPrice() != null) {
            req.setStartPrice(context.getStartPrice().longValue());
        }
        if(context.getEndPrice() != null) {
            req.setEndPrice(context.getEndPrice().longValue());
        }
        if(context.getStartTkRate() != null) {
            req.setStartTkRate(context.getStartTkRate());
        }
        if(context.getEndTkRate() != null) {
            req.setEndTkRate(context.getEndTkRate());
        }
        if(context.getNeedPrepay() != null && context.getNeedPrepay()) {
            req.setNeedPrepay(context.getNeedPrepay());
        }
        if(context.getPlatform() != null) {
            req.setPlatform(context.getPlatform().value);
        }
        if(context.getStartDsr() != null) {
            req.setStartDsr(context.getStartDsr());
        }
        if(context.getIncludeGoodRate() != null) {
            req.setIncludeGoodRate(context.getIncludeGoodRate());
        }
        if(context.getIncludeRfdRate() != null) {
            req.setIncludeRfdRate(context.getIncludeRfdRate());
        }
        if(context.getIncludePayRate30() != null) {
            req.setIncludePayRate30(context.getIncludePayRate30());
        }
        if(StringUtils.isNotBlank(context.getQ())) {
            req.setQ(context.getQ());
        }
        if(StringUtils.isNotBlank(context.getCat()) && !context.getCat().equals("null")) {
            req.setCat(context.getCat());
        }
        if(StringUtils.isNotBlank(context.getItemloc())) {
            req.setItemloc(context.getItemloc());
        }
        if(StringUtils.isNotBlank(context.getIp())) {
            req.setIp(context.getIp());
        }
        req.setNpxLevel(Long.valueOf(context.getNpxLevel()));
        req.setAdzoneId(Long.valueOf(taobaoUnionSetting.getRebateAdzoneId()));
        if (context.getShopcatId() != null && StringUtils.isBlank(context.getCat())) {
            ShopcatDTO shopcatDTO = shopcatService.findById(context.getShopcatId());
            if (shopcatDTO != null) {
                req.setCat(shopcatDTO.getTaobaoCatId() + "");
            }
        }
        try {
            int num = 0;
            while (true) {
                logger.info("搜索" + page + "页");
                req.setPageNo(Long.valueOf(page));
                TbkDgMaterialOptionalResponse rsp = taobaoClient.execute(req);
                if (rsp.getResultList() == null) {
                    break;
                }
                List<CommissionItemFetchDTO> commissionItemFetchDTOList = this.toCommissionItemFetchBO(context, rsp.getResultList());
                Paging<CommissionItemFetchDTO> paging = new Paging<>();
                paging.setTotal(rsp.getTotalResults() == null ? rsp.getResultList().size() :  rsp.getTotalResults().intValue());
                paging.setPage(page);
                paging.setPageSize(context.getPageSize());
                paging.setResults(commissionItemFetchDTOList);
                pagingConsumer.accept(paging);
                num += rsp.getResultList().size();
                if (rsp.getResultList().size() == 0) {
                    break;
                }
                if (page >= context.getMaxPage()) {
                    break;
                }
                page++;
            }
            logger.info("搜索完成");
            result.setSuccess(true);
            result.setTotalNum(num);
        } catch (ApiException e) {
            logger.error("搜索失败", e);
            result.setSuccess(false);
            result.setErrMsg(e.getLocalizedMessage());
        }
        return result;
    }

    private List<CommissionItemFetchDTO> toCommissionItemFetchBO(TbkDgMaterialOptionalCollectContext context, List<TbkDgMaterialOptionalResponse.MapData> mapDataList) {
        List<CommissionItemFetchDTO> dtoList = new ArrayList<>();
        for (TbkDgMaterialOptionalResponse.MapData mapData : mapDataList) {
            CommissionItemFetchDTO dto = new CommissionItemFetchDTO();
            Date now = new Date();
            dto.setListTime(now);
            /**
             * 商品的下架时间为当前的日期+7天，优惠券的下架时间，活动的下架时间取最大者
             * */
            Date delistTime = DateUtils.addDays(now, 7);
            Date couponEndTime;
            if (mapData.getCouponEndTime() != null) {
                try {
                    couponEndTime = DateUtils.parseDate(mapData.getCouponEndTime(), "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss");
                    dto.setCouponEndTime(DateUtils.parseDate(mapData.getCouponEndTime(), "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss"));
                    dto.setCouponStartTime(DateUtils.parseDate(mapData.getCouponStartTime(), "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss"));
                } catch (ParseException e) {
                    try {
                        couponEndTime = new Date(Long.valueOf(mapData.getCouponEndTime()));
                    } catch (Exception e1) {
                        couponEndTime = delistTime;
                    }
                }
            } else {
                couponEndTime = delistTime;
            }
            dto.setDelistTime(Collections.max(Arrays.asList(delistTime, couponEndTime)));
            dto.setShopTitle(org.apache.commons.lang3.StringUtils.defaultString(mapData.getShopTitle(), ""));
            dto.setCouponRemainCount(mapData.getCouponRemainCount() == null ? null : mapData.getCouponRemainCount().intValue());
            dto.setCouponTotalCount(mapData.getCouponTotalCount() == null ? null : mapData.getCouponTotalCount().intValue());
            dto.setCoupon(mapData.getCouponStartTime() == null ? false : true);
            dto.setSummary(mapData.getCouponInfo());
            dto.setCouponInfo(mapData.getCouponInfo());
            float[] couponInfos = CouponHelper.parseCouponAmount(mapData.getCouponInfo());
            dto.setCouponValue(couponInfos[1]);
            dto.setSourceItemUrl(mapData.getItemUrl());
            dto.setSourceItemId(mapData.getNumIid());
            dto.setTitle(mapData.getTitle());
            dto.setSourceItemClickUrl(mapData.getUrl());
            dto.setCouponClickUrl(mapData.getCouponShareUrl());
            dto.setWhitePicUrl(mapData.getWhiteImage());
            dto.setOriginPrice(Float.valueOf(mapData.getZkFinalPrice()));
            dto.setCouponThresholdAmount(couponInfos[0]);
            float commissionRate = Float.valueOf(mapData.getCommissionRate()) / 100;
            if(context.getStartTkRate() != null) {
                //淘宝接口返回淘客佣金比例不一定正确，可能会返回比设置比例小的商品
                if(commissionRate < context.getStartTkRate()) {
                    continue;
                }
            }
            dto.setCommissionRate(commissionRate);

            dto.setEcomPlat(mapData.getUserType() == 0 ? EcomPlat.TAOBAO : EcomPlat.TMALL);
            dto.setImgList(mapData.getSmallImages());
            dto.setPicUrl(mapData.getPictUrl());
            try {
                dto.setSales(mapData.getVolume() == null ? (mapData.getTkTotalSales() == null ? 0 : Integer.valueOf(mapData.getTkTotalSales())) : mapData.getVolume().intValue());
            } catch (Exception e) {
                dto.setSales(0);
            }
            dto.setStatus(CommissionItemStatus.ONSALE);
            dto.setChannelId(context.getChannelId());
            dto.setRecommend(context.isRecommend());
            dto.setShopcatId(context.getShopcatId());
            if(context.getNeedFreeShipment() != null && context.getNeedFreeShipment()) {
                dto.setFreeShipment(context.getNeedFreeShipment());
            }
            dtoList.add(dto);
        }
        return dtoList;
    }

    @Override
    public Class<TbkDgMaterialOptionalCollectContext> getContextType() {
        return TbkDgMaterialOptionalCollectContext.class;
    }

    @Override
    protected ItemSelectionType getItemSelectionType() {
        return ItemSelectionType.TBK_DG_MATERIAL_OPTIONAL;
    }
}
