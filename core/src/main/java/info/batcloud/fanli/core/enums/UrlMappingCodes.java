package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum UrlMappingCodes implements EnumTitle {

    COMMISSION_ITEM_SHARE_PAGE, TAOBAO_NEWER_SHARE_PAGE, COMMISSION_ITEM_PAGE,
    COMMISSION_ITEM_DESC_PAGE, SELLER_PROMOTION_ITEM_SEARCH_BUY_DESC_PAGE,
    HOME, WEIXIN_INVITATION_JUMP_PAGE, WEIXIN_INVITATION_BIND_PAGE,QR_CODE_GENERATE_URL,
    FLASH_SALE_DRAW_URL;

    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, "", null);
    }

    public String getHelp() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + ".help." + this.name(), null, "", null);
    }

}
