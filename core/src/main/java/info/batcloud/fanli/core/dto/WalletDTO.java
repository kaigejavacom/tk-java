package info.batcloud.fanli.core.dto;

public class WalletDTO {

    private Long id;

    private long userId;

    //金币余额
    private float integral;

    //已消费的金币
    private float consumedIntegral;

    //获得的金币
    private float obtainedIntegral;

    //现金余额
    private float money;

    //已提现
    private float withdrawMoney;

    //返利的现金
    private float returnedMoney;

    /**
     * 已消费的金额，用于购买商品，比如夺宝的商品等
     * */
    private float consumedMoney;

    /**
     * 总共获取的金额
     * */
    private float obtainedMoney;

    public float getObtainedMoney() {
        return obtainedMoney;
    }

    public void setObtainedMoney(float obtainedMoney) {
        this.obtainedMoney = obtainedMoney;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public float getIntegral() {
        return integral;
    }

    public void setIntegral(float integral) {
        this.integral = integral;
    }

    public float getConsumedIntegral() {
        return consumedIntegral;
    }

    public void setConsumedIntegral(float consumedIntegral) {
        this.consumedIntegral = consumedIntegral;
    }

    public float getObtainedIntegral() {
        return obtainedIntegral;
    }

    public void setObtainedIntegral(float obtainedIntegral) {
        this.obtainedIntegral = obtainedIntegral;
    }

    public float getMoney() {
        return money;
    }

    public void setMoney(float money) {
        this.money = money;
    }

    public float getWithdrawMoney() {
        return withdrawMoney;
    }

    public void setWithdrawMoney(float withdrawMoney) {
        this.withdrawMoney = withdrawMoney;
    }

    public float getReturnedMoney() {
        return returnedMoney;
    }

    public void setReturnedMoney(float returnedMoney) {
        this.returnedMoney = returnedMoney;
    }

    public float getConsumedMoney() {
        return consumedMoney;
    }

    public void setConsumedMoney(float consumedMoney) {
        this.consumedMoney = consumedMoney;
    }
}
