package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.AgentApply;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AgentApplyRepository extends PagingAndSortingRepository<AgentApply, Long>, JpaSpecificationExecutor<AgentApply> {
}
