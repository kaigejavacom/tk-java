package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum ItemSelectionPlat implements EnumTitle{

    TAOBAO, JD, PDD, DTK;

    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }
}
