package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.FundTransferOrder;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface FundTransferOrderRepository extends PagingAndSortingRepository<FundTransferOrder, Long>, JpaSpecificationExecutor<FundTransferOrder> {
}
