package info.batcloud.fanli.core.item.collection;

public class TbkDgOptimusMaterialContext extends Context{

    private Long materialId;

    private String favoritesId;

    public String getFavoritesId() {
        return favoritesId;
    }

    public void setFavoritesId(String favoritesId) {
        this.favoritesId = favoritesId;
    }

    public Long getMaterialId() {
        return materialId;
    }

    public void setMaterialId(Long materialId) {
        this.materialId = materialId;
    }


}
