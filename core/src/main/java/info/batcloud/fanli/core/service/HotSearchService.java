package info.batcloud.fanli.core.service;

import java.util.List;

public interface HotSearchService {

    List<String> findHotKeywords();

}
