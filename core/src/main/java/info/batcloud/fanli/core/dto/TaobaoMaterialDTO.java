package info.batcloud.fanli.core.dto;

public class TaobaoMaterialDTO {

    private Long id;

    private Long taobaoMaterialCatId;

    private String name;

    private String materialId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTaobaoMaterialCatId() {
        return taobaoMaterialCatId;
    }

    public void setTaobaoMaterialCatId(Long taobaoMaterialCatId) {
        this.taobaoMaterialCatId = taobaoMaterialCatId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMaterialId() {
        return materialId;
    }

    public void setMaterialId(String materialId) {
        this.materialId = materialId;
    }
}
