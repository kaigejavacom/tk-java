package info.batcloud.fanli.core.domain.utam.req;

import java.util.List;

public class AddItemIdToSelectionRequest {

    private Long selectionId;
    private List<Long> itemIdList;

    public Long getSelectionId() {
        return selectionId;
    }

    public void setSelectionId(Long selectionId) {
        this.selectionId = selectionId;
    }

    public List<Long> getItemIdList() {
        return itemIdList;
    }

    public void setItemIdList(List<Long> itemIdList) {
        this.itemIdList = itemIdList;
    }
}
