package info.batcloud.fanli.core.service.impl;

import com.ctospace.archit.common.pagination.Paging;
import info.batcloud.fanli.core.constants.CacheNameConstants;
import info.batcloud.fanli.core.dto.AdvDTO;
import info.batcloud.fanli.core.dto.CommissionItemSearchDTO;
import info.batcloud.fanli.core.enums.AdvSpaceCode;
import info.batcloud.fanli.core.helper.QueryStringHelper;
import info.batcloud.fanli.core.operation.center.domain.PicAdv;
import info.batcloud.fanli.core.operation.center.domain.TabbarNav;
import info.batcloud.fanli.core.service.AdvSpaceService;
import info.batcloud.fanli.core.service.CommissionItemSearchService;
import info.batcloud.fanli.core.service.IndexPageService;
import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.settings.IndexBannerSetting;
import info.batcloud.fanli.core.settings.IndexFloorSetting;
import info.batcloud.fanli.core.settings.IndexQuickNavSetting;
import info.batcloud.fanli.core.settings.IndexTabbarNavSetting;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@CacheConfig(cacheNames = CacheNameConstants.INDEX_PAGE)
public class IndexPageServiceImpl implements IndexPageService {

    @Inject
    private SystemSettingService systemSettingService;

    @Inject
    private CommissionItemSearchService commissionItemSearchService;

    @Inject
    private ApplicationContext applicationContext;

    @Inject
    private AdvSpaceService advSpaceService;

    @Override
    public Banner banner() {
        IndexBannerSetting bannerSetting = systemSettingService.findActiveSetting(IndexBannerSetting.class);
        Banner banner = new Banner();
        banner.setAdvList(advSpaceService.advListByAdvSpaceId(bannerSetting.getAdvSpaceId()));
        return banner;
    }


    @Override
    public List<PicAdv> quickNavList() {
        return systemSettingService.findActiveSetting(IndexQuickNavSetting.class).getQuickNavList();
    }

    @Override
    public List<Floor> floorList() {
        IndexFloorSetting setting = systemSettingService.findActiveSetting(IndexFloorSetting.class);
        List<Floor> floors = new ArrayList<>();
        for (IndexFloorSetting.Floor floor : setting.getFloorList()) {
            Floor f = new Floor();
            f.setTitle(floor.getTitle());
            f.setShowTitle(floor.isShowTitle());
            f.setRowList(rowList(floor.getRowList()));
            f.setTitlePic(floor.getTitlePicUrl());
            f.setTitlePicHeight(floor.getTitlePicHeight());
            f.setTitlePicWidth(floor.getTitlePicWidth());
            floors.add(f);
        }
        return floors;
    }

    @Override
    public List<AdvDTO> quickNavAdvList() {
        return advSpaceService.advListByAdvSpaceCode(AdvSpaceCode.INDEX_QUICK_NAV.name());
    }

    @Override
    public List<TabbarNav> tabbarNavList() {
        return systemSettingService.findActiveSetting(IndexTabbarNavSetting.class).getTabbarNavList();
    }

    @Override
    @CacheEvict(cacheNames = CacheNameConstants.INDEX_PAGE, allEntries = true)
    public void clearCache() {
    }

    private List<Row> rowList(List<IndexFloorSetting.Row> rowList) {
        List<Row> list = new ArrayList<>();
        for (IndexFloorSetting.Row row : rowList) {
            Row r = new Row();
            r.setHeight(row.getHeight());
            r.setShowcaseList(showcaseList(row.getShowcaseList()));
            list.add(r);
        }
        return list;
    }

    private List<Showcase> showcaseList(List<IndexFloorSetting.Showcase> showcaseList) {
        List<Showcase> list = new ArrayList<>();
        for (IndexFloorSetting.Showcase showcase : showcaseList) {
            Showcase s = new Showcase();
            BeanUtils.copyProperties(showcase, s);
            s.setPic(showcase.getPicUrl());
            if (showcase.isShowItem()) {
                s.setCommissionItemList(shuffleFetch(applicationContext.getBean(IndexPageServiceImpl.class)
                                .findCouponItemList(showcase.getItemSearchParams(), showcase.getItemFetchNum())
                        , showcase.getItemShowNum()));
            } else {
                s.setCommissionItemList(Collections.EMPTY_LIST);
            }
            list.add(s);
        }
        return list;
    }

    @Cacheable(value = "INDEX_PAGE#600#60", key = "'showcase:'+ #searchParams ", sync = true)
    public List<CommissionItemSearchDTO> findCouponItemList(String searchParams, int fetchNum) {
        CommissionItemSearchService.SearchParam param = new CommissionItemSearchService.SearchParam();
        QueryStringHelper.populate(param, searchParams);
        param.setPageSize(fetchNum);
        Paging<CommissionItemSearchDTO> paging = commissionItemSearchService.search(param);
        return paging.getResults();
    }

    private List<CommissionItemModel> shuffleFetch(List<CommissionItemSearchDTO> couponItems, int size) {
        List<CommissionItemSearchDTO> list = new ArrayList<>(couponItems);
        if (list.size() > size) {
            Collections.shuffle(list);
            list = list.subList(0, size);
        }
        List<CommissionItemModel> models = new ArrayList<>();
        for (CommissionItemSearchDTO couponItem : list) {
            CommissionItemModel model = new CommissionItemModel();
            BeanUtils.copyProperties(couponItem, model);
            models.add(model);
        }
        return models;
    }
}
