package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import info.batcloud.fanli.core.dto.CommissionItemSearchDTO;
import info.batcloud.fanli.core.enums.CommissionItemSort;
import info.batcloud.fanli.core.enums.EcomPlat;
import info.batcloud.fanli.core.enums.SearchFrom;

import java.io.Serializable;
import java.util.List;

public interface CommissionItemSearchService {

    Paging<CommissionItemSearchDTO> search(SearchParam param);

    List<CommissionItemSearchDTO> findSameStyleById(long id, int size);

    class SearchParam implements Serializable {

        private Boolean recommend;

        private String keywords;

        private Float startPrice;

        private Float endPrice;

        private CommissionItemSort sort;

        private EcomPlat ecomPlat;

        private String catIds;

        private Long catId;

        private Long channelId;

        private String channelIds;

        private String itemIds;

        private Float minCommissionFee;

        private int page = 1;
        private int pageSize = 20;

        private Boolean freeShipment;

        private Boolean jdSale;

        private SearchFrom searchFrom;

        private Boolean coupon;

        private Integer minSales;

        private Long freeChargeActivityId;

        //淘宝物料id
        private Long taobaoMaterialId;

        private Boolean tbkZt;

        public Boolean getTbkZt() {
            return tbkZt;
        }

        public void setTbkZt(Boolean tbkZt) {
            this.tbkZt = tbkZt;
        }

        public Long getTaobaoMaterialId() {
            return taobaoMaterialId;
        }

        public void setTaobaoMaterialId(Long taobaoMaterialId) {
            this.taobaoMaterialId = taobaoMaterialId;
        }

        public Float getMinCommissionFee() {
            return minCommissionFee;
        }

        public void setMinCommissionFee(Float minCommissionFee) {
            this.minCommissionFee = minCommissionFee;
        }

        public Long getFreeChargeActivityId() {
            return freeChargeActivityId;
        }

        public void setFreeChargeActivityId(Long freeChargeActivityId) {
            this.freeChargeActivityId = freeChargeActivityId;
        }

        public Integer getMinSales() {
            return minSales;
        }

        public void setMinSales(Integer minSales) {
            this.minSales = minSales;
        }

        public Boolean getCoupon() {
            return coupon;
        }

        public void setCoupon(Boolean coupon) {
            this.coupon = coupon;
        }

        public SearchFrom getSearchFrom() {
            return searchFrom;
        }

        public void setSearchFrom(SearchFrom searchFrom) {
            this.searchFrom = searchFrom;
        }

        public Boolean getJdSale() {
            return jdSale;
        }

        public void setJdSale(Boolean jdSale) {
            this.jdSale = jdSale;
        }

        public Boolean getFreeShipment() {
            return freeShipment;
        }

        public void setFreeShipment(Boolean freeShipment) {
            this.freeShipment = freeShipment;
        }

        public Boolean getRecommend() {
            return recommend;
        }

        public void setRecommend(Boolean recommend) {
            this.recommend = recommend;
        }

        public Long getCatId() {
            return catId;
        }

        public void setCatId(Long catId) {
            this.catId = catId;
        }

        public Long getChannelId() {
            return channelId;
        }

        public void setChannelId(Long channelId) {
            this.channelId = channelId;
        }

        public String getChannelIds() {
            return channelIds;
        }

        public void setChannelIds(String channelIds) {
            this.channelIds = channelIds;
        }

        public String getItemIds() {
            return itemIds;
        }

        public void setItemIds(String itemIds) {
            this.itemIds = itemIds;
        }

        public String getCatIds() {
            return catIds;
        }

        public void setCatIds(String catIds) {
            this.catIds = catIds;
        }

        public EcomPlat getEcomPlat() {
            return ecomPlat;
        }

        public void setEcomPlat(EcomPlat ecomPlat) {
            this.ecomPlat = ecomPlat;
        }

        public Float getStartPrice() {
            return startPrice;
        }

        public void setStartPrice(Float startPrice) {
            this.startPrice = startPrice;
        }

        public Float getEndPrice() {
            return endPrice;
        }

        public void setEndPrice(Float endPrice) {
            this.endPrice = endPrice;
        }

        public CommissionItemSort getSort() {
            return sort == null ? CommissionItemSort.NONE : sort;
        }

        public void setSort(CommissionItemSort sort) {
            this.sort = sort;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public String getKeywords() {
            return keywords;
        }

        public void setKeywords(String keywords) {
            this.keywords = keywords;
        }
    }

}
