package info.batcloud.fanli.core.service;

import com.alipay.api.AlipayApiException;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import info.batcloud.fanli.core.alipay.domain.NotifyParam;
import info.batcloud.fanli.core.domain.Result;
import info.batcloud.fanli.core.dto.OrderForPay;
import info.batcloud.fanli.core.entity.Order;
import info.batcloud.fanli.core.enums.OrderPayType;

public interface OrderService {

    IntegralPayResult payByIntegral(long id);

    OrderForPay findPayInfo(long id);

    <T extends Order> void registerOrderHandler(Class<T> orderClazz, OrderHandler<T> orderHandler);

    AlipayTradeAppPayResponse appAlipay(long id) throws AlipayApiException;

    boolean handleAlipayNotify(NotifyParam param) throws AlipayApiException;

    boolean payOrder(PayParam payParam);

    class TransferPayResult extends Result {

    }

    class IntegralPayResult extends Result {

    }

    class PayParam {
        private Long orderId;
        private float totalAmount;
        private OrderPayType payType;

        public OrderPayType getPayType() {
            return payType;
        }

        public void setPayType(OrderPayType payType) {
            this.payType = payType;
        }

        public Long getOrderId() {
            return orderId;
        }

        public void setOrderId(Long orderId) {
            this.orderId = orderId;
        }

        public float getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(float totalAmount) {
            this.totalAmount = totalAmount;
        }
    }

    interface OrderHandler<T extends Order> {
        void handlePayResult(T order, PayResult payResult);
        CheckPayResult checkPay(T order);
        String getContent(T order);
        String getTitle(T order);
        String getStatusTitle(T order);
        boolean isCanPay(T order);
    }

    class CheckPayResult extends Result {

    }

    class PayResult extends Result{
        private OrderPayType orderPayType;

        public OrderPayType getOrderPayType() {
            return orderPayType;
        }

        public void setOrderPayType(OrderPayType orderPayType) {
            this.orderPayType = orderPayType;
        }
    }
}
