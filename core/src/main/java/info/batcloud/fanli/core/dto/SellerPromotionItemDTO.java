package info.batcloud.fanli.core.dto;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import info.batcloud.fanli.core.domain.Image;
import info.batcloud.fanli.core.enums.EcomPlat;
import info.batcloud.fanli.core.enums.SellerPromotionItemStatus;
import info.batcloud.fanli.core.helper.UrlHelper;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class SellerPromotionItemDTO {

    private Long id;

    private Long userId;

    private String title;

    private String shopTitle;

    private String picUrl;

    private String sourceItemUrl;

    private String imgs;

    private String logisticsCompany;

    private Long shopcatId;

    private String keywords;

    private float rebateFee;

    private float originPrice;

    private float price;

    private int totalNum;

    private int totalSaleNum;

    private EcomPlat ecomPlat;

    //日放单数量
    private int dayLimitNum;

    //担保金
    private float guaranteeFee;

    private int remainNum;

    private String description;

    private SellerPromotionItemStatus status;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    private String screenshots;

    private long remainSeconds;

    public long getRemainSeconds() {
        return remainSeconds;
    }

    public void setRemainSeconds(long remainSeconds) {
        this.remainSeconds = remainSeconds;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRemainNum() {
        return remainNum;
    }

    public void setRemainNum(int remainNum) {
        this.remainNum = remainNum;
    }

    public String getScreenshots() {
        return screenshots;
    }

    public void setScreenshots(String screenshots) {
        this.screenshots = screenshots;
    }

    public float getGuaranteeFee() {
        return guaranteeFee;
    }

    public void setGuaranteeFee(float guaranteeFee) {
        this.guaranteeFee = guaranteeFee;
    }

    public String getEcomPlatTitle() {
        return ecomPlat == null ? null : ecomPlat.getTitle();
    }

    public String getStatusTitle() {
        return status == null ? null : status.getTitle();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public void setShopTitle(String shopTitle) {
        this.shopTitle = shopTitle;
    }

    public String getPicUrl() {
        return UrlHelper.toUrl(picUrl);
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getSourceItemUrl() {
        return sourceItemUrl;
    }

    public void setSourceItemUrl(String sourceItemUrl) {
        this.sourceItemUrl = sourceItemUrl;
    }

    public String getImgs() {
        return imgs;
    }

    public void setImgs(String imgs) {
        this.imgs = imgs;
    }

    public String getLogisticsCompany() {
        return logisticsCompany;
    }

    public void setLogisticsCompany(String logisticsCompany) {
        this.logisticsCompany = logisticsCompany;
    }

    public Long getShopcatId() {
        return shopcatId;
    }

    public void setShopcatId(Long shopcatId) {
        this.shopcatId = shopcatId;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public float getRebateFee() {
        return rebateFee;
    }

    public void setRebateFee(float rebateFee) {
        this.rebateFee = rebateFee;
    }

    public float getOriginPrice() {
        return originPrice;
    }

    public void setOriginPrice(float originPrice) {
        this.originPrice = originPrice;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    public int getTotalSaleNum() {
        return totalSaleNum;
    }

    public void setTotalSaleNum(int totalSaleNum) {
        this.totalSaleNum = totalSaleNum;
    }

    public EcomPlat getEcomPlat() {
        return ecomPlat;
    }

    public void setEcomPlat(EcomPlat ecomPlat) {
        this.ecomPlat = ecomPlat;
    }

    public int getDayLimitNum() {
        return dayLimitNum;
    }

    public void setDayLimitNum(int dayLimitNum) {
        this.dayLimitNum = dayLimitNum;
    }

    public SellerPromotionItemStatus getStatus() {
        return status;
    }

    public void setStatus(SellerPromotionItemStatus status) {
        this.status = status;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public List<String> getImgList() {
        if (StringUtils.isBlank(this.getImgs())) {
            return new ArrayList<>();
        }
        return JSON.parseObject(this.getImgs(), new TypeReference<List<String>>() {
        }).stream().map(url -> UrlHelper.toUrl(picUrl)).collect(Collectors.toList());
    }

    public List<Image> getScreenshotList() {
        if (StringUtils.isBlank(this.getScreenshots())) {
            return new ArrayList<>();
        }
        List<String> list = JSON.parseObject(this.getScreenshots(), new TypeReference<List<String>>() {
        });
        return list.stream().map(key -> new Image(key)).collect(Collectors.toList());
    }
}
