package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.entity.TaobaoPid;

public interface TaobaoPidService {

    TaobaoPid fetch();

    void save(String value);
}
