package info.batcloud.fanli.core.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import info.batcloud.fanli.core.enums.Gender;
import info.batcloud.fanli.core.enums.UserLevel;

import java.util.Date;

public class UserDTO {

    private Long id;

    private String nickname;

    private Gender gender;

    private String city;

    private Long cityId;

    private Long provinceId;

    private String district;

    private Long districtId;

    private String province;

    private String country;

    private String avatarUrl;

    private boolean locked;

    private String taobaoSpecialId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    private Date lastSignTime;

    //连续签到次数，总的
    private int continuousSignTimes;

    private String phone;

    private String password;

    //层级关系
    private String relationPath;

    private String taobaoPid;

    private Long taobaoAdzoneId;

    private String invitationCode;//邀请码

    private String weixinOpenId;

    private UserLevel level;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date levelExpireTime;

    private String lastLoginIp;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastActiveTime;

    private int activeTimes;

    private int loginTimes;

    private Long superUserId;

    private String superUserPhone;

    private String superUserNickname;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastLoginTime;

    public String getTaobaoSpecialId() {
        return taobaoSpecialId;
    }

    public void setTaobaoSpecialId(String taobaoSpecialId) {
        this.taobaoSpecialId = taobaoSpecialId;
    }

    public Date getLastActiveTime() {
        return lastActiveTime;
    }

    public void setLastActiveTime(Date lastActiveTime) {
        this.lastActiveTime = lastActiveTime;
    }

    public int getActiveTimes() {
        return activeTimes;
    }

    public void setActiveTimes(int activeTimes) {
        this.activeTimes = activeTimes;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public int getLoginTimes() {
        return loginTimes;
    }

    public void setLoginTimes(int loginTimes) {
        this.loginTimes = loginTimes;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Long getSuperUserId() {
        return superUserId;
    }

    public void setSuperUserId(Long superUserId) {
        this.superUserId = superUserId;
    }

    public String getSuperUserPhone() {
        return superUserPhone;
    }

    public void setSuperUserPhone(String superUserPhone) {
        this.superUserPhone = superUserPhone;
    }

    public String getSuperUserNickname() {
        return superUserNickname;
    }

    public void setSuperUserNickname(String superUserNickname) {
        this.superUserNickname = superUserNickname;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public Date getLevelExpireTime() {
        return levelExpireTime;
    }

    public void setLevelExpireTime(Date levelExpireTime) {
        this.levelExpireTime = levelExpireTime;
    }

    public UserLevel getLevel() {
        return level;
    }

    public String getLevelTitle() {
        return level == null ? null : level.getTitle();
    }

    public void setLevel(UserLevel level) {
        this.level = level;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastSignTime() {
        return lastSignTime;
    }

    public void setLastSignTime(Date lastSignTime) {
        this.lastSignTime = lastSignTime;
    }

    public int getContinuousSignTimes() {
        return continuousSignTimes;
    }

    public void setContinuousSignTimes(int continuousSignTimes) {
        this.continuousSignTimes = continuousSignTimes;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRelationPath() {
        return relationPath;
    }

    public void setRelationPath(String relationPath) {
        this.relationPath = relationPath;
    }

    public String getTaobaoPid() {
        return taobaoPid;
    }

    public void setTaobaoPid(String taobaoPid) {
        this.taobaoPid = taobaoPid;
    }

    public Long getTaobaoAdzoneId() {
        return taobaoAdzoneId;
    }

    public void setTaobaoAdzoneId(Long taobaoAdzoneId) {
        this.taobaoAdzoneId = taobaoAdzoneId;
    }

    public String getInvitationCode() {
        return invitationCode;
    }

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }

    public String getWeixinOpenId() {
        return weixinOpenId;
    }

    public void setWeixinOpenId(String weixinOpenId) {
        this.weixinOpenId = weixinOpenId;
    }
}
