package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum AdvSpaceCode implements EnumTitle{
    NONE, INDEX_QUICK_NAV, COMMISSION_ITEM_SEARCH_PAGE, INDEX_POPUP_ADV;

    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }
}
