package info.batcloud.fanli.core.dto;

import info.batcloud.fanli.core.enums.CommissionItemStatus;
import info.batcloud.fanli.core.enums.EcomPlat;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Date;
import java.util.List;

public class CommissionItemUpdateDTO {

    private String title;

    private String picUrl;

    private Float price;

    private Float originPrice;

    private String shopTitle;

    private Date listTime; //上架时间

    private Date delistTime;//下架时间

    private String description;

    private String summary;

    private int sales;

    private String shopcatPath;

    private Long shopcatId;

    @Enumerated(EnumType.STRING)
    private CommissionItemStatus status;

    //来源电商平台
    @Enumerated(EnumType.STRING)
    private EcomPlat ecomPlat;

    private Long sourceItemId;

    private String sourceItemUrl;

    private String sourceItemClickUrl;

    private Long sourceCatId;

    private String itemPlace; //货源地

    private Float commissionRate;

    private Float couponValue;

    private boolean coupon;

    private String couponClickUrl;

    private Date couponStartTime;

    private Date couponEndTime;

    private String couponInfo;

    private Integer couponTotalCount;

    private Integer couponRemainCount;

    private boolean recommend;

    private List<String> imgList;

    public boolean isRecommend() {
        return recommend;
    }

    public void setRecommend(boolean recommend) {
        this.recommend = recommend;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getOriginPrice() {
        return originPrice;
    }

    public void setOriginPrice(Float originPrice) {
        this.originPrice = originPrice;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public void setShopTitle(String shopTitle) {
        this.shopTitle = shopTitle;
    }

    public Date getListTime() {
        return listTime;
    }

    public void setListTime(Date listTime) {
        this.listTime = listTime;
    }

    public Date getDelistTime() {
        return delistTime;
    }

    public void setDelistTime(Date delistTime) {
        this.delistTime = delistTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public int getSales() {
        return sales;
    }

    public void setSales(int sales) {
        this.sales = sales;
    }

    public String getShopcatPath() {
        return shopcatPath;
    }

    public void setShopcatPath(String shopcatPath) {
        this.shopcatPath = shopcatPath;
    }

    public Long getShopcatId() {
        return shopcatId;
    }

    public void setShopcatId(Long shopcatId) {
        this.shopcatId = shopcatId;
    }

    public CommissionItemStatus getStatus() {
        return status;
    }

    public void setStatus(CommissionItemStatus status) {
        this.status = status;
    }

    public EcomPlat getEcomPlat() {
        return ecomPlat;
    }

    public void setEcomPlat(EcomPlat ecomPlat) {
        this.ecomPlat = ecomPlat;
    }

    public Long getSourceItemId() {
        return sourceItemId;
    }

    public void setSourceItemId(Long sourceItemId) {
        this.sourceItemId = sourceItemId;
    }

    public String getSourceItemUrl() {
        return sourceItemUrl;
    }

    public void setSourceItemUrl(String sourceItemUrl) {
        this.sourceItemUrl = sourceItemUrl;
    }

    public String getSourceItemClickUrl() {
        return sourceItemClickUrl;
    }

    public void setSourceItemClickUrl(String sourceItemClickUrl) {
        this.sourceItemClickUrl = sourceItemClickUrl;
    }

    public Long getSourceCatId() {
        return sourceCatId;
    }

    public void setSourceCatId(Long sourceCatId) {
        this.sourceCatId = sourceCatId;
    }

    public String getItemPlace() {
        return itemPlace;
    }

    public void setItemPlace(String itemPlace) {
        this.itemPlace = itemPlace;
    }

    public Float getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(Float commissionRate) {
        this.commissionRate = commissionRate;
    }

    public Float getCouponValue() {
        return couponValue;
    }

    public void setCouponValue(Float couponValue) {
        this.couponValue = couponValue;
    }

    public boolean isCoupon() {
        return coupon;
    }

    public void setCoupon(boolean coupon) {
        this.coupon = coupon;
    }

    public String getCouponClickUrl() {
        return couponClickUrl;
    }

    public void setCouponClickUrl(String couponClickUrl) {
        this.couponClickUrl = couponClickUrl;
    }

    public Date getCouponStartTime() {
        return couponStartTime;
    }

    public void setCouponStartTime(Date couponStartTime) {
        this.couponStartTime = couponStartTime;
    }

    public Date getCouponEndTime() {
        return couponEndTime;
    }

    public void setCouponEndTime(Date couponEndTime) {
        this.couponEndTime = couponEndTime;
    }

    public String getCouponInfo() {
        return couponInfo;
    }

    public void setCouponInfo(String couponInfo) {
        this.couponInfo = couponInfo;
    }

    public Integer getCouponTotalCount() {
        return couponTotalCount;
    }

    public void setCouponTotalCount(Integer couponTotalCount) {
        this.couponTotalCount = couponTotalCount;
    }

    public Integer getCouponRemainCount() {
        return couponRemainCount;
    }

    public void setCouponRemainCount(Integer couponRemainCount) {
        this.couponRemainCount = couponRemainCount;
    }

    public List<String> getImgList() {
        return imgList;
    }

    public void setImgList(List<String> imgList) {
        this.imgList = imgList;
    }
}
