package info.batcloud.fanli.core.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * list: {
 *                 status: function() {
 *                     switch (this.status) {
 *                     case 1:
 *                         return "已注册";
 *                     case 2:
 *                         return "已激活";
 *                     case 3:
 *                         return "已首购";
 *                     case 4:
 *                         return "已收货"
 *                     }
 *                 },
 *                 orderTkType: function() {
 *                     switch (this.orderTkType) {
 *                     case -1:
 *                         return "未完成首购";
 *                     case 1:
 *                         return "淘客订单";
 *                     case 2:
 *                         return "非淘客订单"
 *                     }
 *                 }
 *             }
 * */
@Entity
public class TbPullNewDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Date registerTime;

    private Date bindTime;

    private Date buyTime;

    private String mobile;

    private int status;

    private int orderTkType;

    private Long tbTradeParentId;

    //广告位id
    private Long adzoneId;

    //媒体id
    private Long memberId;

    private Date createTime;

    private Long userId;

    //是否已经结算掉
    private boolean settled;

    public boolean isSettled() {
        return settled;
    }

    public void setSettled(boolean settled) {
        this.settled = settled;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    public Date getBindTime() {
        return bindTime;
    }

    public void setBindTime(Date bindTime) {
        this.bindTime = bindTime;
    }

    public Date getBuyTime() {
        return buyTime;
    }

    public void setBuyTime(Date buyTime) {
        this.buyTime = buyTime;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getOrderTkType() {
        return orderTkType;
    }

    public void setOrderTkType(int orderTkType) {
        this.orderTkType = orderTkType;
    }

    public Long getTbTradeParentId() {
        return tbTradeParentId;
    }

    public void setTbTradeParentId(Long tbTradeParentId) {
        this.tbTradeParentId = tbTradeParentId;
    }

    public Long getAdzoneId() {
        return adzoneId;
    }

    public void setAdzoneId(Long adzoneId) {
        this.adzoneId = adzoneId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
