package info.batcloud.fanli.core.settings;

import info.batcloud.fanli.core.settings.annotation.Single;

import java.io.Serializable;

@Single
public class TaobaoUnionSetting implements Serializable {

    private String taobaoUsername;

    private String taobaoAccount;

    private String taobaoAccountId;

    /**
     * 会员运营PID
     */
    private String rebatePid;

    /**
     * 会员运营AdzoneId
     */
    private String rebateAdzoneId;

    private String adzoneId;

    private String pid;

    private String defaultUserPid;

    private String siteId;

    /**
     * 渠道PID
     */
    private String proxyPid;

    /**
     * 渠道运营AdzoneId
     */
    private String proxyAdzoneId;

    /**
     * 联盟ID
     */
    private String unionId;

    /**
     * 会员邀请码
     * */
    private String inviteCode;

    private String channelInviteCode;

    public String getChannelInviteCode() {
        return channelInviteCode;
    }

    public void setChannelInviteCode(String channelInviteCode) {
        this.channelInviteCode = channelInviteCode;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getDefaultUserPid() {
        return defaultUserPid;
    }

    public void setDefaultUserPid(String defaultUserPid) {
        this.defaultUserPid = defaultUserPid;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getAdzoneId() {
        return adzoneId;
    }

    public void setAdzoneId(String adzoneId) {
        this.adzoneId = adzoneId;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public String getRebateAdzoneId() {
        return rebateAdzoneId;
    }

    public void setRebateAdzoneId(String rebateAdzoneId) {
        this.rebateAdzoneId = rebateAdzoneId;
    }

    public String getProxyAdzoneId() {
        return proxyAdzoneId;
    }

    public void setProxyAdzoneId(String proxyAdzoneId) {
        this.proxyAdzoneId = proxyAdzoneId;
    }

    public String getUnionId() {
        return unionId;
    }

    public void setUnionId(String unionId) {
        this.unionId = unionId;
    }

    public String getTaobaoUsername() {
        return taobaoUsername;
    }

    public void setTaobaoUsername(String taobaoUsername) {
        this.taobaoUsername = taobaoUsername;
    }

    public String getTaobaoAccount() {
        return taobaoAccount;
    }

    public void setTaobaoAccount(String taobaoAccount) {
        this.taobaoAccount = taobaoAccount;
    }

    public String getTaobaoAccountId() {
        return taobaoAccountId;
    }

    public void setTaobaoAccountId(String taobaoAccountId) {
        this.taobaoAccountId = taobaoAccountId;
    }

    public String getRebatePid() {
        return rebatePid;
    }

    public void setRebatePid(String rebatePid) {
        this.rebatePid = rebatePid;
    }

    public String getProxyPid() {
        return proxyPid;
    }

    public void setProxyPid(String proxyPid) {
        this.proxyPid = proxyPid;
    }
}
