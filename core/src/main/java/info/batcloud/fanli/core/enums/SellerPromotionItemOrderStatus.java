package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum SellerPromotionItemOrderStatus implements EnumTitle {

    WAIT_FILL_TRADE_NO, WAIT_VERIFY, VERIFY_FAIL, WAIT_REBATE, REBATE_SUCCESS, CANCELED, DELETED;


    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, "", null);
    }

}
