package info.batcloud.fanli.core.settings;

import info.batcloud.fanli.core.operation.center.domain.PicAdv;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class IndexAdvSetting implements Serializable {

    private List<PicAdv> bannerAdvList = new ArrayList<>();

    public List<PicAdv> getBannerAdvList() {
        return bannerAdvList;
    }

    public void setBannerAdvList(List<PicAdv> bannerAdvList) {
        this.bannerAdvList = bannerAdvList;
    }
}
