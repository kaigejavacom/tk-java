package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.UserLog;
import info.batcloud.fanli.core.enums.UserLogType;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserLogRepository extends PagingAndSortingRepository<UserLog, Long> {

    UserLog findTop1ByUserIdAndType(long userId, UserLogType logType);

}
