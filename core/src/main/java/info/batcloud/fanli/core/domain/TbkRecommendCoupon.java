package info.batcloud.fanli.core.domain;

import info.batcloud.fanli.core.excel.stereotype.XslCol;

import java.util.Date;

public class TbkRecommendCoupon {

    @XslCol(title = "商品id")
    private Long itemId;

    @XslCol(title = "商品名称")
    private String title;

    @XslCol(title = "商品主图")
    private String picUrl;

    @XslCol(title = "商品详情页链接地址")
    private String itemUrl;

    @XslCol(title = "商品一级类目")
    private String itemCatName;

    @XslCol(title = "淘宝客链接")
    private String clickUrl;

    @XslCol(title = "商品价格(单位：元)")
    private Float price;

    @XslCol(title = "商品月销量")
    private int sales;

    @XslCol(title = "收入比率(%)")
    private Float commissionRate;

    @XslCol(title = "佣金")
    private Float commissionFee;

    @XslCol(title = "卖家旺旺")
    private String sellerName;

    @XslCol(title = "卖家id")
    private Long sellerId;

    @XslCol(title = "店铺名称")
    private String shopName;

    @XslCol(title = "平台类型")
    private String platform;

    @XslCol(title = "优惠券id")
    private String couponId;

    @XslCol(title = "优惠券总量")
    private int couponTotalCount;

    @XslCol(title = "优惠券剩余量")
    private int couponRemainCount;

    @XslCol(title = "优惠券面额")
    private String couponInfo;

    @XslCol(title = "优惠券开始时间")
    private Date couponStartTime;

    @XslCol(title = "优惠券结束时间")
    private Date couponEndTime;

    @XslCol(title = "优惠券链接")
    private String couponUrl;

    @XslCol(title = "商品优惠券推广链接")
    private String couponClickUrl;

    public String getItemUrl() {
        return itemUrl;
    }

    public void setItemUrl(String itemUrl) {
        this.itemUrl = itemUrl;
    }

    public int getCouponTotalCount() {
        return couponTotalCount;
    }

    public void setCouponTotalCount(int couponTotalCount) {
        this.couponTotalCount = couponTotalCount;
    }

    public int getCouponRemainCount() {
        return couponRemainCount;
    }

    public void setCouponRemainCount(int couponRemainCount) {
        this.couponRemainCount = couponRemainCount;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getItemCatName() {
        return itemCatName;
    }

    public void setItemCatName(String itemCatName) {
        this.itemCatName = itemCatName;
    }

    public String getClickUrl() {
        return clickUrl;
    }

    public void setClickUrl(String clickUrl) {
        this.clickUrl = clickUrl;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public int getSales() {
        return sales;
    }

    public void setSales(int sales) {
        this.sales = sales;
    }

    public Float getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(Float commissionRate) {
        this.commissionRate = commissionRate;
    }

    public Float getCommissionFee() {
        return commissionFee;
    }

    public void setCommissionFee(Float commissionFee) {
        this.commissionFee = commissionFee;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCouponInfo() {
        return couponInfo;
    }

    public void setCouponInfo(String couponInfo) {
        this.couponInfo = couponInfo;
    }

    public Date getCouponStartTime() {
        return couponStartTime;
    }

    public void setCouponStartTime(Date couponStartTime) {
        this.couponStartTime = couponStartTime;
    }

    public Date getCouponEndTime() {
        return couponEndTime;
    }

    public void setCouponEndTime(Date couponEndTime) {
        this.couponEndTime = couponEndTime;
    }

    public String getCouponUrl() {
        return couponUrl;
    }

    public void setCouponUrl(String couponUrl) {
        this.couponUrl = couponUrl;
    }

    public String getCouponClickUrl() {
        return couponClickUrl;
    }

    public void setCouponClickUrl(String couponClickUrl) {
        this.couponClickUrl = couponClickUrl;
    }
}
