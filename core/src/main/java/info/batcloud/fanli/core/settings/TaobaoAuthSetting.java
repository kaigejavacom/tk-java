package info.batcloud.fanli.core.settings;

import info.batcloud.fanli.core.settings.annotation.Single;

import java.io.Serializable;
import java.util.Date;

@Single
public class TaobaoAuthSetting implements Serializable {

    private Date expiresIn;

    private Date w2ExpiresIn;

    private String taobaoUserNick;

    private Long taobaoUserId;

    private Date w1ExpiresIn;

    private Date reExpiresIn;

    private Date r2ExpiresIn;

    private String tokenType;

    private String refreshToken;

    private String accessToken;

    private Date r1ExpiresIn;

    public Date getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Date expiresIn) {
        this.expiresIn = expiresIn;
    }

    public Date getW2ExpiresIn() {
        return w2ExpiresIn;
    }

    public void setW2ExpiresIn(Date w2ExpiresIn) {
        this.w2ExpiresIn = w2ExpiresIn;
    }

    public String getTaobaoUserNick() {
        return taobaoUserNick;
    }

    public void setTaobaoUserNick(String taobaoUserNick) {
        this.taobaoUserNick = taobaoUserNick;
    }

    public Long getTaobaoUserId() {
        return taobaoUserId;
    }

    public void setTaobaoUserId(Long taobaoUserId) {
        this.taobaoUserId = taobaoUserId;
    }

    public Date getW1ExpiresIn() {
        return w1ExpiresIn;
    }

    public void setW1ExpiresIn(Date w1ExpiresIn) {
        this.w1ExpiresIn = w1ExpiresIn;
    }

    public Date getReExpiresIn() {
        return reExpiresIn;
    }

    public void setReExpiresIn(Date reExpiresIn) {
        this.reExpiresIn = reExpiresIn;
    }

    public Date getR2ExpiresIn() {
        return r2ExpiresIn;
    }

    public void setR2ExpiresIn(Date r2ExpiresIn) {
        this.r2ExpiresIn = r2ExpiresIn;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Date getR1ExpiresIn() {
        return r1ExpiresIn;
    }

    public void setR1ExpiresIn(Date r1ExpiresIn) {
        this.r1ExpiresIn = r1ExpiresIn;
    }
}
