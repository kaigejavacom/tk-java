package info.batcloud.fanli.core.mapper.stat;

import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.domain.stat.UserCommissionEarning;
import info.batcloud.fanli.core.domain.stat.UserMonthSettleEarning;
import info.batcloud.fanli.core.domain.stat.UserSettledCommissionEarning;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Mapper
public interface UserCommissionEarningMapper {

    /**
     * 按日期维度进行收益统计
     * */
    List<UserCommissionEarning> userCommissionEarning(UserEstimateStatParam param);

    List<UserMonthSettleEarning> userMonthSettleEarning(UserMonthSettleStatParam param);
    List<UserSettledCommissionEarning> userSettledCommissionEarning(UserSettledCommissionStatParam param);

    class UserSettledCommissionStatParam {
        private Date startTime;
        private Date endTime;
        private Long userId;

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Date getStartTime() {
            return startTime;
        }

        public void setStartTime(Date startTime) {
            this.startTime = startTime;
        }

        public Date getEndTime() {
            return endTime;
        }

        public void setEndTime(Date endTime) {
            this.endTime = endTime;
        }

    }
    class UserMonthSettleStatParam {
        private Date startTime;
        private Date endTime;
        private Date settleStartTime;
        private Date settleEndTime;
        private Long userId;

        public Date getSettleStartTime() {
            return settleStartTime;
        }

        public void setSettleStartTime(Date settleStartTime) {
            this.settleStartTime = settleStartTime;
        }

        public Date getSettleEndTime() {
            return settleEndTime;
        }

        public void setSettleEndTime(Date settleEndTime) {
            this.settleEndTime = settleEndTime;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Date getStartTime() {
            return startTime;
        }

        public void setStartTime(Date startTime) {
            this.startTime = startTime;
        }

        public Date getEndTime() {
            return endTime;
        }

        public void setEndTime(Date endTime) {
            this.endTime = endTime;
        }

    }
    class UserEstimateStatParam extends PagingParam {
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date startTime;
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date endTime;
        private TimeDimension timeDimension;
        private Long userId;

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Date getStartTime() {
            return startTime;
        }

        public void setStartTime(Date startTime) {
            this.startTime = startTime;
        }

        public Date getEndTime() {
            return endTime;
        }

        public void setEndTime(Date endTime) {
            this.endTime = endTime;
        }

        public TimeDimension getTimeDimension() {
            return timeDimension;
        }

        public void setTimeDimension(TimeDimension timeDimension) {
            this.timeDimension = timeDimension;
        }
    }

    enum TimeDimension {
        NONE, DAY, MONTH
    }
}
