package info.batcloud.fanli.core.constants;

public class MessageKeyConstants {

    public final static String COMMISSION_ITEM_SHARE_TEXT = "CommissionItem.shareText";
    public final static String COMMISSION_ITEM_SHARE_TEXT_KL = "CommissionItem.shareTextKl";

    public final static String USER_NOT_EXISTS = "user.not.exists";

    public final static String PHONE_VERIFY_CODE_INVALID = "phone.verify.code.invalid";

    public final static String TAOBAO_NEWER_SHARE_TEXT = "TaobaoNewer.shareText";
    public final static String TAOBAO_NEWER_SHARE_TEXT_KL = "TaobaoNewer.shareTextKl";

    public final static String TB_PULL_NEW_DETAIL = "TbPullNewDetail.status";

    public final static String INVITATION_CODE_NOT_EXISTS = "invitation.code.not.exists";

    public final static String USER_PHONE_EXISTS = "user.phone.exists";

    public final static String WEIXIN_HAS_BOUND = "weixin.has.bound";

    public final static String USER_AGENT_FREE_APPLICATION_DAY_OVER = "user.agent.free.application.day.over";

    public final static String REGISTER_INVITATION_CODE_IS_NOT_AGENT = "register.invitation.code.is.not.agent";

    public final static String NO_ENOUGH_MONEY_FOR_WITHDRAW = "no.enough.money.for.withdraw";
    public final static String LESS_MONEY_FOR_WITHDRAW = "less.money.for.withdraw";

    public final static String USER_AGENT_APPLY_FAIL = "user.agent.apply.fail";

    public final static String NO_ENOUGH_MONEY = "no.enough.money";
    public final static String NO_ENOUGH_INTEGRAL = "no.enough.integral";

    public final static String OLD_PASSWORD_INCORRECT = "old.password.incorrect";

    public final static String UPGRADE_USER_LEVEL_FAIL_BY_INVITATION = "upgrade.user.level.fail.by.invitation";
    public final static String UPGRADE_USER_LEVEL_FAIL_BY_DIRECT_ORDER_NUM = "upgrade.user.level.fail.by.direct.order.num";
    public final static String UPGRADE_USER_LEVEL_FAIL_BY_CHILDREN_NUM = "upgrade.user.level.fail.by.children.num";
    public final static String UPGRADE_USER_LEVEL_FAIL_BY_TEAM_NUM = "upgrade.user.level.fail.by.team.num";
    public final static String PAID_BY_INTEGRAL_IS_NOT_UNAVAILABLE = "paid.by.integral.is.unavailable";

    public final static String USER_LEVEL_UPGRADE_ORDER_STATUS_IS_NOT_WAIT_PAY = "user.level.upgrade.order.status.is.not.wait.pay";
    public final static String ORDER_STATUS_IS_NOT_WAIT_PAY = "order.status.is.not.wait.pay";
    public final static String SELLER_PROMOTION_ITEM_CAN_NOT_PAY = "seller.promotion.item.can.not.pay";

    public final static String SELLER_PROMOTION_ITEM_DEPOSIT_ORDER_EXISTS = "seller.promotion.item.deposit.order.exists";
    public final static String ALIPAY_TRADE_NO_EXISTS = "alipay.trade.no.exists";

    public final static String SELLER_PROMOTION_ITEM_RUSH_BUY_IN_LOCKED = "seller.promotion.item.rush.buy.in.locked";
    public final static String SELLER_PROMOTION_ITEM_RUSH_BUY_NOT_START = "seller.promotion.item.rush.buy.not.start";
    public final static String SELLER_PROMOTION_ITEM_RUSH_BUY_LIMITED = "seller.promotion.item.rush.buy.limited";
    public final static String SELLER_PROMOTION_ITEM_RUSH_BUY_NO_ENOUGH_NUM = "seller.promotion.item.rush.buy.no.enough.num";

    public final static String SELLER_PROMOTION_ITEM_ORDER_FILL_TRADE_NO_FAIL = "seller.promotion.item.order.fill.trade.no.fail";
    public final static String SELLER_PROMOTION_ITEM_ORDER_TRADE_NO_EXISTS = "seller.promotion.item.order.trade.no.exists";
    public final static String SELLER_PROMOTION_ITEM_ORDER_STATUS_NOT_ALLOWED_REBATE = "seller.promotion.item.order.status.not.allowed.rebate";

    public final static String FLASH_SALE_ITEM_EXISTS_IN_SEASON = "flash.sale.item.exists.in.season";

    public final static String AGENT_APPLY_IS_WAIT_VERIFY = "agent.apply.is.wait.verify";

    public final static String USER_WITHDRAW_REMARK = "user.withdraw.remark";

    public final static String WITHDRAW_IS_CLOSED = "withdraw.is.closed";

    public final static String FREE_CHARGE_ACTIVITY_STOPPED = "free.charge.activity.stopped";
    public final static String FREE_CHARGE_ACTIVITY_NO_THAT_ITEM = "free.charge.activity.no.that.item";
    public final static String FREE_CHARGE_ACTIVITY_HAS_BEEN_DRAW = "free.charge.activity.has.been.draw";
    public final static String WEIXIN_NEITHER_REGISTER_NOR_BIND = "weixin.neither.register.nor.bind";

    public final static String BAD_CREDENTIALS = "BadCredentials";
}
