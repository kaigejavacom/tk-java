package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.FreeChargeActivity;
import info.batcloud.fanli.core.enums.FreeChargeActivityStatus;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;

public interface FreeChargeActivityRepository extends PagingAndSortingRepository<FreeChargeActivity, Long>, JpaSpecificationExecutor<FreeChargeActivity> {

    List<FreeChargeActivity> findByStartTimeLessThanEqualAndEndTimeGreaterThanEqualAndStatus(Date startTime, Date endTime, FreeChargeActivityStatus status);
    FreeChargeActivity findByIdAndStartTimeLessThanEqualAndEndTimeGreaterThanEqualAndStatus(long id, Date startTime, Date endTime, FreeChargeActivityStatus status);

}
