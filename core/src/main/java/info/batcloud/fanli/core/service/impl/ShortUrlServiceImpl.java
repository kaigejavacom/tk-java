package info.batcloud.fanli.core.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import info.batcloud.fanli.core.service.ShortUrlService;
import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.settings.OpenWeiboSetting;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.IOException;
import java.net.URL;
import java.util.List;

@Service
public class ShortUrlServiceImpl implements ShortUrlService {

    private static final Logger logger = LoggerFactory.getLogger(ShortUrlServiceImpl.class);

    private static final String SHORT_URL_API = "http://api.weibo.com/2/short_url/shorten.json?source={source}&url_long={url_long}";

    @Inject
    private SystemSettingService systemSettingService;

    @Override
    public String generate(String url) {
        OpenWeiboSetting settings = systemSettingService.findActiveSetting(OpenWeiboSetting.class);
        String api = SHORT_URL_API.replace("{source}", settings.getShortUrlSource())
                .replace("{url_long}", url);
        try {
            String responseText = IOUtils.toString(new URL(api), "utf8");
            GenerateResult rs = JSON.parseObject(responseText, GenerateResult.class);
            return rs.getUrls().get(0).getUrlShort().replace("http://", "https://");
        } catch (IOException e) {
            logger.error("调用短链接生成器失败");
        }
        return null;
    }

    public static class GenerateResult {

        public List<ShortUrl> urls;

        public List<ShortUrl> getUrls() {
            return urls;
        }

        public void setUrls(List<ShortUrl> urls) {
            this.urls = urls;
        }

        public static class ShortUrl {
            private boolean result;
            @JSONField(name = "url_short")
            private String urlShort;
            @JSONField(name = "url_long")
            private String urlLong;
            @JSONField(name = "object_type")
            private String objectType;
            private int type;
            @JSONField(name = "object_id")
            private String objectId;

            public boolean isResult() {
                return result;
            }

            public void setResult(boolean result) {
                this.result = result;
            }

            public String getUrlShort() {
                return urlShort;
            }

            public void setUrlShort(String urlShort) {
                this.urlShort = urlShort;
            }

            public String getUrlLong() {
                return urlLong;
            }

            public void setUrlLong(String urlLong) {
                this.urlLong = urlLong;
            }

            public String getObjectType() {
                return objectType;
            }

            public void setObjectType(String objectType) {
                this.objectType = objectType;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }

            public String getObjectId() {
                return objectId;
            }

            public void setObjectId(String objectId) {
                this.objectId = objectId;
            }
        }
    }
}
