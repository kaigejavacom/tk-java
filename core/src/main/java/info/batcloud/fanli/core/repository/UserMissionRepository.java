package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.UserMission;
import info.batcloud.fanli.core.enums.MissionType;
import info.batcloud.fanli.core.enums.UserMissionStatus;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface UserMissionRepository extends CrudRepository<UserMission, Long>{

    @Query("select um from UserMission um join fetch um.mission where um.userId=?1 and um.mission.type=?2 and um.status=?3")
    List<UserMission> findByUserIdAndMissionTypeAndStatus(long userId, MissionType type, UserMissionStatus status);

    @Query("select um from UserMission um join fetch um.mission where um.userId=?1 and um.status=?2")
    List<UserMission> findByUserIdAndStatus(long userId, UserMissionStatus status);

    @Query("select um from UserMission um join fetch um.mission where um.userId=?1 and um.status<>?2")
    List<UserMission> findByUserIdAndStatusNot(long userId, UserMissionStatus status);

    @Query("select um from UserMission um join fetch um.mission where um.userId=?1 and um.status=?2 or um.completeTime between ?3 and ?4")
    List<UserMission> findByUserIdAndStatusOrCompleteTimeBetween(long userId, UserMissionStatus status,
                                                                  Date completeStartTime, Date completeEndTime);
}
