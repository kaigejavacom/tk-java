package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.UserRedPacketDTO;

public interface UserRedPacketService {

    Paging<UserRedPacketDTO> search(SearchParam searchParam);

    void addUserRedPacket(UserRedPacketAddParam param);

    void drawByIdAndUserId(long id, long userId);

    class UserRedPacketAddParam {
        private Long userId;

        private Long redPacketId;

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Long getRedPacketId() {
            return redPacketId;
        }

        public void setRedPacketId(Long redPacketId) {
            this.redPacketId = redPacketId;
        }
    }

    class SearchParam extends PagingParam {
        private Long userId;
        private Boolean draw;
        private Long redPacketId;

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Boolean getDraw() {
            return draw;
        }

        public void setDraw(Boolean draw) {
            this.draw = draw;
        }

        public Long getRedPacketId() {
            return redPacketId;
        }

        public void setRedPacketId(Long redPacketId) {
            this.redPacketId = redPacketId;
        }
    }

}
