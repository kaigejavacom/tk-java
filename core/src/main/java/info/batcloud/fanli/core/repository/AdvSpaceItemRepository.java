package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.AdvSpaceItem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AdvSpaceItemRepository extends CrudRepository<AdvSpaceItem, Long> {

    List<AdvSpaceItem> findByAdvSpaceId(long advSpaceId);

    int countByAdvSpaceId(long advSpaceId);
}
