package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.CommissionItemDTO;
import info.batcloud.fanli.core.dto.CommissionItemFetchDTO;
import info.batcloud.fanli.core.dto.CommissionItemUpdateDTO;
import info.batcloud.fanli.core.domain.CommissionItemBuyParams;
import info.batcloud.fanli.core.entity.CommissionItem;
import info.batcloud.fanli.core.enums.CommissionItemSort;
import info.batcloud.fanli.core.enums.CommissionItemStatus;
import info.batcloud.fanli.core.enums.EcomPlat;

import java.util.List;

public interface CommissionItemService {

    CommissionItemDTO saveCommissionItem(CommissionItemFetchDTO item);

    List<CommissionItemDTO> saveCommissionItemList(List<CommissionItemFetchDTO> itemList);

    List<CommissionItemDTO> saveCommissionItemListSync(List<CommissionItemFetchDTO> itemList);

    List<CommissionItemDTO> findByIdList(List<Long> ids);

    void updateCommissionItem(long itemId, CommissionItemUpdateDTO item);

    Paging<CommissionItemDTO> search(SearchParam param);

    void pushDocumentByItemIdList(List<Long> itemIds);

    ShareInfo findShareInfo(long couponItemId, Long userId);

    ShareInfo findShareInfo(CommissionItemDTO ci, Long userId);

    void rePushDocument();

    CommissionItemDTO findById(long id);

    CommissionItemDTO findBySourceItemIdAndEcomPlat(long sourceItemId, EcomPlat ecomPlat);
    CommissionItemDTO findOrFetchBySourceItemIdAndEcomPlat(long sourceItemId, EcomPlat ecomPlat);
    CommissionItemDTO findOnlineBySourceItemIdAndEcomPlat(long sourceItemId, EcomPlat ecomPlat);

    CommissionItemFetchDTO fetchOnlineBySourceItemIdAndEcomPlat(long sourceItemId, EcomPlat ecomPlat);

    /**
     * 清理商品，过期商品下架
     */
    void clean();

    void clean(SearchParam param);

    void setStatus(CommissionItemStatus status, List<Long> ids);

    List<String> findTitleSegmentById(long commissionItemId);

    CommissionItemDTO toCommissionItemDto(CommissionItem commissionItem);

    float cacheCommissionRateByEcomPlatAndSourceItemId(EcomPlat ecomPlat, long sourceItemId, float commissionRate);

    CommissionItemFetchDTO cacheCommissionItemByEcomPlatAndSourceItemId(EcomPlat ecomPlat, long sourceItemId, CommissionItemFetchDTO commissionItemBO);

    void cacheCommissionItemList(List<CommissionItemFetchDTO> list);

    <T> CommissionItemBuyParams<T> findBuyParams(long id, Long userId);

    float findShareCommission(float price, float commissionRate);

    class SearchParam extends PagingParam {

        private Float startPrice;
        private Float endPrice;

        private Integer maxSales;
        private Integer minSales;

        private CommissionItemSort sort;

        private String catPath;
        private String keywords;

        private Float maxCouponValue;

        private Boolean coupon;

        private CommissionItemStatus status;

        public Float getMaxCouponValue() {
            return maxCouponValue;
        }

        public void setMaxCouponValue(Float maxCouponValue) {
            this.maxCouponValue = maxCouponValue;
        }

        public Boolean getCoupon() {
            return coupon;
        }

        public void setCoupon(Boolean coupon) {
            this.coupon = coupon;
        }

        public Integer getMinSales() {
            return minSales;
        }

        public void setMinSales(Integer minSales) {
            this.minSales = minSales;
        }

        public Integer getMaxSales() {
            return maxSales;
        }

        public void setMaxSales(Integer maxSales) {
            this.maxSales = maxSales;
        }

        public CommissionItemStatus getStatus() {
            return status;
        }

        public void setStatus(CommissionItemStatus status) {
            this.status = status;
        }

        public String getKeywords() {
            return keywords;
        }

        public void setKeywords(String keywords) {
            this.keywords = keywords;
        }

        public String getCatPath() {
            return catPath;
        }

        public void setCatPath(String catPath) {
            this.catPath = catPath;
        }

        public CommissionItemSort getSort() {
            return sort;
        }

        public void setSort(CommissionItemSort sort) {
            this.sort = sort;
        }

        public Float getStartPrice() {
            return startPrice;
        }

        public void setStartPrice(Float startPrice) {
            this.startPrice = startPrice;
        }

        public Float getEndPrice() {
            return endPrice;
        }

        public void setEndPrice(Float endPrice) {
            this.endPrice = endPrice;
        }
    }

    class ShareInfo {
        private String ecomPlatTitle;
        private String title;
        private float price;
        private float originPrice;
        private String kl;
        private String url;
        private String shareText;
        private long userId;
        private String clickUrl;
        private String shortUrl;
        private String taobaoRelationId;

        public String getTaobaoRelationId() {
            return taobaoRelationId;
        }

        public void setTaobaoRelationId(String taobaoRelationId) {
            this.taobaoRelationId = taobaoRelationId;
        }

        public String getShortUrl() {
            return shortUrl;
        }

        public void setShortUrl(String shortUrl) {
            this.shortUrl = shortUrl;
        }

        public String getEcomPlatTitle() {
            return ecomPlatTitle;
        }

        public void setEcomPlatTitle(String ecomPlatTitle) {
            this.ecomPlatTitle = ecomPlatTitle;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public float getPrice() {
            return price;
        }

        public void setPrice(float price) {
            this.price = price;
        }

        public float getOriginPrice() {
            return originPrice;
        }

        public void setOriginPrice(float originPrice) {
            this.originPrice = originPrice;
        }

        public String getClickUrl() {
            return clickUrl;
        }

        public void setClickUrl(String clickUrl) {
            this.clickUrl = clickUrl;
        }

        public long getUserId() {
            return userId;
        }

        public void setUserId(long userId) {
            this.userId = userId;
        }

        public String getShareText() {
            return shareText;
        }

        public void setShareText(String shareText) {
            this.shareText = shareText;
        }

        public String getKl() {
            return kl;
        }

        public void setKl(String kl) {
            this.kl = kl;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}
