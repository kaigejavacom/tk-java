package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.FreeChargeActivityOrderDTO;
import info.batcloud.fanli.core.enums.FreeChargeActivityOrderStatus;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

public interface FreeChargeActivityOrderService {

    long createFreeChargeActivityOrder(FreeChargeActivityOrderCreateParam param);

    long countFreeChargeActivityByUserIdAndActivityId(long userId, long freeChargeActivityId);

    long countFreeChargeActivityByUserIdAndActivityIdAndCreateTimeBetween(long userId, long freeChargeActivityId, Date startTime, Date endTime);

    boolean checkHasWaitVerifyOrderByUserIdAndItemId(long userId, long itemId);

    void settle();
    void settle(List<Long> ids);

    void deleteById(long id);

    Paging<FreeChargeActivityOrderDTO> search(SearchParam param);

    class SearchParam extends PagingParam {
        private Long userId;
        private Long itemId;
        private Long freeChargeActivityId;
        private Long userCommissionOrderId;
        private String orderNo;
        private FreeChargeActivityOrderStatus status;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date minCreateTime;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date maxCreateTime;

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public Date getMinCreateTime() {
            return minCreateTime;
        }

        public void setMinCreateTime(Date minCreateTime) {
            this.minCreateTime = minCreateTime;
        }

        public Date getMaxCreateTime() {
            return maxCreateTime;
        }

        public void setMaxCreateTime(Date maxCreateTime) {
            this.maxCreateTime = maxCreateTime;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Long getItemId() {
            return itemId;
        }

        public void setItemId(Long itemId) {
            this.itemId = itemId;
        }

        public Long getFreeChargeActivityId() {
            return freeChargeActivityId;
        }

        public void setFreeChargeActivityId(Long freeChargeActivityId) {
            this.freeChargeActivityId = freeChargeActivityId;
        }

        public Long getUserCommissionOrderId() {
            return userCommissionOrderId;
        }

        public void setUserCommissionOrderId(Long userCommissionOrderId) {
            this.userCommissionOrderId = userCommissionOrderId;
        }

        public FreeChargeActivityOrderStatus getStatus() {
            return status;
        }

        public void setStatus(FreeChargeActivityOrderStatus status) {
            this.status = status;
        }
    }

    class FreeChargeActivityOrderCreateParam {
        private Long userId;
        private Long itemId;
        private Long freeChargeActivityId;

        public Long getFreeChargeActivityId() {
            return freeChargeActivityId;
        }

        public void setFreeChargeActivityId(Long freeChargeActivityId) {
            this.freeChargeActivityId = freeChargeActivityId;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Long getItemId() {
            return itemId;
        }

        public void setItemId(Long itemId) {
            this.itemId = itemId;
        }
    }

}
