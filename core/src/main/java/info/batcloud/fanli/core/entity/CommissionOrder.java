package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.CommissionOrderStatus;
import info.batcloud.fanli.core.enums.EcomPlat;

import javax.persistence.*;
import java.util.Date;

@Entity
public class CommissionOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String itemPicUrl;

    private Long commissionItemId;

    private Date createTime;

    private Date settledTime;

    private String itemTitle;

    private String itemId;

    private String shopTitle;

    private int itemNum;

    private Float price;

    @Enumerated(EnumType.STRING)
    private CommissionOrderStatus status;

    @Enumerated(EnumType.STRING)
    private EcomPlat ecomPlat;

    /**
     * 预估佣金金额
     */
    private Float estimateCommissionFee; //预估佣金

    /**
     * 预估佣金比例
     */
    private Float estimateCommissionRate;

    private Float commissionRate;

    private Float commissionFee;

    private Float payFee;

    private String orderNo;

    private Long userId;

    @Version
    private Integer version;

    private Float allotFee;

    private Date platSettledTime;

    private boolean allocated;

    public boolean isAllocated() {
        return allocated;
    }

    public void setAllocated(boolean allocated) {
        this.allocated = allocated;
    }

    public Date getPlatSettledTime() {
        return platSettledTime;
    }

    public void setPlatSettledTime(Date platSettledTime) {
        this.platSettledTime = platSettledTime;
    }

    public Float getAllotFee() {
        return allotFee;
    }

    public void setAllotFee(Float allotFee) {
        this.allotFee = allotFee;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Date getSettledTime() {
        return settledTime;
    }

    public void setSettledTime(Date settledTime) {
        this.settledTime = settledTime;
    }

    public Long getCommissionItemId() {
        return commissionItemId;
    }

    public void setCommissionItemId(Long commissionItemId) {
        this.commissionItemId = commissionItemId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getItemPicUrl() {
        return itemPicUrl;
    }

    public void setItemPicUrl(String itemPicUrl) {
        this.itemPicUrl = itemPicUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public void setShopTitle(String shopTitle) {
        this.shopTitle = shopTitle;
    }

    public int getItemNum() {
        return itemNum;
    }

    public void setItemNum(int itemNum) {
        this.itemNum = itemNum;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public CommissionOrderStatus getStatus() {
        return status;
    }

    public void setStatus(CommissionOrderStatus status) {
        this.status = status;
    }

    public EcomPlat getEcomPlat() {
        return ecomPlat;
    }

    public void setEcomPlat(EcomPlat ecomPlat) {
        this.ecomPlat = ecomPlat;
    }

    public Float getEstimateCommissionFee() {
        return estimateCommissionFee;
    }

    public void setEstimateCommissionFee(Float estimateCommissionFee) {
        this.estimateCommissionFee = estimateCommissionFee;
    }

    public Float getEstimateCommissionRate() {
        return estimateCommissionRate;
    }

    public void setEstimateCommissionRate(Float estimateCommissionRate) {
        this.estimateCommissionRate = estimateCommissionRate;
    }

    public Float getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(Float commissionRate) {
        this.commissionRate = commissionRate;
    }

    public Float getCommissionFee() {
        return commissionFee;
    }

    public void setCommissionFee(Float commissionFee) {
        this.commissionFee = commissionFee;
    }

    public Float getPayFee() {
        return payFee;
    }

    public void setPayFee(Float payFee) {
        this.payFee = payFee;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

}
