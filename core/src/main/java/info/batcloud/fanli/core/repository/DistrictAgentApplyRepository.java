package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.DistrictAgentApply;
import info.batcloud.fanli.core.enums.AgentApplyStatus;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface DistrictAgentApplyRepository extends PagingAndSortingRepository<DistrictAgentApply, Long>, JpaSpecificationExecutor<DistrictAgentApply> {

    int countByUserIdAndStatus(long userId, AgentApplyStatus status);

}
