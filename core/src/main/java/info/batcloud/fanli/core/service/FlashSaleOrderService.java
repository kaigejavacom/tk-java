package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.FlashSaleOrderDTO;
import info.batcloud.fanli.core.enums.FlashSaleOrderStatus;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

public interface FlashSaleOrderService {

    long createFlashSaleOrder(FlashSaleOrderCreateParam param);

    long countFlashSaleItemByUserIdAndFlashSaleItemId(long userId, long flashSaleItemId);

    long countFlashSaleItemByUserIdAndFlashSaleItemIdAndCreateTimeBetween(long userId, long flashSaleItemId, Date startTime, Date endTime);

    boolean checkHasWaitVerifyOrderByUserIdAndItemId(long userId, long itemId);

    void settle();
    void settle(List<Long> ids);

    void deleteById(long id);

    Paging<FlashSaleOrderDTO> search(SearchParam param);

    class SearchParam extends PagingParam {
        private Long userId;
        private Long itemId;
        private Long flashSaleItemId;
        private String date;
        private String season;
        private Long userCommissionOrderId;
        private String orderNo;
        private FlashSaleOrderStatus status;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date minCreateTime;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date maxCreateTime;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getSeason() {
            return season;
        }

        public void setSeason(String season) {
            this.season = season;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public Date getMinCreateTime() {
            return minCreateTime;
        }

        public void setMinCreateTime(Date minCreateTime) {
            this.minCreateTime = minCreateTime;
        }

        public Date getMaxCreateTime() {
            return maxCreateTime;
        }

        public void setMaxCreateTime(Date maxCreateTime) {
            this.maxCreateTime = maxCreateTime;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Long getItemId() {
            return itemId;
        }

        public void setItemId(Long itemId) {
            this.itemId = itemId;
        }

        public Long getFlashSaleItemId() {
            return flashSaleItemId;
        }

        public void setFlashSaleItemId(Long flashSaleItemId) {
            this.flashSaleItemId = flashSaleItemId;
        }

        public Long getUserCommissionOrderId() {
            return userCommissionOrderId;
        }

        public void setUserCommissionOrderId(Long userCommissionOrderId) {
            this.userCommissionOrderId = userCommissionOrderId;
        }

        public FlashSaleOrderStatus getStatus() {
            return status;
        }

        public void setStatus(FlashSaleOrderStatus status) {
            this.status = status;
        }
    }

    class FlashSaleOrderCreateParam {
        private Long userId;
        private Long itemId;
        private Long flashSaleItemId;

        public Long getFlashSaleItemId() {
            return flashSaleItemId;
        }

        public void setFlashSaleItemId(Long flashSaleItemId) {
            this.flashSaleItemId = flashSaleItemId;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Long getItemId() {
            return itemId;
        }

        public void setItemId(Long itemId) {
            this.itemId = itemId;
        }
    }

}
