package info.batcloud.fanli.core.settings;

import java.io.Serializable;

public class CommissionSettlementSetting implements Serializable {

    private float rewardCommissionRate;//奖励佣金比例
    private boolean useIntegralToReward; //奖励消耗积分

    public boolean isUseIntegralToReward() {
        return useIntegralToReward;
    }

    public void setUseIntegralToReward(boolean useIntegralToReward) {
        this.useIntegralToReward = useIntegralToReward;
    }

    public float getRewardCommissionRate() {
        return rewardCommissionRate;
    }

    public void setRewardCommissionRate(float rewardCommissionRate) {
        this.rewardCommissionRate = rewardCommissionRate;
    }

}
