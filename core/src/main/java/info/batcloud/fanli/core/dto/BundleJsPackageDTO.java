package info.batcloud.fanli.core.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import info.batcloud.fanli.core.enums.AppPlatform;
import info.batcloud.fanli.core.enums.BundleJsPackageStatus;

import java.io.Serializable;
import java.util.Date;

public class BundleJsPackageDTO implements Serializable {

    private Long id;
    private AppPlatform platform;
    private boolean useCache;
    private String platformVersion;
    private boolean iosShow;

    private BundleJsPackageStatus status;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private String version;

    private boolean showUpdate;

    public boolean isShowUpdate() {
        return showUpdate;
    }

    public void setShowUpdate(boolean showUpdate) {
        this.showUpdate = showUpdate;
    }

    public boolean isIosShow() {
        return iosShow;
    }

    public void setIosShow(boolean iosShow) {
        this.iosShow = iosShow;
    }

    public boolean isUseCache() {
        return useCache;
    }

    public void setUseCache(boolean useCache) {
        this.useCache = useCache;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AppPlatform getPlatform() {
        return platform;
    }

    public void setPlatform(AppPlatform platform) {
        this.platform = platform;
    }

    public String getPlatformVersion() {
        return platformVersion;
    }

    public void setPlatformVersion(String platformVersion) {
        this.platformVersion = platformVersion;
    }

    public BundleJsPackageStatus getStatus() {
        return status;
    }

    public void setStatus(BundleJsPackageStatus status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
