package info.batcloud.fanli.core.service;

import java.util.Date;

public interface TaobaoAccountService {

    void saveUserTaobaoAccount(long userId, TaobaoAccountSaveParam param);

    String findUserAccessToken(long userId);

    class TaobaoAccountSaveParam {
        private String taobaoUserNick;

        private String accessToken;

        private String refreshToken;

        private Long taobaoUserId;

        private String taobaoOpenId;

        private Date expiresIn;

        public Date getExpiresIn() {
            return expiresIn;
        }

        public void setExpiresIn(Date expiresIn) {
            this.expiresIn = expiresIn;
        }

        public String getTaobaoUserNick() {
            return taobaoUserNick;
        }

        public void setTaobaoUserNick(String taobaoUserNick) {
            this.taobaoUserNick = taobaoUserNick;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public String getRefreshToken() {
            return refreshToken;
        }

        public void setRefreshToken(String refreshToken) {
            this.refreshToken = refreshToken;
        }

        public Long getTaobaoUserId() {
            return taobaoUserId;
        }

        public void setTaobaoUserId(Long taobaoUserId) {
            this.taobaoUserId = taobaoUserId;
        }

        public String getTaobaoOpenId() {
            return taobaoOpenId;
        }

        public void setTaobaoOpenId(String taobaoOpenId) {
            this.taobaoOpenId = taobaoOpenId;
        }
    }

}
