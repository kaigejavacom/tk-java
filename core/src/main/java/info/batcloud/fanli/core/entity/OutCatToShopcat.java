package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.ItemSelectionPlat;

import javax.persistence.*;

@Entity
public class OutCatToShopcat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String catName;

    private Long catId;

    private Long shopcatId;

    @Enumerated(EnumType.STRING)
    private ItemSelectionPlat plat;

    public ItemSelectionPlat getPlat() {
        return plat;
    }

    public void setPlat(ItemSelectionPlat plat) {
        this.plat = plat;
    }

    public String getPlatTitle() {
        return plat == null ? null : plat.getTitle();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public Long getCatId() {
        return catId;
    }

    public void setCatId(Long catId) {
        this.catId = catId;
    }

    public Long getShopcatId() {
        return shopcatId;
    }

    public void setShopcatId(Long shopcatId) {
        this.shopcatId = shopcatId;
    }
}
