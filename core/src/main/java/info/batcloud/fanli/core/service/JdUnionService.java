package info.batcloud.fanli.core.service;

public interface JdUnionService {

    void fetchOrder(String time);

    boolean refreshAccessToken();

    String getCouponUrl(long userId, long itemId, String couponClickUrl);
}
