package info.batcloud.fanli.core.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import info.batcloud.fanli.core.enums.FlashSaleOrderStatus;

import java.util.Date;

public class FlashSaleOrderDTO {

    private Long id;

    private Long userId;

    private String userNickname;

    private String userAvatarUrl;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    private Long itemId;

    private String itemPicUrl;

    private String itemShopTitle;

    private String itemTitle;

    private Float itemPrice;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    private Long userCommissionOrderId;

    private FlashSaleOrderStatus status;

    private Long flashSaleItemId;

    private String flashSaleItemDate;

    private String flashSaleItemSeason;

    private Float freeFee;

    private String orderNo;

    public Long getFlashSaleItemId() {
        return flashSaleItemId;
    }

    public void setFlashSaleItemId(Long flashSaleItemId) {
        this.flashSaleItemId = flashSaleItemId;
    }

    public String getFlashSaleItemDate() {
        return flashSaleItemDate;
    }

    public void setFlashSaleItemDate(String flashSaleItemDate) {
        this.flashSaleItemDate = flashSaleItemDate;
    }

    public String getFlashSaleItemSeason() {
        return flashSaleItemSeason;
    }

    public void setFlashSaleItemSeason(String flashSaleItemSeason) {
        this.flashSaleItemSeason = flashSaleItemSeason;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Float getFreeFee() {
        return freeFee;
    }

    public void setFreeFee(Float freeFee) {
        this.freeFee = freeFee;
    }

    public String getItemShopTitle() {
        return itemShopTitle;
    }

    public void setItemShopTitle(String itemShopTitle) {
        this.itemShopTitle = itemShopTitle;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserNickname() {
        return userNickname;
    }

    public void setUserNickname(String userNickname) {
        this.userNickname = userNickname;
    }

    public String getUserAvatarUrl() {
        return userAvatarUrl;
    }

    public void setUserAvatarUrl(String userAvatarUrl) {
        this.userAvatarUrl = userAvatarUrl;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getItemPicUrl() {
        return itemPicUrl;
    }

    public void setItemPicUrl(String itemPicUrl) {
        this.itemPicUrl = itemPicUrl;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public Float getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(Float itemPrice) {
        this.itemPrice = itemPrice;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUserCommissionOrderId() {
        return userCommissionOrderId;
    }

    public void setUserCommissionOrderId(Long userCommissionOrderId) {
        this.userCommissionOrderId = userCommissionOrderId;
    }

    public FlashSaleOrderStatus getStatus() {
        return status;
    }

    public String getStatusTitle() {
        return status == null ? null : status.getTitle();
    }

    public void setStatus(FlashSaleOrderStatus status) {
        this.status = status;
    }

}
