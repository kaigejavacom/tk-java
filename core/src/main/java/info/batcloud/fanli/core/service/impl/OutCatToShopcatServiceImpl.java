package info.batcloud.fanli.core.service.impl;

import info.batcloud.fanli.core.entity.OutCatToShopcat;
import info.batcloud.fanli.core.repository.OutCatToShopcatRepository;
import info.batcloud.fanli.core.service.OutCatToShopcatService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service
public class OutCatToShopcatServiceImpl implements OutCatToShopcatService {

    @Inject
    private OutCatToShopcatRepository outCatToShopcatRepository;

    @Override
    public void addOutCatToShopcat(AddParam param) {
        OutCatToShopcat octs = new OutCatToShopcat();
        BeanUtils.copyProperties(param, octs);
        outCatToShopcatRepository.save(octs);
    }

    @Override
    public void deleteById(long id) {
        outCatToShopcatRepository.delete(id);
    }

    @Override
    public List<OutCatToShopcat> listByShopcatId(long shopcatId) {
        return outCatToShopcatRepository.findByShopcatId(shopcatId);
    }
}
