package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.SellerPromotionItemDepositOrderStatus;
import info.batcloud.fanli.core.enums.WalletFlowDetailType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
@DiscriminatorValue("SellerPromotionItemDepositOrder")
public class SellerPromotionItemDepositOrder extends Order{

    private Long sellerPromotionItemId;

    @Enumerated(EnumType.STRING)
    private SellerPromotionItemDepositOrderStatus status;

    //充值数量
    private int num;

    private float rebateFee;

    private String verifyRemark;

    public String getVerifyRemark() {
        return verifyRemark;
    }

    public void setVerifyRemark(String verifyRemark) {
        this.verifyRemark = verifyRemark;
    }

    public float getRebateFee() {
        return rebateFee;
    }

    public void setRebateFee(float rebateFee) {
        this.rebateFee = rebateFee;
    }

    public SellerPromotionItemDepositOrderStatus getStatus() {
        return status;
    }

    public void setStatus(SellerPromotionItemDepositOrderStatus status) {
        this.status = status;
    }

    public Long getSellerPromotionItemId() {
        return sellerPromotionItemId;
    }

    public void setSellerPromotionItemId(Long sellerPromotionItemId) {
        this.sellerPromotionItemId = sellerPromotionItemId;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    @Override
    public WalletFlowDetailType walletFlowDetailType() {
        return WalletFlowDetailType.SELLER_PROMOTION_ITEM_DEPOSIT;
    }
}
