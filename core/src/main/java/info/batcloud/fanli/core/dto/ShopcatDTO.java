package info.batcloud.fanli.core.dto;

import info.batcloud.fanli.core.enums.ShopcatStatus;

import java.io.Serializable;
import java.util.List;

public class ShopcatDTO implements Serializable {

    private long id;
    private String title;
    private String keyword;
    private String iconUrl;
    private List<ShopcatDTO> children;
    private String path;
    private Long parentId;
    private int idx;

    private Long taobaoCatId;
    private Long jdCatId;

    private Long pddCatId;

    private ShopcatDTO ref;

    private boolean searchByKeyword;

    private String pathName;

    private ShopcatStatus status;

    public ShopcatStatus getStatus() {
        return status;
    }

    public void setStatus(ShopcatStatus status) {
        this.status = status;
    }

    public String getStatusTitle() {
        return status == null ? null : status.getTitle();
    }

    public String getPathName() {
        return pathName;
    }

    public void setPathName(String pathName) {
        this.pathName = pathName;
    }

    public ShopcatDTO getRef() {
        return ref;
    }

    public void setRef(ShopcatDTO ref) {
        this.ref = ref;
    }

    public Long getPddCatId() {
        return pddCatId;
    }

    public void setPddCatId(Long pddCatId) {
        this.pddCatId = pddCatId;
    }

    public Long getJdCatId() {
        return jdCatId;
    }

    public void setJdCatId(Long jdCatId) {
        this.jdCatId = jdCatId;
    }

    public Long getTaobaoCatId() {
        return taobaoCatId;
    }

    public void setTaobaoCatId(Long taobaoCatId) {
        this.taobaoCatId = taobaoCatId;
    }

    public boolean isSearchByKeyword() {
        return searchByKeyword;
    }

    public void setSearchByKeyword(boolean searchByKeyword) {
        this.searchByKeyword = searchByKeyword;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public List<ShopcatDTO> getChildren() {
        return children;
    }

    public void setChildren(List<ShopcatDTO> children) {
        this.children = children;
    }
}
