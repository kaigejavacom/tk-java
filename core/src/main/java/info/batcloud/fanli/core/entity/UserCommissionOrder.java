package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.CommissionAllotType;

import javax.persistence.*;
import java.util.Date;

@Entity
public class UserCommissionOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long userId;

    @ManyToOne(cascade = CascadeType.REFRESH)
    private CommissionOrder commissionOrder;

    //分佣比例
    private Float commissionRate;

    //结算金额，为 settledCommissionFee + settledRewardFee
    private Float settledFee;

    private Float settledCommissionFee;

    private Float settledRewardFee;

    private Date createTime;

    private Date settledTime;

    @Enumerated(EnumType.STRING)
    private CommissionAllotType allotType;

    @Version
    private Integer version;

    private float commissionRewardRate;

    public Float getSettledCommissionFee() {
        return settledCommissionFee;
    }

    public void setSettledCommissionFee(Float settledCommissionFee) {
        this.settledCommissionFee = settledCommissionFee;
    }

    public Float getSettledRewardFee() {
        return settledRewardFee;
    }

    public void setSettledRewardFee(Float settledRewardFee) {
        this.settledRewardFee = settledRewardFee;
    }

    public float getCommissionRewardRate() {
        return commissionRewardRate;
    }

    public void setCommissionRewardRate(float commissionRewardRate) {
        this.commissionRewardRate = commissionRewardRate;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public CommissionAllotType getAllotType() {
        return allotType;
    }

    public void setAllotType(CommissionAllotType allotType) {
        this.allotType = allotType;
    }

    public Date getSettledTime() {
        return settledTime;
    }

    public void setSettledTime(Date settledTime) {
        this.settledTime = settledTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Float getSettledFee() {
        return settledFee;
    }

    public void setSettledFee(Float settledFee) {
        this.settledFee = settledFee;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public CommissionOrder getCommissionOrder() {
        return commissionOrder;
    }

    public void setCommissionOrder(CommissionOrder commissionOrder) {
        this.commissionOrder = commissionOrder;
    }

    public Float getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(Float commissionRate) {
        this.commissionRate = commissionRate;
    }
}
