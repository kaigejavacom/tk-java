package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.UserItemSelection;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserItemSelectionRepository extends PagingAndSortingRepository<UserItemSelection, Long>, JpaSpecificationExecutor<UserItemSelection> {

    @Modifying
    void deleteByUserIdAndId(long userId, long id);

}
