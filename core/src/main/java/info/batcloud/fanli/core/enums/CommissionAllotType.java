package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

/**
 * 佣金分配类型
 * */
public enum CommissionAllotType implements EnumTitle{

    SELF_BUY, DIRECT, INDIRECT, CARRIER, CHIEF, CITY_AGENT, DISTRICT_AGENT, RELATION_AGENT;

    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, "", null);
    }

}
