package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.User;
import info.batcloud.fanli.core.enums.UserLevel;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface UserRepository extends PagingAndSortingRepository<User, Long>, JpaSpecificationExecutor<User> {

    User findByPhone(String phone);

    User findByOpenId(String openId);

    User findByPhoneAndDeleted(String phone, boolean deleted);

    User findByInvitationCode(String invitationCode);

    int countByInvitationCode(String invitationCode);

    int countByPhone(String phone);

    User findByTaobaoAdzoneId(long taobaoAdzoneId);
    User findByTaobaoSpecialId(String taobaoSpecialId);
    User findByTaobaoRelationId(String taobaoRelationId);

    User findByWeixinOpenId(String weixinOpenId);
    User findByWeixinOpenIdAndDeleted(String weixinOpenId, boolean deleted);

    User findByWeixinUnionId(String weixinUnionId);
    User findByWeixinUnionIdAndDeleted(String weixinUnionId, boolean deleted);

    List<User> findByIdInOrderByIdAsc(List<Long> userIds);
    List<User> findByIdIn(List<Long> userIds);

    @Modifying
    @Query("update User u set u.taobaoSpecialId = null where u.taobaoSpecialId = ?1")
    int clearTaobaoSpecialId(String taobaoSpecialId);

    @Modifying
    @Query("update User u set u.taobaoRelationId = null where u.taobaoRelationId = ?1")
    int clearTaobaoRelationId(String taobaoRelationId);

    int countByRelationPathStartsWith(String start);

    int countBySuperUserId(long superUserId);
    int countBySuperUserIdAndLevel(long superUserId, UserLevel userLevel);
    int countBySuperUserIdAndLevelIn(long superUserId, UserLevel... userLevel);

//    @Query(nativeQuery = true, value = "select count(1) from `user` u where u.relation_path like CONCAT(?1,'%') and u.level=?2" +
//            " and u.relation_path<>?1 and relation_depth<=?3")
//    int countChildrenByRelationPathAndUserLevelAndMaxRelationDepth(String relationPath, UserLevel userLevel, int maxDepth);

    int countByRelationPathStartsWithAndRelationPathNotAndRelationDepthLessThanEqualAndLevel(String relationPathStart, String relationPathNot,
                                                                                             int relationDepth, UserLevel level);

    int countByRelationPathStartsWithAndRelationPathNotAndRelationDepthLessThanEqualAndLevelIn(String relationPathStart, String relationPathNot,
                                                                                             int relationDepth, UserLevel... level);

    List<User> findByRelationPathStartsWith(String relationPathStart);

    @Query(nativeQuery = true, value = "select count(1) as total, sum(case when su.relation_path=?1 then 1 else 0 end ) as direct," +
            " sum(case when su.relation_path!=?1 then 1 else 0 end ) as indirect from user u left join user su on u.super_user_id = su.id" +
            " where u.relation_path like CONCAT(?1,'%')")
    Object statMemberNumByRelationPathStartWith(String relationPath);

    class MemberNumStat {
        private long total;
        private long direct;
        private long indirect;

        public MemberNumStat(long total, long direct, long indirect) {
            this.total = total;
            this.direct = direct;
            this.indirect = indirect;
        }

        public long getTotal() {
            return total;
        }

        public void setTotal(long total) {
            this.total = total;
        }

        public long getDirect() {
            return direct;
        }

        public void setDirect(long direct) {
            this.direct = direct;
        }

        public long getIndirect() {
            return indirect;
        }

        public void setIndirect(long indirect) {
            this.indirect = indirect;
        }
    }

}
