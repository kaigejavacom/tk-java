package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.Service;
import info.batcloud.fanli.core.enums.UserServiceSubscriptionStatus;

import javax.persistence.*;
import java.util.Date;

@Entity
public class UserServiceSubscription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.REFRESH)
    private User user;

    @Enumerated(EnumType.STRING)
    private Service service;

    private Date expireTime; //过期时间

    private Date createTime;

    private Date updateTime;

    @Enumerated(EnumType.STRING)
    private UserServiceSubscriptionStatus status;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Date getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }

    public UserServiceSubscriptionStatus getStatus() {
        return status;
    }

    public void setStatus(UserServiceSubscriptionStatus status) {
        this.status = status;
    }
}
