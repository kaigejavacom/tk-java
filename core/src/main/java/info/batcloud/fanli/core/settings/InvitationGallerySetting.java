package info.batcloud.fanli.core.settings;

import info.batcloud.fanli.core.domain.Pic;

import java.io.Serializable;
import java.util.List;

public class InvitationGallerySetting implements Serializable {

    private List<Pic> picList;

    public List<Pic> getPicList() {
        return picList;
    }

    public void setPicList(List<Pic> picList) {
        this.picList = picList;
    }


}
