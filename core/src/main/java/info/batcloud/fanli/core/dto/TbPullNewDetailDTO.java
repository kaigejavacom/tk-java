package info.batcloud.fanli.core.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import info.batcloud.fanli.core.constants.MessageKeyConstants;
import info.batcloud.fanli.core.context.StaticContext;

import java.util.Date;

public class TbPullNewDetailDTO {

    private Long id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date registerTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date bindTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date buyTime;

    private String mobile;

    private int status;

    private int orderTkType;

    private Long tbTradeParentId;

    //广告位id
    private Long adzoneId;

    //媒体id
    private Long memberId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    private Long userId;

    //是否已经结算掉
    private boolean settled;

    public String getStatusTitle() {
        return StaticContext.messageSource.getMessage(MessageKeyConstants.TB_PULL_NEW_DETAIL + "." + getStatus(), null, null);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    public Date getBindTime() {
        return bindTime;
    }

    public void setBindTime(Date bindTime) {
        this.bindTime = bindTime;
    }

    public Date getBuyTime() {
        return buyTime;
    }

    public void setBuyTime(Date buyTime) {
        this.buyTime = buyTime;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getOrderTkType() {
        return orderTkType;
    }

    public void setOrderTkType(int orderTkType) {
        this.orderTkType = orderTkType;
    }

    public Long getTbTradeParentId() {
        return tbTradeParentId;
    }

    public void setTbTradeParentId(Long tbTradeParentId) {
        this.tbTradeParentId = tbTradeParentId;
    }

    public Long getAdzoneId() {
        return adzoneId;
    }

    public void setAdzoneId(Long adzoneId) {
        this.adzoneId = adzoneId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public boolean isSettled() {
        return settled;
    }

    public void setSettled(boolean settled) {
        this.settled = settled;
    }
}
