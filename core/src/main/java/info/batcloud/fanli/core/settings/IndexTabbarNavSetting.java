package info.batcloud.fanli.core.settings;

import info.batcloud.fanli.core.operation.center.domain.TabbarNav;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class IndexTabbarNavSetting implements Serializable {

    private List<TabbarNav> tabbarNavList = new ArrayList<>();

    public List<TabbarNav> getTabbarNavList() {
        return tabbarNavList;
    }

    public void setTabbarNavList(List<TabbarNav> tabbarNavList) {
        this.tabbarNavList = tabbarNavList;
    }
}
