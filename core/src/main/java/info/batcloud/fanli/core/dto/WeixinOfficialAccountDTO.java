package info.batcloud.fanli.core.dto;

import info.batcloud.fanli.core.enums.WeixinOfficialAccountType;

import java.io.Serializable;

public class WeixinOfficialAccountDTO implements Serializable {

    private Long id;

    private Long userId;

    private String token;

    private String name;

    private String encodingAESKey;

    private String serverUrl;

    private WeixinOfficialAccountType type;

    public String getTypeTitle() {
        return type == null ? null : type.getTitle();
    }

    public WeixinOfficialAccountType getType() {
        return type;
    }

    public void setType(WeixinOfficialAccountType type) {
        this.type = type;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEncodingAESKey() {
        return encodingAESKey;
    }

    public void setEncodingAESKey(String encodingAESKey) {
        this.encodingAESKey = encodingAESKey;
    }
}
