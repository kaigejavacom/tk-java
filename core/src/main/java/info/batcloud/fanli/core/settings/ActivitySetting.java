package info.batcloud.fanli.core.settings;

import java.io.Serializable;

public class ActivitySetting implements Serializable {

    //淘宝拉新活动每个奖励金额
    private float tbFullNewEachFee;


    public float getTbFullNewEachFee() {
        return tbFullNewEachFee;
    }

    public void setTbFullNewEachFee(float tbFullNewEachFee) {
        this.tbFullNewEachFee = tbFullNewEachFee;
    }
}
