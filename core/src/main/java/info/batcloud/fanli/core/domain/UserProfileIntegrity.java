package info.batcloud.fanli.core.domain;

public class UserProfileIntegrity {

    private boolean phone;
    private boolean region;

    public boolean isPhone() {
        return phone;
    }

    public void setPhone(boolean phone) {
        this.phone = phone;
    }

    public boolean isRegion() {
        return region;
    }

    public void setRegion(boolean region) {
        this.region = region;
    }
}
