package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.domain.TaobaoMaterial;
import info.batcloud.fanli.core.dto.TaobaoMaterialCatDTO;
import info.batcloud.fanli.core.enums.TaobaoMaterialCatStatus;

import java.util.List;

public interface TaobaoMaterialCatService {

    List<TaobaoMaterialCatDTO> findAll();

    TaobaoMaterialCatDTO findById(long id);

    TaobaoMaterialCatDTO addTaobaoMaterialCat(String name, TaobaoMaterialCatStatus status);

    void updateTaobaoMaterialCat(long id, String name);

    void deleteById(long id);

    void setMaterialList(long id, List<TaobaoMaterial> list);

    List<TaobaoMaterial> getMaterialList(long id);
}
