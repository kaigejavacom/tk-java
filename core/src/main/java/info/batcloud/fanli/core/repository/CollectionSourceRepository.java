package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.CollectionSource;
import info.batcloud.fanli.core.enums.CollectionSourceStatus;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CollectionSourceRepository extends PagingAndSortingRepository<CollectionSource, Long>{

    List<CollectionSource> findByStatusNot(CollectionSourceStatus status, Sort sort);

    List<CollectionSource> findByStatus(CollectionSourceStatus status, Sort sort);

}
