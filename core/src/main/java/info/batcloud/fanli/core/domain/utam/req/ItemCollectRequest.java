package info.batcloud.fanli.core.domain.utam.req;

import java.util.List;

public class ItemCollectRequest {

    private List<Long> itemSelectionIdList;

    public List<Long> getItemSelectionIdList() {
        return itemSelectionIdList;
    }

    public void setItemSelectionIdList(List<Long> itemSelectionIdList) {
        this.itemSelectionIdList = itemSelectionIdList;
    }
}
