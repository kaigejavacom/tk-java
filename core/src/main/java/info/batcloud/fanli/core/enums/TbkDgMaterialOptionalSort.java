package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum TbkDgMaterialOptionalSort implements EnumTitle{

    TK_RATE_DES, TK_RATE_ASC, TOTAL_SALES_DES, TOTAL_SALES_ASC, TK_TOTAL_SALES_DES,
    TK_TOTAL_SALES_ASC,TK_TOTAL_COMMI_DES, TK_TOTAL_COMMI_ASC, PRICE_DES, PRICE_ASC;

    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, "", null);
    }

}
