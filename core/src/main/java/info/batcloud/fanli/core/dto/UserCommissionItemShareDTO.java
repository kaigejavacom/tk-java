package info.batcloud.fanli.core.dto;

public class UserCommissionItemShareDTO {

    private Long id;
    private long userId;
    private long commissionItemId;
    private String url;
    private String shortUrl;
    private String kl;
    private String clickUrl;
    private String taobaoRelationId;

    public String getTaobaoRelationId() {
        return taobaoRelationId;
    }

    public void setTaobaoRelationId(String taobaoRelationId) {
        this.taobaoRelationId = taobaoRelationId;
    }

    public String getClickUrl() {
        return clickUrl;
    }

    public void setClickUrl(String clickUrl) {
        this.clickUrl = clickUrl;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public String getKl() {
        return kl;
    }

    public void setKl(String kl) {
        this.kl = kl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getCommissionItemId() {
        return commissionItemId;
    }

    public void setCommissionItemId(long commissionItemId) {
        this.commissionItemId = commissionItemId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
