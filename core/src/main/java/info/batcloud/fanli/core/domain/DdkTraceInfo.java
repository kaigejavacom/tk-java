package info.batcloud.fanli.core.domain;

import com.alibaba.fastjson.JSON;

public class DdkTraceInfo {

    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String toJson() {
        return JSON.toJSONString(this);
    }

    public static DdkTraceInfo fromJson(String json) {
        return JSON.parseObject(json, DdkTraceInfo.class);
    }
}
