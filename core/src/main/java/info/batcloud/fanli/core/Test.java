package info.batcloud.fanli.core;

public class Test {

    public static class Animal {
        public void say() {
            System.out.println("animal");
        }

        public void doSay() {
            say();
        }
    }

    public static class Dog extends Animal{
        @Override
        public void say() {
            System.out.println("wangwang");
        }
    }

    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.doSay();

        String id = "123";
        System.out.println(id.hashCode());
        String id1 = "123";
        System.out.println(id1.hashCode());
    }

}
