package info.batcloud.fanli.core.domain.stat;

public class UserCommissionEarning {

    private Long userId;

    private String userAvatar;

    private String userNickname;

    //时间
    private String date;

    //预估收益
    private float estimateCommissionFee;

    //预估奖励
    private float estimateRewardFee;

    private float selfBuyEstimateCommissionFee;

    private float directEstimateCommissionFee;

    private float indirectEstimateCommissionFee;

    private float carrierEstimateCommissionFee;

    private float chiefEstimateCommissionFee;

    private float cityAgentEstimateCommissionFee;

    private float districtAgentEstimateCommissionFee;

    private float relationAgentEstimateCommissionFee;

    private float selfBuyEstimateRewardFee;

    private float directEstimateRewardFee;

    private float indirectEstimateRewardFee;

    private float carrierEstimateRewardFee;
    private float chiefEstimateRewardFee;

    private float cityAgentEstimateRewardFee;

    private float districtAgentEstimateRewardFee;

    private float relationAgentEstimateRewardFee;

    //订单数
    private int orderNum;

    private int selfBuyOrderNum;

    private int directOrderNum;

    private int indirectOrderNum;

    private int carrierOrderNum;
    private int chiefOrderNum;

    private int cityAgentOrderNum;

    private int districtAgentOrderNum;

    private int relationAgentOrderNum;

    private int paidOrderNum;

    private int waitSettleOrderNum;

    private int settledOrderNum;


    private float settledFee;

    private float settledRewardFee;

    private float settledCommissionFee;

    private float selfBuySettledCommissionFee;

    private float directSettledCommissionFee;

    private float indirectSettledCommissionFee;

    private float carrierSettledCommissionFee;
    private float chiefSettledCommissionFee;

    private float cityAgentSettledCommissionFee;

    private float districtAgentSettledCommissionFee;

    private float relationAgentSettledCommissionFee;

    private float selfBuySettledRewardFee;

    private float directSettledRewardFee;

    private float indirectSettledRewardFee;

    private float carrierSettledRewardFee;
    private float chiefSettledRewardFee;

    private float cityAgentSettledRewardFee;

    private float districtAgentSettledRewardFee;

    private float relationAgentSettledRewardFee;

    public float getChiefEstimateRewardFee() {
        return chiefEstimateRewardFee;
    }

    public void setChiefEstimateRewardFee(float chiefEstimateRewardFee) {
        this.chiefEstimateRewardFee = chiefEstimateRewardFee;
    }

    public float getChiefEstimateCommissionFee() {
        return chiefEstimateCommissionFee;
    }

    public void setChiefEstimateCommissionFee(float chiefEstimateCommissionFee) {
        this.chiefEstimateCommissionFee = chiefEstimateCommissionFee;
    }

    public int getChiefOrderNum() {
        return chiefOrderNum;
    }

    public void setChiefOrderNum(int chiefOrderNum) {
        this.chiefOrderNum = chiefOrderNum;
    }

    public float getChiefSettledCommissionFee() {
        return chiefSettledCommissionFee;
    }

    public void setChiefSettledCommissionFee(float chiefSettledCommissionFee) {
        this.chiefSettledCommissionFee = chiefSettledCommissionFee;
    }

    public float getChiefSettledRewardFee() {
        return chiefSettledRewardFee;
    }

    public void setChiefSettledRewardFee(float chiefSettledRewardFee) {
        this.chiefSettledRewardFee = chiefSettledRewardFee;
    }

    public int getPaidOrderNum() {
        return paidOrderNum;
    }

    public void setPaidOrderNum(int paidOrderNum) {
        this.paidOrderNum = paidOrderNum;
    }

    public int getWaitSettleOrderNum() {
        return waitSettleOrderNum;
    }

    public void setWaitSettleOrderNum(int waitSettleOrderNum) {
        this.waitSettleOrderNum = waitSettleOrderNum;
    }

    public int getSettledOrderNum() {
        return settledOrderNum;
    }

    public void setSettledOrderNum(int settledOrderNum) {
        this.settledOrderNum = settledOrderNum;
    }

    public float getSettledFee() {
        return settledFee;
    }

    public void setSettledFee(float settledFee) {
        this.settledFee = settledFee;
    }

    public float getSettledRewardFee() {
        return settledRewardFee;
    }

    public void setSettledRewardFee(float settledRewardFee) {
        this.settledRewardFee = settledRewardFee;
    }

    public float getSettledCommissionFee() {
        return settledCommissionFee;
    }

    public void setSettledCommissionFee(float settledCommissionFee) {
        this.settledCommissionFee = settledCommissionFee;
    }

    public float getSelfBuySettledCommissionFee() {
        return selfBuySettledCommissionFee;
    }

    public void setSelfBuySettledCommissionFee(float selfBuySettledCommissionFee) {
        this.selfBuySettledCommissionFee = selfBuySettledCommissionFee;
    }

    public float getDirectSettledCommissionFee() {
        return directSettledCommissionFee;
    }

    public void setDirectSettledCommissionFee(float directSettledCommissionFee) {
        this.directSettledCommissionFee = directSettledCommissionFee;
    }

    public float getIndirectSettledCommissionFee() {
        return indirectSettledCommissionFee;
    }

    public void setIndirectSettledCommissionFee(float indirectSettledCommissionFee) {
        this.indirectSettledCommissionFee = indirectSettledCommissionFee;
    }

    public float getCarrierSettledCommissionFee() {
        return carrierSettledCommissionFee;
    }

    public void setCarrierSettledCommissionFee(float carrierSettledCommissionFee) {
        this.carrierSettledCommissionFee = carrierSettledCommissionFee;
    }

    public float getCityAgentSettledCommissionFee() {
        return cityAgentSettledCommissionFee;
    }

    public void setCityAgentSettledCommissionFee(float cityAgentSettledCommissionFee) {
        this.cityAgentSettledCommissionFee = cityAgentSettledCommissionFee;
    }

    public float getDistrictAgentSettledCommissionFee() {
        return districtAgentSettledCommissionFee;
    }

    public void setDistrictAgentSettledCommissionFee(float districtAgentSettledCommissionFee) {
        this.districtAgentSettledCommissionFee = districtAgentSettledCommissionFee;
    }

    public float getRelationAgentSettledCommissionFee() {
        return relationAgentSettledCommissionFee;
    }

    public void setRelationAgentSettledCommissionFee(float relationAgentSettledCommissionFee) {
        this.relationAgentSettledCommissionFee = relationAgentSettledCommissionFee;
    }

    public float getSelfBuySettledRewardFee() {
        return selfBuySettledRewardFee;
    }

    public void setSelfBuySettledRewardFee(float selfBuySettledRewardFee) {
        this.selfBuySettledRewardFee = selfBuySettledRewardFee;
    }

    public float getDirectSettledRewardFee() {
        return directSettledRewardFee;
    }

    public void setDirectSettledRewardFee(float directSettledRewardFee) {
        this.directSettledRewardFee = directSettledRewardFee;
    }

    public float getIndirectSettledRewardFee() {
        return indirectSettledRewardFee;
    }

    public void setIndirectSettledRewardFee(float indirectSettledRewardFee) {
        this.indirectSettledRewardFee = indirectSettledRewardFee;
    }

    public float getCarrierSettledRewardFee() {
        return carrierSettledRewardFee;
    }

    public void setCarrierSettledRewardFee(float carrierSettledRewardFee) {
        this.carrierSettledRewardFee = carrierSettledRewardFee;
    }

    public float getCityAgentSettledRewardFee() {
        return cityAgentSettledRewardFee;
    }

    public void setCityAgentSettledRewardFee(float cityAgentSettledRewardFee) {
        this.cityAgentSettledRewardFee = cityAgentSettledRewardFee;
    }

    public float getDistrictAgentSettledRewardFee() {
        return districtAgentSettledRewardFee;
    }

    public void setDistrictAgentSettledRewardFee(float districtAgentSettledRewardFee) {
        this.districtAgentSettledRewardFee = districtAgentSettledRewardFee;
    }

    public float getRelationAgentSettledRewardFee() {
        return relationAgentSettledRewardFee;
    }

    public void setRelationAgentSettledRewardFee(float relationAgentSettledRewardFee) {
        this.relationAgentSettledRewardFee = relationAgentSettledRewardFee;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getUserNickname() {
        return userNickname;
    }

    public void setUserNickname(String userNickname) {
        this.userNickname = userNickname;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public float getEstimateCommissionFee() {
        return estimateCommissionFee;
    }

    public void setEstimateCommissionFee(float estimateCommissionFee) {
        this.estimateCommissionFee = estimateCommissionFee;
    }

    public float getSelfBuyEstimateCommissionFee() {
        return selfBuyEstimateCommissionFee;
    }

    public void setSelfBuyEstimateCommissionFee(float selfBuyEstimateCommissionFee) {
        this.selfBuyEstimateCommissionFee = selfBuyEstimateCommissionFee;
    }

    public float getDirectEstimateCommissionFee() {
        return directEstimateCommissionFee;
    }

    public void setDirectEstimateCommissionFee(float directEstimateCommissionFee) {
        this.directEstimateCommissionFee = directEstimateCommissionFee;
    }

    public float getIndirectEstimateCommissionFee() {
        return indirectEstimateCommissionFee;
    }

    public void setIndirectEstimateCommissionFee(float indirectEstimateCommissionFee) {
        this.indirectEstimateCommissionFee = indirectEstimateCommissionFee;
    }

    public float getCarrierEstimateCommissionFee() {
        return carrierEstimateCommissionFee;
    }

    public void setCarrierEstimateCommissionFee(float carrierEstimateCommissionFee) {
        this.carrierEstimateCommissionFee = carrierEstimateCommissionFee;
    }

    public float getCityAgentEstimateCommissionFee() {
        return cityAgentEstimateCommissionFee;
    }

    public void setCityAgentEstimateCommissionFee(float cityAgentEstimateCommissionFee) {
        this.cityAgentEstimateCommissionFee = cityAgentEstimateCommissionFee;
    }

    public float getDistrictAgentEstimateCommissionFee() {
        return districtAgentEstimateCommissionFee;
    }

    public void setDistrictAgentEstimateCommissionFee(float districtAgentEstimateCommissionFee) {
        this.districtAgentEstimateCommissionFee = districtAgentEstimateCommissionFee;
    }

    public float getRelationAgentEstimateCommissionFee() {
        return relationAgentEstimateCommissionFee;
    }

    public void setRelationAgentEstimateCommissionFee(float relationAgentEstimateCommissionFee) {
        this.relationAgentEstimateCommissionFee = relationAgentEstimateCommissionFee;
    }

    public float getSelfBuyEstimateRewardFee() {
        return selfBuyEstimateRewardFee;
    }

    public void setSelfBuyEstimateRewardFee(float selfBuyEstimateRewardFee) {
        this.selfBuyEstimateRewardFee = selfBuyEstimateRewardFee;
    }

    public float getDirectEstimateRewardFee() {
        return directEstimateRewardFee;
    }

    public void setDirectEstimateRewardFee(float directEstimateRewardFee) {
        this.directEstimateRewardFee = directEstimateRewardFee;
    }

    public float getIndirectEstimateRewardFee() {
        return indirectEstimateRewardFee;
    }

    public void setIndirectEstimateRewardFee(float indirectEstimateRewardFee) {
        this.indirectEstimateRewardFee = indirectEstimateRewardFee;
    }

    public float getCarrierEstimateRewardFee() {
        return carrierEstimateRewardFee;
    }

    public void setCarrierEstimateRewardFee(float carrierEstimateRewardFee) {
        this.carrierEstimateRewardFee = carrierEstimateRewardFee;
    }

    public float getCityAgentEstimateRewardFee() {
        return cityAgentEstimateRewardFee;
    }

    public void setCityAgentEstimateRewardFee(float cityAgentEstimateRewardFee) {
        this.cityAgentEstimateRewardFee = cityAgentEstimateRewardFee;
    }

    public float getDistrictAgentEstimateRewardFee() {
        return districtAgentEstimateRewardFee;
    }

    public void setDistrictAgentEstimateRewardFee(float districtAgentEstimateRewardFee) {
        this.districtAgentEstimateRewardFee = districtAgentEstimateRewardFee;
    }

    public float getRelationAgentEstimateRewardFee() {
        return relationAgentEstimateRewardFee;
    }

    public void setRelationAgentEstimateRewardFee(float relationAgentEstimateRewardFee) {
        this.relationAgentEstimateRewardFee = relationAgentEstimateRewardFee;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public int getSelfBuyOrderNum() {
        return selfBuyOrderNum;
    }

    public void setSelfBuyOrderNum(int selfBuyOrderNum) {
        this.selfBuyOrderNum = selfBuyOrderNum;
    }

    public int getDirectOrderNum() {
        return directOrderNum;
    }

    public void setDirectOrderNum(int directOrderNum) {
        this.directOrderNum = directOrderNum;
    }

    public int getIndirectOrderNum() {
        return indirectOrderNum;
    }

    public void setIndirectOrderNum(int indirectOrderNum) {
        this.indirectOrderNum = indirectOrderNum;
    }

    public int getCarrierOrderNum() {
        return carrierOrderNum;
    }

    public void setCarrierOrderNum(int carrierOrderNum) {
        this.carrierOrderNum = carrierOrderNum;
    }

    public int getCityAgentOrderNum() {
        return cityAgentOrderNum;
    }

    public void setCityAgentOrderNum(int cityAgentOrderNum) {
        this.cityAgentOrderNum = cityAgentOrderNum;
    }

    public int getDistrictAgentOrderNum() {
        return districtAgentOrderNum;
    }

    public void setDistrictAgentOrderNum(int districtAgentOrderNum) {
        this.districtAgentOrderNum = districtAgentOrderNum;
    }

    public int getRelationAgentOrderNum() {
        return relationAgentOrderNum;
    }

    public void setRelationAgentOrderNum(int relationAgentOrderNum) {
        this.relationAgentOrderNum = relationAgentOrderNum;
    }

    public float getEstimateRewardFee() {
        return estimateRewardFee;
    }

    public void setEstimateRewardFee(float estimateRewardFee) {
        this.estimateRewardFee = estimateRewardFee;
    }
}
