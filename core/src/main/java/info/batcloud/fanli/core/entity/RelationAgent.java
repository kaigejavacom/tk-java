package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.AgentType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("RelationAgent")
public class RelationAgent extends Agent {

    @Override
    public AgentType getAgentType() {
        return AgentType.RELATION;
    }

}
