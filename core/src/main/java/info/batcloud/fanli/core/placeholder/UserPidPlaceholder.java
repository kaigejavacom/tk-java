package info.batcloud.fanli.core.placeholder;

import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.service.UserService;
import info.batcloud.fanli.core.settings.TaobaoUnionSetting;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class UserPidPlaceholder extends AbstractPlaceholder {

    @Inject
    private UserService userService;

    @Inject
    private SystemSettingService systemSettingService;

    @Override
    protected String replacePlaceholder(ReplaceContext context) {
        if(context.getUserId() == null) {
            TaobaoUnionSetting taobaoUnionSetting = systemSettingService.findActiveSetting(TaobaoUnionSetting.class);
            return taobaoUnionSetting.getRebatePid();
        }
        return userService.getTaobaoPidByUserId(context.getUserId());
    }

    @Override
    protected String getKey() {
        return "USER_PID";
    }

}
