package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.CommissionAllotType;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Entity
public class AllotTypeCommissionFee {

    @Id
    private String id;

    @Enumerated(EnumType.STRING)
    private CommissionAllotType allotType;

    private float commissionFee;

    private int orderNum;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public CommissionAllotType getAllotType() {
        return allotType;
    }

    public void setAllotType(CommissionAllotType allotType) {
        this.allotType = allotType;
    }

    public float getCommissionFee() {
        return commissionFee;
    }

    public void setCommissionFee(float commissionFee) {
        this.commissionFee = commissionFee;
    }
}
