package info.batcloud.fanli.core.settings;

import info.batcloud.fanli.core.enums.UrlMappingCodes;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UrlMappingSetting implements Serializable {

    private List<UrlMapping> urlMappingList;

    private Map<String, String> urlMappedByCode = new HashMap<>();

    public List<UrlMapping> getUrlMappingList() {
        return urlMappingList;
    }

    public void setUrlMappingList(List<UrlMapping> urlMappingList) {
        this.urlMappingList = urlMappingList;
    }

    public UrlMapping getUrlByCode(String code) {
        if (urlMappingList == null) {
            return null;
        }
        for (UrlMapping urlMapping : urlMappingList) {
            if (code.equals(urlMapping.getCode())) {
                return urlMapping;
            }
        }
        return null;
    }

    public static class UrlMapping implements Serializable {
        private String title;
        private String code;
        private String url;
        private String help;

        public String getHelp() {
            if (StringUtils.isNotBlank(help)) {
                return help;
            }
            try {
                UrlMappingCodes codes = UrlMappingCodes.valueOf(this.getCode());
                return codes == null ? null : codes.getHelp();
            } catch (Exception e) {
                return null;
            }
        }

        public void setHelp(String help) {
            this.help = help;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}
