package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.UserUpgradeOrder;
import info.batcloud.fanli.core.enums.UserLevelUpgradeOrderStatus;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface UserUpgradeOrderRepository extends PagingAndSortingRepository<UserUpgradeOrder, Long> {

    List<UserUpgradeOrder> findByUserIdAndStatus(long userId, UserLevelUpgradeOrderStatus status);

}
