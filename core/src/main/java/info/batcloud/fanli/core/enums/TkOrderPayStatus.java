package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum TkOrderPayStatus implements EnumTitle {

    SETTLED("3"), PAID("12"), INVALID("13"), FINISH("14");
    public String value;

    TkOrderPayStatus(String value) {
        this.value = value;
    }


    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, "", null);
    }

}
