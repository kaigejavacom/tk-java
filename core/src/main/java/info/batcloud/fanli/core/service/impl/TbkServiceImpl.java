package info.batcloud.fanli.core.service.impl;

import com.taobao.api.ApiException;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.TbkTpwdCreateRequest;
import com.taobao.api.response.TbkTpwdCreateResponse;
import info.batcloud.fanli.core.constants.TaobaoClientConstants;
import info.batcloud.fanli.core.service.TbkService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class TbkServiceImpl implements TbkService {

    @Inject
    @Qualifier(TaobaoClientConstants.TAOBAO_CLIENT_ITEM_TBK)
    private TaobaoClient taobaoClient;

    @Override
    public String transformToToken(String text, String url) throws ApiException {
        TbkTpwdCreateRequest req = new TbkTpwdCreateRequest();
        req.setUrl(url);
        req.setText(text);
        TbkTpwdCreateResponse rsp = taobaoClient.execute(req);
        if(rsp.getData() == null) {
            return null;
        }
        return rsp.getData().getModel();
    }
}
