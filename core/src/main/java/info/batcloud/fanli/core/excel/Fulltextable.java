package info.batcloud.fanli.core.excel;

public interface Fulltextable {

    String setFulltextWords(String words);

    String getFulltextSource();
}
