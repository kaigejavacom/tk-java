package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.UserCommissionOrderDTO;
import info.batcloud.fanli.core.entity.AllotTypeCommissionFee;
import info.batcloud.fanli.core.enums.CommissionOrderStatus;
import info.batcloud.fanli.core.enums.EcomPlat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public interface UserCommissionOrderService {

    long findUserCommissionOrderNumByDay(long userId, Date day);

    float findUserEstimateCommissionFeeByDay(long userId, Date day);
    float findUserEstimateCommissionFeeByStartTime(long userId, Date startTime);
    float findUserEstimateCommissionFeeBetweenTime(long userId, Date startTime, Date endTime);

    float findUserSettledCommissionFeeBetweenTime(long userId, Date startTime, Date endTime);

    float findUserPaidEstimateCommissionFee(long userId);

    float findUserWaitSettleEstimateCommissionFee(long userId);

    float findUserUnSettleEstimateCommissionFee(long userId);

    List<AllotTypeCommissionFee> statUserEstimateAllotTypeCommissionFeeBetweenTime(long userId, Date startTime, Date endTime);

    Paging<UserCommissionOrderDTO> search(SearchParam param);

    File exportXls(ExportParam param) throws IOException;

    class ExportParam extends SearchParam {
        private int maxCount;

        public int getMaxCount() {
            return maxCount;
        }

        public void setMaxCount(int maxCount) {
            this.maxCount = maxCount;
        }
    }

    class SearchParam extends PagingParam {
        private Long userId;
        private Long commissionOrderId;
        private Long commissionItemId;
        private CommissionOrderStatus status;
        private String itemTitle;

        private Long itemId;
        private EcomPlat ecomPlat;
        private String orderNo;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date minCreateTime;
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date maxCreateTime;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date minSettledTime;
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date maxSettledTime;

        public Long getCommissionItemId() {
            return commissionItemId;
        }

        public void setCommissionItemId(Long commissionItemId) {
            this.commissionItemId = commissionItemId;
        }

        public String getItemTitle() {
            return itemTitle;
        }

        public void setItemTitle(String itemTitle) {
            this.itemTitle = itemTitle;
        }

        public Long getItemId() {
            return itemId;
        }

        public void setItemId(Long itemId) {
            this.itemId = itemId;
        }

        public EcomPlat getEcomPlat() {
            return ecomPlat;
        }

        public void setEcomPlat(EcomPlat ecomPlat) {
            this.ecomPlat = ecomPlat;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public Date getMinCreateTime() {
            return minCreateTime;
        }

        public void setMinCreateTime(Date minCreateTime) {
            this.minCreateTime = minCreateTime;
        }

        public Date getMaxCreateTime() {
            return maxCreateTime;
        }

        public void setMaxCreateTime(Date maxCreateTime) {
            this.maxCreateTime = maxCreateTime;
        }

        public Date getMinSettledTime() {
            return minSettledTime;
        }

        public void setMinSettledTime(Date minSettledTime) {
            this.minSettledTime = minSettledTime;
        }

        public Date getMaxSettledTime() {
            return maxSettledTime;
        }

        public void setMaxSettledTime(Date maxSettledTime) {
            this.maxSettledTime = maxSettledTime;
        }

        public Long getCommissionOrderId() {
            return commissionOrderId;
        }

        public void setCommissionOrderId(Long commissionOrderId) {
            this.commissionOrderId = commissionOrderId;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public CommissionOrderStatus getStatus() {
            return status;
        }

        public void setStatus(CommissionOrderStatus status) {
            this.status = status;
        }
    }
}
