package info.batcloud.fanli.core.item.collection;

import com.ctospace.archit.common.pagination.Paging;
import info.batcloud.fanli.core.dto.CommissionItemFetchDTO;

import java.util.function.Consumer;

public interface CommissionItemCollector<T extends Context> {

    CollectResult collect(T context, Consumer<Paging<CommissionItemFetchDTO>> pagingConsumer);

    Class<T> getContextType();
}
