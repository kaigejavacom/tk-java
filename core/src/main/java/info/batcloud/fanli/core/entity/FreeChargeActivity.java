package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.ActivityFreq;
import info.batcloud.fanli.core.enums.FreeChargeActivityStatus;

import javax.persistence.*;
import java.util.Date;

@Entity
@Cacheable
public class FreeChargeActivity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String btnTitle;

    private String description;

    private String confirmDescription;

    private Date createTime;

    private Date updateTime;

    @Enumerated(EnumType.STRING)
    private ActivityFreq freq;

    private int maxUserDrawNum;

    private Date lastDrawTime;

    private Date startTime;

    private Date endTime;

    private int drawNum;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    private Crowd crowd;

    @Enumerated(EnumType.STRING)
    private FreeChargeActivityStatus status;

    private String helpUrl;

    private String markIcon;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    private Material material;

    public String getConfirmDescription() {
        return confirmDescription;
    }

    public void setConfirmDescription(String confirmDescription) {
        this.confirmDescription = confirmDescription;
    }

    public String getBtnTitle() {
        return btnTitle;
    }

    public void setBtnTitle(String btnTitle) {
        this.btnTitle = btnTitle;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public String getMarkIcon() {
        return markIcon;
    }

    public void setMarkIcon(String markIcon) {
        this.markIcon = markIcon;
    }

    public String getHelpUrl() {
        return helpUrl;
    }

    public void setHelpUrl(String helpUrl) {
        this.helpUrl = helpUrl;
    }

    public int getDrawNum() {
        return drawNum;
    }

    public void setDrawNum(int drawNum) {
        this.drawNum = drawNum;
    }

    public int getMaxUserDrawNum() {
        return maxUserDrawNum;
    }

    public void setMaxUserDrawNum(int maxUserDrawNum) {
        this.maxUserDrawNum = maxUserDrawNum;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public ActivityFreq getFreq() {
        return freq;
    }

    public void setFreq(ActivityFreq freq) {
        this.freq = freq;
    }

    public Date getLastDrawTime() {
        return lastDrawTime;
    }

    public void setLastDrawTime(Date lastDrawTime) {
        this.lastDrawTime = lastDrawTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Crowd getCrowd() {
        return crowd;
    }

    public void setCrowd(Crowd crowd) {
        this.crowd = crowd;
    }

    public FreeChargeActivityStatus getStatus() {
        return status;
    }

    public void setStatus(FreeChargeActivityStatus status) {
        this.status = status;
    }
}
