package info.batcloud.fanli.core.operation.center.domain;

import info.batcloud.fanli.core.helper.OSSImageHelper;

import java.io.Serializable;

public class TabbarNav implements Serializable {
    private String title;
    private String page;
    private boolean pushOpen;
    private String icon;
    private String selectedIcon;
    private String style;

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getIconUrl() {
        return OSSImageHelper.toUrl(icon);
    }

    public String getSelectedIconUrl() {
        return OSSImageHelper.toUrl(selectedIcon);
    }

    public boolean isPushOpen() {
        return pushOpen;
    }

    public void setPushOpen(boolean pushOpen) {
        this.pushOpen = pushOpen;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getSelectedIcon() {
        return selectedIcon;
    }

    public void setSelectedIcon(String selectedIcon) {
        this.selectedIcon = selectedIcon;
    }
}
