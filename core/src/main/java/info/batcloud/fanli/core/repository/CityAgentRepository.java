package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.CityAgent;
import info.batcloud.fanli.core.enums.AgentStatus;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CityAgentRepository extends PagingAndSortingRepository<CityAgent, Long>, JpaSpecificationExecutor<CityAgent> {

    CityAgent findByCityIdAndStatusIsNot(long cityId, AgentStatus status);

    CityAgent findByUserIdAndStatusIsNot(long userId, AgentStatus status);
}
