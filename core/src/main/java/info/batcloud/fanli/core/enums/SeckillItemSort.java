package info.batcloud.fanli.core.enums;

public enum SeckillItemSort {

    NONE, PRICE_DESC, PRICE_ASC, END_TIME_ASC, ID_DESC

}
