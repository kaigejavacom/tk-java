package info.batcloud.fanli.core.service.impl;

import com.tencent.xinge.Message;
import com.tencent.xinge.MessageIOS;
import com.tencent.xinge.XingeApp;
import info.batcloud.fanli.core.operation.center.domain.Page;
import info.batcloud.fanli.core.service.MobilePushService;
import info.batcloud.fanli.core.tencent.TencentConfig;
import info.batcloud.fanli.core.tencent.XgConfig;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@Service
public class MobilePushServiceImpl implements MobilePushService {

    @Inject
    private TencentConfig tencentConfig;

    @Override
    public void pushPage(Platform platform, String title, String content, Page page, String pageQueryString) {
        Map<String, Object> map = new HashMap<>(10);
        map.put("page", page.name());
        map.put("params", pageQueryString);
        this.push(platform, title, content, map);
    }

    @Override
    public void push(Platform platform, String title, String content, Map<String, Object> data) {
        Map<String, Object> map = new HashMap<>(10);
        map.put("data", data);
        if(platform == Platform.ANDROID) {
            Message message = new Message();
            message.setType(1);
            message.setTitle(title);
            message.setContent(content);
            message.setCustom(map);
            this.pushAndroid(message);
        }
        if(platform == Platform.IOS) {
            MessageIOS iosMsg = new MessageIOS();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("title", title);
            jsonObject.put("body", content);
            iosMsg.setAlert(jsonObject);
            iosMsg.setBadge(1);
            iosMsg.setSound("beep.wav");
            iosMsg.setCustom(map);
            this.pushIos(iosMsg);
        }
    }

    @Override
    public void pushToAccount(String account, Platform platform, String title, String content, Map<String, Object> data) {
        Map<String, Object> map = new HashMap<>(10);
        map.put("data", data);
        if(platform == Platform.ANDROID) {
            Message message = new Message();
            message.setType(1);
            message.setTitle(title);
            message.setContent(content);
            message.setCustom(map);
            this.pushAndroidAccount(account, message);
        }
        if(platform == Platform.IOS) {
            MessageIOS iosMsg = new MessageIOS();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("title", title);
            jsonObject.put("body", content);
            iosMsg.setAlert(jsonObject);
            iosMsg.setBadge(1);
            iosMsg.setSound("beep.wav");
            iosMsg.setCustom(map);
            this.pushIosAccount(account, iosMsg);
        }
    }

    @Override
    public void pushToAccount(String account, Platform platform, String title, String content, Page page, String pageQueryString) {
        Map<String, Object> map = new HashMap<>(10);
        map.put("page", page.name());
        map.put("params", pageQueryString);
        this.pushToAccount(account, platform, title, content, map);
    }

    @Override
    public void pushToAccount(String account, String title, String content, Page page, String pageQueryString) {
        this.pushToAccount(account, Platform.IOS, title, content , page, pageQueryString);
        this.pushToAccount(account, Platform.ANDROID, title, content , page, pageQueryString);
    }

    private JSONObject pushIos(MessageIOS message) {
        XgConfig xgConfig = tencentConfig.iosXgConfig();
        XingeApp xinge = new XingeApp(Long.valueOf(xgConfig.getAccessId()), xgConfig.getSecretKey());
        JSONObject ret = xinge.pushAllDevice(0, message, xgConfig.getIosEnv() == XgConfig.IosEnv.DEV ? XingeApp.IOSENV_DEV : XingeApp.IOSENV_PROD);
        return ret;
    }

    private JSONObject pushAndroid(Message message) {
        XgConfig xgConfig = tencentConfig.androidXgConfig();
        XingeApp xinge = new XingeApp(Long.valueOf(xgConfig.getAccessId()), xgConfig.getSecretKey());
        JSONObject ret = xinge.pushAllDevice(0, message);
        return ret;
    }

    private JSONObject pushIosAccount(String account, MessageIOS message) {
        XgConfig xgConfig = tencentConfig.iosXgConfig();
        XingeApp xinge = new XingeApp(Long.valueOf(xgConfig.getAccessId()), xgConfig.getSecretKey());
        JSONObject ret = xinge.pushSingleAccount(0, account, message, xgConfig.getIosEnv() == XgConfig.IosEnv.DEV ? XingeApp.IOSENV_DEV : XingeApp.IOSENV_PROD);
        return ret;
    }

    private JSONObject pushAndroidAccount(String account, Message message) {
        XgConfig xgConfig = tencentConfig.androidXgConfig();
        XingeApp xinge = new XingeApp(Long.valueOf(xgConfig.getAccessId()), xgConfig.getSecretKey());
        JSONObject ret = xinge.pushSingleAccount(0, account, message);
        return ret;
    }
}
