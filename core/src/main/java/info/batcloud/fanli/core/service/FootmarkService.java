package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.FootmarkDTO;
import info.batcloud.fanli.core.enums.FootmarkType;

public interface FootmarkService {

    void addFootmark(long userId, long itemId, FootmarkType type);

    void deleteByUserIdAndItemId(long userId, long itemId);

    void clearByUserId(long userId);

    boolean toggleFavor(long userId, long itemId);

    boolean isFavor(long userId, long itemId);

    Paging<FootmarkDTO> search(SearchParam param);

    class SearchParam extends PagingParam {
        private String keywords;
        private Long userId;
        private FootmarkType type;

        public FootmarkType getType() {
            return type;
        }

        public void setType(FootmarkType type) {
            this.type = type;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public String getKeywords() {
            return keywords;
        }

        public void setKeywords(String keywords) {
            this.keywords = keywords;
        }
    }
}
