package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.Channel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ChannelRepository extends CrudRepository<Channel, Long>{

    List<Channel> findAllByDeleted(boolean deleted);
}
