package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.FlashSaleOrderStatus;

import javax.persistence.*;
import java.util.Date;

@Entity
public class FlashSaleOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    private User user;

    private Date createTime;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    private CommissionItem item;

    private Date updateTime;

    @OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    private CommissionOrder commissionOrder;

    @Enumerated(EnumType.STRING)
    private FlashSaleOrderStatus status;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    private FlashSaleItem flashSaleItem;

    @Version
    private int version;

    //免单金额
    private Float freeFee;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public CommissionItem getItem() {
        return item;
    }

    public void setItem(CommissionItem item) {
        this.item = item;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public CommissionOrder getCommissionOrder() {
        return commissionOrder;
    }

    public void setCommissionOrder(CommissionOrder commissionOrder) {
        this.commissionOrder = commissionOrder;
    }

    public FlashSaleOrderStatus getStatus() {
        return status;
    }

    public void setStatus(FlashSaleOrderStatus status) {
        this.status = status;
    }

    public FlashSaleItem getFlashSaleItem() {
        return flashSaleItem;
    }

    public void setFlashSaleItem(FlashSaleItem flashSaleItem) {
        this.flashSaleItem = flashSaleItem;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Float getFreeFee() {
        return freeFee;
    }

    public void setFreeFee(Float freeFee) {
        this.freeFee = freeFee;
    }
}
