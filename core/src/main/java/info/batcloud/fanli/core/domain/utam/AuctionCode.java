package info.batcloud.fanli.core.domain.utam;

public class AuctionCode {

    private String clickUrl;
    private String couponLink;
    private String couponLinkTaoToken;
    private String couponShortLinkUrl;
    private String qrCodeUrl;
    private String shortLinkUrl;
    private String tkCommonRate;
    private String type;
    private String taoToken;

    public String getTaoToken() {
        return taoToken;
    }

    public void setTaoToken(String taoToken) {
        this.taoToken = taoToken;
    }

    public String getClickUrl() {
        return clickUrl;
    }

    public void setClickUrl(String clickUrl) {
        this.clickUrl = clickUrl;
    }

    public String getCouponLink() {
        return couponLink;
    }

    public void setCouponLink(String couponLink) {
        this.couponLink = couponLink;
    }

    public String getCouponLinkTaoToken() {
        return couponLinkTaoToken;
    }

    public void setCouponLinkTaoToken(String couponLinkTaoToken) {
        this.couponLinkTaoToken = couponLinkTaoToken;
    }

    public String getCouponShortLinkUrl() {
        return couponShortLinkUrl;
    }

    public void setCouponShortLinkUrl(String couponShortLinkUrl) {
        this.couponShortLinkUrl = couponShortLinkUrl;
    }

    public String getQrCodeUrl() {
        return qrCodeUrl;
    }

    public void setQrCodeUrl(String qrCodeUrl) {
        this.qrCodeUrl = qrCodeUrl;
    }

    public String getShortLinkUrl() {
        return shortLinkUrl;
    }

    public void setShortLinkUrl(String shortLinkUrl) {
        this.shortLinkUrl = shortLinkUrl;
    }

    public String getTkCommonRate() {
        return tkCommonRate;
    }

    public void setTkCommonRate(String tkCommonRate) {
        this.tkCommonRate = tkCommonRate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

