package info.batcloud.fanli.core.settings;

import info.batcloud.fanli.core.enums.JdUnionApiPermissionType;
import info.batcloud.fanli.core.settings.annotation.Single;

import java.io.Serializable;

@Single
public class LaxiaokeOpenSetting implements Serializable {

    private String openId;

    private String apiUrl;

    private JdUnionApiPermissionType jdUnionApiPermissionType;

    public JdUnionApiPermissionType getJdUnionApiPermissionType() {
        return jdUnionApiPermissionType;
    }

    public void setJdUnionApiPermissionType(JdUnionApiPermissionType jdUnionApiPermissionType) {
        this.jdUnionApiPermissionType = jdUnionApiPermissionType;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }
}
