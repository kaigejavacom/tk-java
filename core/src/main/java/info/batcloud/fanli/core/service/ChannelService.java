package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.dto.ChannelDTO;

import java.util.List;

public interface ChannelService {

    List<ChannelDTO> findAll();

    ChannelDTO findById(long id);

    ChannelDTO addChannel(String name);

    void updateChannel(long id, String name);

    void deleteById(long id);
}
