package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.entity.OutCatToShopcat;
import info.batcloud.fanli.core.enums.ItemSelectionPlat;

import java.util.List;


public interface OutCatToShopcatService {

    void addOutCatToShopcat(AddParam param);

    void deleteById(long id);

    List<OutCatToShopcat> listByShopcatId(long shopcatId);

    class AddParam {
        private String catName;

        private Long catId;

        private Long shopcatId;

        private ItemSelectionPlat plat;

        public String getCatName() {
            return catName;
        }

        public void setCatName(String catName) {
            this.catName = catName;
        }

        public Long getCatId() {
            return catId;
        }

        public void setCatId(Long catId) {
            this.catId = catId;
        }

        public Long getShopcatId() {
            return shopcatId;
        }

        public void setShopcatId(Long shopcatId) {
            this.shopcatId = shopcatId;
        }

        public ItemSelectionPlat getPlat() {
            return plat;
        }

        public void setPlat(ItemSelectionPlat plat) {
            this.plat = plat;
        }
    }

}
