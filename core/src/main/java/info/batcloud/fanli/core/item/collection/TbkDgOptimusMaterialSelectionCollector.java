package info.batcloud.fanli.core.item.collection;

import com.ctospace.archit.common.pagination.Paging;
import com.taobao.api.ApiException;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.TbkDgOptimusMaterialRequest;
import com.taobao.api.response.TbkDgOptimusMaterialResponse;
import info.batcloud.fanli.core.constants.TaobaoClientConstants;
import info.batcloud.fanli.core.dto.CommissionItemFetchDTO;
import info.batcloud.fanli.core.enums.CommissionItemStatus;
import info.batcloud.fanli.core.enums.EcomPlat;
import info.batcloud.fanli.core.enums.ItemSelectionType;
import info.batcloud.fanli.core.exception.BizException;
import info.batcloud.fanli.core.service.ShopcatService;
import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.settings.TaobaoUnionSetting;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.text.ParseException;
import java.util.*;
import java.util.function.Consumer;

@Service
public class TbkDgOptimusMaterialSelectionCollector extends AbstractItemSelectionCollector<TbkDgOptimusMaterialContext> {

    private static final Logger logger = LoggerFactory.getLogger(TbkDgOptimusMaterialSelectionCollector.class);

    @Inject
    @Qualifier(TaobaoClientConstants.TAOBAO_CLIENT_ITEM_TBK)
    private TaobaoClient taobaoClient;

    @Inject
    private SystemSettingService systemSettingService;

    @Inject
    private ShopcatService shopcatService;

    @Override
    public CollectResult collect(TbkDgOptimusMaterialContext context, Consumer<Paging<CommissionItemFetchDTO>> pagingConsumer) {

        TbkDgOptimusMaterialRequest req = new TbkDgOptimusMaterialRequest();
        BeanUtils.copyProperties(context, req);
        req.setPageSize(context.getPageSize() != null ? context.getPageSize() : 20l);
        CollectResult result = new CollectResult();
        TaobaoUnionSetting taobaoUnionSetting = systemSettingService.findActiveSetting(TaobaoUnionSetting.class);
        int page = context.getStartPage();
        req.setMaterialId(Long.valueOf(context.getMaterialId()));
        req.setAdzoneId(Long.valueOf(taobaoUnionSetting.getRebateAdzoneId()));
        if (StringUtils.isNotEmpty(context.getFavoritesId())) {
            req.setFavoritesId(context.getFavoritesId());
        }
        try {
            int num = 0;
            while (true) {
                logger.info("搜索" + page + "页");
                req.setPageNo(Long.valueOf(page));
                TbkDgOptimusMaterialResponse rsp = taobaoClient.execute(req);
                if (rsp.getResultList() == null) {
                    break;
                }
                List<CommissionItemFetchDTO> commissionItemFetchDTOList = this.toCommissionItemFetchBO(context, rsp.getResultList());
                Paging<CommissionItemFetchDTO> paging = new Paging<>();
                if (rsp.getResultList().size() >= context.getPageSize() - 1) {
                    paging.setTotal(paging.getPageSize() * page + 1);
                } else {
                    paging.setTotal(paging.getPageSize() * (page - 1) + rsp.getResultList().size());
                }
//                paging.setTotal(rsp.getResultList() == null
//                        ? rsp.getResultList().size() : rsp.getResultList().size());
                paging.setPage(page);
                paging.setPageSize(context.getPageSize());
                paging.setResults(commissionItemFetchDTOList);
                pagingConsumer.accept(paging);
                num += rsp.getResultList().size();
                if (rsp.getResultList().size() == 0) {
                    break;
                }
                if (page >= context.getMaxPage()) {
                    break;
                }
                page++;
            }
            logger.info("搜索完成");
            result.setSuccess(true);
            result.setTotalNum(num);
        } catch (ApiException e) {
            logger.error("搜索失败", e);
            result.setSuccess(false);
            result.setErrMsg(e.getLocalizedMessage());
        }
        return result;
    }

    private List<CommissionItemFetchDTO> toCommissionItemFetchBO(TbkDgOptimusMaterialContext context, List<TbkDgOptimusMaterialResponse.MapData> mapDataList) {
        List<CommissionItemFetchDTO> dtoList = new ArrayList<>();
        for (TbkDgOptimusMaterialResponse.MapData mapData : mapDataList) {
            if (mapData.getZkFinalPrice() == null) {
                throw new BizException(mapData.getTitle() + "已经下架，请删除后再进行同步");
            }
            CommissionItemFetchDTO dto = new CommissionItemFetchDTO();
            Date now = new Date();
            dto.setListTime(now);
            /**
             * 商品的下架时间为当前的日期+7天，优惠券的下架时间，活动的下架时间取最大者
             * */
            Date delistTime = DateUtils.addDays(now, 7);
            Date couponEndTime;
            if (StringUtils.isNotEmpty(mapData.getCouponEndTime())) {
                try {
                    couponEndTime = DateUtils.parseDate(mapData.getCouponEndTime(), "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss");
                    dto.setCouponEndTime(DateUtils.parseDate(mapData.getCouponEndTime(), "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss"));
                    dto.setCouponStartTime(DateUtils.parseDate(mapData.getCouponStartTime(), "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss"));
                } catch (ParseException e) {
                    try {
                        couponEndTime = new Date(Long.valueOf(mapData.getCouponEndTime()));
                    } catch (Exception e1) {
                        couponEndTime = delistTime;
                    }
                }
            } else {
                couponEndTime = delistTime;
            }
            dto.setDelistTime(Collections.max(Arrays.asList(delistTime, couponEndTime)));
            dto.setShopTitle(StringUtils.defaultString(mapData.getShopTitle(), ""));
            dto.setCouponRemainCount(mapData.getCouponRemainCount() == null ? null : mapData.getCouponRemainCount().intValue());
            dto.setCouponTotalCount(mapData.getCouponTotalCount() == null ? null : mapData.getCouponTotalCount().intValue());
            dto.setCoupon(StringUtils.isNotEmpty(mapData.getCouponClickUrl()));
            dto.setSummary(mapData.getItemDescription());
            dto.setCouponValue(Float.valueOf(mapData.getCouponAmount()));
            dto.setSourceItemUrl(mapData.getClickUrl());
            dto.setSourceItemId(mapData.getItemId());
            dto.setTitle(mapData.getTitle());
            dto.setWhitePicUrl(mapData.getWhiteImage());
            dto.setSourceItemClickUrl(mapData.getClickUrl());
            dto.setCouponClickUrl(mapData.getCouponClickUrl());
            dto.setOriginPrice(Float.valueOf(mapData.getZkFinalPrice()));
            dto.setCouponThresholdAmount(Float.valueOf(mapData.getCouponStartFee()));
            float commissionRate = Float.valueOf(mapData.getCommissionRate());
            dto.setCommissionRate(commissionRate);

            dto.setEcomPlat(mapData.getUserType() == 0 ? EcomPlat.TAOBAO : EcomPlat.TMALL);
            dto.setImgList(mapData.getSmallImages());
            dto.setPicUrl(mapData.getPictUrl());
            try {
                dto.setSales(mapData.getVolume() == null ? 0 : mapData.getVolume().intValue());
            } catch (Exception e) {
                dto.setSales(0);
            }
            dto.setStatus(CommissionItemStatus.ONSALE);
            dto.setChannelId(context.getChannelId());
            dto.setRecommend(context.isRecommend());
            dto.setShopcatId(context.getShopcatId());
            dtoList.add(dto);
        }
        return dtoList;
    }

    @Override
    public Class<TbkDgOptimusMaterialContext> getContextType() {
        return TbkDgOptimusMaterialContext.class;
    }

    @Override
    protected ItemSelectionType getItemSelectionType() {
        return ItemSelectionType.TBK_DG_OPTIMUS_MATERIAL;
    }
}
