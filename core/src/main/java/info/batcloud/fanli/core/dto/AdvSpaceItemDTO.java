package info.batcloud.fanli.core.dto;

public class AdvSpaceItemDTO {

    private Long id;

    private AdvDTO adv;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AdvDTO getAdv() {
        return adv;
    }

    public void setAdv(AdvDTO adv) {
        this.adv = adv;
    }
}
