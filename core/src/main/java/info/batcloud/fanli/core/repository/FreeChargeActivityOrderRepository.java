package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.FreeChargeActivityOrder;
import info.batcloud.fanli.core.enums.FreeChargeActivityOrderStatus;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;

public interface FreeChargeActivityOrderRepository extends PagingAndSortingRepository<FreeChargeActivityOrder, Long>,
        JpaSpecificationExecutor<FreeChargeActivityOrder> {

    int countByUserIdAndFreeChargeActivityId(long userId, long activityId);

    int countByUserIdAndFreeChargeActivityIdAndCreateTimeBetween(long id, long activityId, Date startTime, Date endTime);

    int countByUserIdAndItemIdAndStatus(long userId, long itemId, FreeChargeActivityOrderStatus status);

    FreeChargeActivityOrder findByUserIdAndItemId(long userId, long itemId);
    FreeChargeActivityOrder findByCommissionOrderId(long commissionOrderId);

    List<FreeChargeActivityOrder> findByStatus(FreeChargeActivityOrderStatus status);
}
