package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.RedPacketDTO;
import info.batcloud.fanli.core.enums.ActivityFreq;
import info.batcloud.fanli.core.enums.RedPacketStatus;
import info.batcloud.fanli.core.enums.RedPacketType;
import info.batcloud.fanli.core.enums.WalletFlowDetailType;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public interface RedPacketService {

    long createRedPacket(RedPacketCreateParam param);

    void updateRedPacket(long id, RedPacketUpdateParam param);

    void setStatus(long id, RedPacketStatus status);

    int grantToUser(long userId);

    Paging<RedPacketDTO> search(SearchParam param);

    class SearchParam extends PagingParam {
        private RedPacketStatus status;
        private RedPacketType type;

        public RedPacketType getType() {
            return type;
        }

        public void setType(RedPacketType type) {
            this.type = type;
        }

        public RedPacketStatus getStatus() {
            return status;
        }

        public void setStatus(RedPacketStatus status) {
            this.status = status;
        }
    }

    class RedPacketCreateParam {

        public WalletFlowDetailType moneyWalletFlowDetailType;
        public WalletFlowDetailType integralWalletFlowDetailType;

        private String name;

        private String description;

        /**
         * 总红包数量
         */
        private int totalNum;

        /**
         * 已发放的数量
         */
        private int drawNum;

        private RedPacketType type;

        private float integral;

        private float money;

        private int validDayNum;

        private int maxUserGrantNum;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date startTime;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date endTime;

        private RedPacketStatus status;

        private ActivityFreq freq;

        private Long crowdId;

        public WalletFlowDetailType getMoneyWalletFlowDetailType() {
            return moneyWalletFlowDetailType;
        }

        public void setMoneyWalletFlowDetailType(WalletFlowDetailType moneyWalletFlowDetailType) {
            this.moneyWalletFlowDetailType = moneyWalletFlowDetailType;
        }

        public WalletFlowDetailType getIntegralWalletFlowDetailType() {
            return integralWalletFlowDetailType;
        }

        public void setIntegralWalletFlowDetailType(WalletFlowDetailType integralWalletFlowDetailType) {
            this.integralWalletFlowDetailType = integralWalletFlowDetailType;
        }

        public int getValidDayNum() {
            return validDayNum;
        }

        public void setValidDayNum(int validDayNum) {
            this.validDayNum = validDayNum;
        }

        public int getMaxUserGrantNum() {
            return maxUserGrantNum;
        }

        public void setMaxUserGrantNum(int maxUserGrantNum) {
            this.maxUserGrantNum = maxUserGrantNum;
        }

        public ActivityFreq getFreq() {
            return freq;
        }

        public void setFreq(ActivityFreq freq) {
            this.freq = freq;
        }

        public float getIntegral() {
            return integral;
        }

        public void setIntegral(float integral) {
            this.integral = integral;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getTotalNum() {
            return totalNum;
        }

        public void setTotalNum(int totalNum) {
            this.totalNum = totalNum;
        }


        public int getDrawNum() {
            return drawNum;
        }

        public void setDrawNum(int drawNum) {
            this.drawNum = drawNum;
        }

        public Long getCrowdId() {
            return crowdId;
        }

        public void setCrowdId(Long crowdId) {
            this.crowdId = crowdId;
        }

        public RedPacketType getType() {
            return type;
        }

        public void setType(RedPacketType type) {
            this.type = type;
        }

        public float getMoney() {
            return money;
        }

        public void setMoney(float money) {
            this.money = money;
        }

        public Date getStartTime() {
            return startTime;
        }

        public void setStartTime(Date startTime) {
            this.startTime = startTime;
        }

        public Date getEndTime() {
            return endTime;
        }

        public void setEndTime(Date endTime) {
            this.endTime = endTime;
        }

        public RedPacketStatus getStatus() {
            return status;
        }

        public void setStatus(RedPacketStatus status) {
            this.status = status;
        }
    }

    class RedPacketUpdateParam {

        public WalletFlowDetailType moneyWalletFlowDetailType;
        public WalletFlowDetailType integralWalletFlowDetailType;

        private String name;

        private String description;

        /**
         * 总红包数量
         */
        private int totalNum;

        /**
         * 已发放的数量
         */
        private int drawNum;

        private int validDayNum;

        private int maxUserGrantNum;


        private Long crowdId;

        private RedPacketType type;

        private float integral;

        private float money;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date startTime;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date endTime;

        private ActivityFreq freq;

        public WalletFlowDetailType getMoneyWalletFlowDetailType() {
            return moneyWalletFlowDetailType;
        }

        public void setMoneyWalletFlowDetailType(WalletFlowDetailType moneyWalletFlowDetailType) {
            this.moneyWalletFlowDetailType = moneyWalletFlowDetailType;
        }

        public WalletFlowDetailType getIntegralWalletFlowDetailType() {
            return integralWalletFlowDetailType;
        }

        public void setIntegralWalletFlowDetailType(WalletFlowDetailType integralWalletFlowDetailType) {
            this.integralWalletFlowDetailType = integralWalletFlowDetailType;
        }

        public int getValidDayNum() {
            return validDayNum;
        }

        public void setValidDayNum(int validDayNum) {
            this.validDayNum = validDayNum;
        }

        public int getMaxUserGrantNum() {
            return maxUserGrantNum;
        }

        public void setMaxUserGrantNum(int maxUserGrantNum) {
            this.maxUserGrantNum = maxUserGrantNum;
        }

        public ActivityFreq getFreq() {
            return freq;
        }

        public void setFreq(ActivityFreq freq) {
            this.freq = freq;
        }

        public float getIntegral() {
            return integral;
        }

        public void setIntegral(float integral) {
            this.integral = integral;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getTotalNum() {
            return totalNum;
        }

        public void setTotalNum(int totalNum) {
            this.totalNum = totalNum;
        }

        public int getDrawNum() {
            return drawNum;
        }

        public void setDrawNum(int drawNum) {
            this.drawNum = drawNum;
        }

        public Long getCrowdId() {
            return crowdId;
        }

        public void setCrowdId(Long crowdId) {
            this.crowdId = crowdId;
        }

        public RedPacketType getType() {
            return type;
        }

        public void setType(RedPacketType type) {
            this.type = type;
        }

        public float getMoney() {
            return money;
        }

        public void setMoney(float money) {
            this.money = money;
        }

        public Date getStartTime() {
            return startTime;
        }

        public void setStartTime(Date startTime) {
            this.startTime = startTime;
        }

        public Date getEndTime() {
            return endTime;
        }

        public void setEndTime(Date endTime) {
            this.endTime = endTime;
        }
    }

}
