package info.batcloud.fanli.core.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import info.batcloud.fanli.core.enums.EcomPlat;
import info.batcloud.fanli.core.enums.SellerPromotionItemOrderStatus;
import info.batcloud.fanli.core.helper.UrlHelper;

import java.util.Date;

public class SellerPromotionItemOrderDTO {

    private Long id;

    private Long sellerPromotionItemId;

    private String sellerPromotionItemPicUrl;

    private float sellerPromotionItemOriginPrice;

    private float sellerPromotionItemPrice;

    private float sellerPromotionItemRebateFee;

    private String sellerPromotionItemTitle;

    private String sellerPromotionItemShopTitle;

    private EcomPlat ecomPlat;

    private Long userId;

    private Long sellerId;

    private String userPhone;

    private String userNickname;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    private String outTradeNo; //外部订单号

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    private String verifyRemark;

    private SellerPromotionItemOrderStatus status;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date rebateDeadline;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date verifyTime;

    public Date getVerifyTime() {
        return verifyTime;
    }

    public void setVerifyTime(Date verifyTime) {
        this.verifyTime = verifyTime;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Date getRebateDeadline() {
        return rebateDeadline;
    }

    public void setRebateDeadline(Date rebateDeadline) {
        this.rebateDeadline = rebateDeadline;
    }

    public float getSellerPromotionItemPrice() {
        return sellerPromotionItemPrice;
    }

    public void setSellerPromotionItemPrice(float sellerPromotionItemPrice) {
        this.sellerPromotionItemPrice = sellerPromotionItemPrice;
    }

    public String getSellerPromotionItemShopTitle() {
        return sellerPromotionItemShopTitle;
    }

    public void setSellerPromotionItemShopTitle(String sellerPromotionItemShopTitle) {
        this.sellerPromotionItemShopTitle = sellerPromotionItemShopTitle;
    }

    public EcomPlat getEcomPlat() {
        return ecomPlat;
    }

    public void setEcomPlat(EcomPlat ecomPlat) {
        this.ecomPlat = ecomPlat;
    }

    public String getEcomPlatTitle() {
        return ecomPlat == null ? null : ecomPlat.getTitle();
    }

    public String getStatusTitle() {
        return status == null ? null : status.getTitle();
    }

    public String getSellerPromotionItemTitle() {
        return sellerPromotionItemTitle;
    }

    public void setSellerPromotionItemTitle(String sellerPromotionItemTitle) {
        this.sellerPromotionItemTitle = sellerPromotionItemTitle;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSellerPromotionItemId() {
        return sellerPromotionItemId;
    }

    public void setSellerPromotionItemId(Long sellerPromotionItemId) {
        this.sellerPromotionItemId = sellerPromotionItemId;
    }

    public String getSellerPromotionItemPicUrl() {
        return UrlHelper.toUrl(sellerPromotionItemPicUrl);
    }

    public void setSellerPromotionItemPicUrl(String sellerPromotionItemPicUrl) {
        this.sellerPromotionItemPicUrl = sellerPromotionItemPicUrl;
    }

    public float getSellerPromotionItemOriginPrice() {
        return sellerPromotionItemOriginPrice;
    }

    public void setSellerPromotionItemOriginPrice(float sellerPromotionItemOriginPrice) {
        this.sellerPromotionItemOriginPrice = sellerPromotionItemOriginPrice;
    }

    public float getSellerPromotionItemRebateFee() {
        return sellerPromotionItemRebateFee;
    }

    public void setSellerPromotionItemRebateFee(float sellerPromotionItemRebateFee) {
        this.sellerPromotionItemRebateFee = sellerPromotionItemRebateFee;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserNickname() {
        return userNickname;
    }

    public void setUserNickname(String userNickname) {
        this.userNickname = userNickname;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getVerifyRemark() {
        return verifyRemark;
    }

    public void setVerifyRemark(String verifyRemark) {
        this.verifyRemark = verifyRemark;
    }

    public SellerPromotionItemOrderStatus getStatus() {
        return status;
    }

    public void setStatus(SellerPromotionItemOrderStatus status) {
        this.status = status;
    }

    public boolean isCanFillTradeNo() {
        if(status == null) {
            return false;
        }
        return status == SellerPromotionItemOrderStatus.WAIT_VERIFY || status == SellerPromotionItemOrderStatus.WAIT_FILL_TRADE_NO || status == SellerPromotionItemOrderStatus.VERIFY_FAIL;
    }
}
