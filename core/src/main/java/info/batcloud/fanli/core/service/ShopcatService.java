package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.dto.ShopcatDTO;
import info.batcloud.fanli.core.entity.Shopcat;
import info.batcloud.fanli.core.exception.ShopcatNameExistsException;

import java.util.List;
import java.util.Set;

public interface ShopcatService {

    void saveShopcat(Shopcat shopcat) throws ShopcatNameExistsException;

    void updateShopcat(Shopcat shopcat) throws ShopcatNameExistsException;

    ShopcatDTO findById(long id);

    ShopcatDTO findRealById(long id);

    void deleteById(long id);

    String getPathName(String path);

    List<ShopcatDTO> findEnabledByParentId(long parentId);

    List<ShopcatDTO> findValidNoIconList();

    List<ShopcatDTO> findEnabledByParentIdList(List<Long> parentIdList);

    List<ShopcatDTO> findEnabledByParentIds(String parentIds);

    List<ShopcatDTO> findEnabledTreeByPath(String path);

    Set<Long> findShopcatIdByTaobaoCatId(long taobaoCatId);

    ShopcatDTO findReal(ShopcatDTO shopcatDTO);

    Set<Long> findShopcatIdByJdCatId(long jdCatId);

    List<ShopcatDTO> findValidByParentId(long parentId);

    List<ShopcatDTO> findValidByTitleLike(String title);

}
