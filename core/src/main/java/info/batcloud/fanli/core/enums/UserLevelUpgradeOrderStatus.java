package info.batcloud.fanli.core.enums;
import info.batcloud.fanli.core.context.StaticContext;

public enum UserLevelUpgradeOrderStatus {


    WAIT_PAY, PAID, DELETED;

    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }

}
