package info.batcloud.fanli.core.settings;

import info.batcloud.fanli.core.helper.OSSImageHelper;
import info.batcloud.fanli.core.service.CommissionItemSearchService;
import info.batcloud.fanli.core.settings.annotation.Single;

import java.io.Serializable;

@Single
public class IndexItemShowSetting implements Serializable {

    private boolean showTitle;
    private String title;
    private String titlePic;
    private int titlePicWidth;
    private int titlePicHeight;
    private Display shopcatBarDisplay;
    private Display filterBarDisplay;
    private ListType listType;

    private CommissionItemSearchService.SearchParam searchParam;

    public String getTitlePicUrl() {
        return OSSImageHelper.toUrl(titlePic);
    }

    public int getTitlePicWidth() {
        return titlePicWidth;
    }

    public void setTitlePicWidth(int titlePicWidth) {
        this.titlePicWidth = titlePicWidth;
    }

    public int getTitlePicHeight() {
        return titlePicHeight;
    }

    public void setTitlePicHeight(int titlePicHeight) {
        this.titlePicHeight = titlePicHeight;
    }

    public Display getShopcatBarDisplay() {
        return shopcatBarDisplay;
    }

    public void setShopcatBarDisplay(Display shopcatBarDisplay) {
        this.shopcatBarDisplay = shopcatBarDisplay;
    }

    public Display getFilterBarDisplay() {
        return filterBarDisplay;
    }

    public void setFilterBarDisplay(Display filterBarDisplay) {
        this.filterBarDisplay = filterBarDisplay;
    }

    public ListType getListType() {
        return listType;
    }

    public void setListType(ListType listType) {
        this.listType = listType;
    }

    public enum ListType {
        GRID, LIST, ALTERNATE
    }

    public enum Display {
        NONE, AFFIX_TOP, SHOW
    }

    public boolean isShowTitle() {
        return showTitle;
    }

    public void setShowTitle(boolean showTitle) {
        this.showTitle = showTitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitlePic() {
        return titlePic;
    }

    public void setTitlePic(String titlePic) {
        this.titlePic = titlePic;
    }

    public CommissionItemSearchService.SearchParam getSearchParam() {
        return searchParam;
    }

    public void setSearchParam(CommissionItemSearchService.SearchParam searchParam) {
        this.searchParam = searchParam;
    }
}
