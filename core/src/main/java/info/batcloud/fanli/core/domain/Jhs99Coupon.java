package info.batcloud.fanli.core.domain;

import info.batcloud.fanli.core.excel.stereotype.XslCol;

import java.util.Date;

public class Jhs99Coupon {

    @XslCol(title = "商品一级类目")
    private String itemCatName;

    @XslCol(title = "店铺名称")
    private String shopName;

    @XslCol(title = "平台类型")
    private String platform;


    @XslCol(title = "商品id")
    private Long itemId;

    @XslCol(title = "商品名称")
    private String title;

    @XslCol(title = "商品主图")
    private String picUrl;

    @XslCol(title = "商品链接")
    private String itemUrl;

    @XslCol(title = "推广链接")
    private String clickUrl;

    @XslCol(title = "商品价格(单位：元)")
    private Float price;

    @XslCol(title = "收入比率(%)")
    private Float commissionRate;

    @XslCol(title = "优惠券面额")
    private String couponInfo;

    @XslCol(title = "券后价")
    private Float zkPrice; //折扣价格

    @XslCol(title = "优惠券总量")
    private int couponTotalCount;

    @XslCol(title = "优惠券剩余量")
    private int couponRemainCount;

    @XslCol(title = "优惠券开始时间")
    private Date couponStartTime;

    @XslCol(title = "优惠券结束时间")
    private Date couponEndTime;

    @XslCol(title = "推广链接")
    private String couponUrl;

    public String getItemCatName() {
        return itemCatName;
    }

    public void setItemCatName(String itemCatName) {
        this.itemCatName = itemCatName;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getItemUrl() {
        return itemUrl;
    }

    public void setItemUrl(String itemUrl) {
        this.itemUrl = itemUrl;
    }

    public String getClickUrl() {
        return clickUrl;
    }

    public void setClickUrl(String clickUrl) {
        this.clickUrl = clickUrl;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(Float commissionRate) {
        this.commissionRate = commissionRate;
    }

    public String getCouponInfo() {
        return couponInfo;
    }

    public void setCouponInfo(String couponInfo) {
        this.couponInfo = couponInfo;
    }

    public Float getZkPrice() {
        return zkPrice;
    }

    public void setZkPrice(Float zkPrice) {
        this.zkPrice = zkPrice;
    }

    public int getCouponTotalCount() {
        return couponTotalCount;
    }

    public void setCouponTotalCount(int couponTotalCount) {
        this.couponTotalCount = couponTotalCount;
    }

    public int getCouponRemainCount() {
        return couponRemainCount;
    }

    public void setCouponRemainCount(int couponRemainCount) {
        this.couponRemainCount = couponRemainCount;
    }

    public Date getCouponStartTime() {
        return couponStartTime;
    }

    public void setCouponStartTime(Date couponStartTime) {
        this.couponStartTime = couponStartTime;
    }

    public Date getCouponEndTime() {
        return couponEndTime;
    }

    public void setCouponEndTime(Date couponEndTime) {
        this.couponEndTime = couponEndTime;
    }

    public String getCouponUrl() {
        return couponUrl;
    }

    public void setCouponUrl(String couponUrl) {
        this.couponUrl = couponUrl;
    }
}
