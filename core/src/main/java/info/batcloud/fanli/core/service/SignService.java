package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.domain.UserSignInfo;

public interface SignService {

    UserSignInfo findByUserId(Long userId);

    UserSignInfo userSign(long userId);
}
