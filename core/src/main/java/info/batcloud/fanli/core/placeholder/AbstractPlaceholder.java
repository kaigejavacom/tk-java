package info.batcloud.fanli.core.placeholder;

import javax.annotation.PostConstruct;

public abstract class AbstractPlaceholder implements Placeholder {

    @PostConstruct
    public void init() {
        PlaceholderContext.register("{" + this.getKey() + "}", this);
    }

    @Override
    public String replace(ReplaceContext context) {
        return replacePlaceholder(context);
    }

    protected abstract String replacePlaceholder(ReplaceContext context);

    protected abstract String getKey();
}
