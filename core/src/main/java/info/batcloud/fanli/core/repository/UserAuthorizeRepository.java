package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.UserAuthorize;
import info.batcloud.fanli.core.enums.Authorize;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserAuthorizeRepository extends CrudRepository<UserAuthorize, Long> {

    int countByUserIdAndAuthorize(long userId, Authorize authorize);

    List<UserAuthorize> findByUserId(long userId);

    @Modifying
    int deleteByUserIdAndAuthorize(long userId, Authorize authorize);

}
