package info.batcloud.fanli.core.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import info.batcloud.fanli.core.enums.CommissionAllotType;
import info.batcloud.fanli.core.enums.CommissionOrderStatus;
import info.batcloud.fanli.core.enums.EcomPlat;

import java.util.Date;

public class UserCommissionOrderDTO {

    private Long id;

    private Long userId;

    private Float settledFee;

    private Float settledCommissionFee;

    private Float settledRewardFee;

    private String itemPicUrl;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date settledTime;

    private String itemTitle;

    private String itemId;

    private Long commissionItemId;

    private String shopTitle;

    private CommissionOrderStatus status;

    private EcomPlat ecomPlat;

    /**
     * 预估佣金金额
     */
    private Float estimateCommissionFee; //预估佣金

    //分配佣金
    private Float allotCommissionRate;

    private float commissionRewardRate;

    /**
     * 订单预估佣金
     * */
    private Float commissionOrderEstimateCommissionFee;
    private Float commissionOrderEstimateCommissionRate;
    private Float commissionOrderCommissionRate;
    private Float commissionOrderCommissionFee;

    private Float commissionRate;

    private Float payFee;

    private CommissionAllotType allotType;

    private String userAvatarUrl;

    private String userNickname;

    private String userPhone;

    private Float estimateRewardFee; //预估奖励

    public Float getSettledCommissionFee() {
        return settledCommissionFee;
    }

    public void setSettledCommissionFee(Float settledCommissionFee) {
        this.settledCommissionFee = settledCommissionFee;
    }

    public Float getSettledRewardFee() {
        return settledRewardFee;
    }

    public void setSettledRewardFee(Float settledRewardFee) {
        this.settledRewardFee = settledRewardFee;
    }

    public float getCommissionRewardRate() {
        return commissionRewardRate;
    }

    public void setCommissionRewardRate(float commissionRewardRate) {
        this.commissionRewardRate = commissionRewardRate;
    }

    public Float getEstimateRewardFee() {
        return estimateRewardFee;
    }

    public void setEstimateRewardFee(Float estimateRewardFee) {
        this.estimateRewardFee = estimateRewardFee;
    }

    public Float getCommissionOrderEstimateCommissionRate() {
        return commissionOrderEstimateCommissionRate;
    }

    public void setCommissionOrderEstimateCommissionRate(Float commissionOrderEstimateCommissionRate) {
        this.commissionOrderEstimateCommissionRate = commissionOrderEstimateCommissionRate;
    }

    public Float getCommissionOrderCommissionRate() {
        return commissionOrderCommissionRate;
    }

    public void setCommissionOrderCommissionRate(Float commissionOrderCommissionRate) {
        this.commissionOrderCommissionRate = commissionOrderCommissionRate;
    }

    public Float getCommissionOrderCommissionFee() {
        return commissionOrderCommissionFee;
    }

    public void setCommissionOrderCommissionFee(Float commissionOrderCommissionFee) {
        this.commissionOrderCommissionFee = commissionOrderCommissionFee;
    }

    public Float getAllotCommissionRate() {
        return allotCommissionRate;
    }

    public void setAllotCommissionRate(Float allotCommissionRate) {
        this.allotCommissionRate = allotCommissionRate;
    }

    public Float getCommissionOrderEstimateCommissionFee() {
        return commissionOrderEstimateCommissionFee;
    }

    public void setCommissionOrderEstimateCommissionFee(Float commissionOrderEstimateCommissionFee) {
        this.commissionOrderEstimateCommissionFee = commissionOrderEstimateCommissionFee;
    }

    public String getUserAvatarUrl() {
        return userAvatarUrl;
    }

    public void setUserAvatarUrl(String userAvatarUrl) {
        this.userAvatarUrl = userAvatarUrl;
    }

    public String getUserNickname() {
        return userNickname;
    }

    public void setUserNickname(String userNickname) {
        this.userNickname = userNickname;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getEcomPlatTitle() {
        return ecomPlat == null ? null : ecomPlat.getTitle();
    }

    public String getAllotTypeTitle() {
        return allotType == null ? null : allotType.getTitle();
    }

    public CommissionAllotType getAllotType() {
        return allotType;
    }

    public void setAllotType(CommissionAllotType allotType) {
        this.allotType = allotType;
    }

    public Date getSettledTime() {
        return settledTime;
    }

    public void setSettledTime(Date settledTime) {
        this.settledTime = settledTime;
    }

    public Long getCommissionItemId() {
        return commissionItemId;
    }

    public void setCommissionItemId(Long commissionItemId) {
        this.commissionItemId = commissionItemId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Float getSettledFee() {
        return settledFee;
    }

    public void setSettledFee(Float settledFee) {
        this.settledFee = settledFee;
    }

    public String getItemPicUrl() {
        return itemPicUrl;
    }

    public void setItemPicUrl(String itemPicUrl) {
        this.itemPicUrl = itemPicUrl;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public void setShopTitle(String shopTitle) {
        this.shopTitle = shopTitle;
    }

    public CommissionOrderStatus getStatus() {
        return status;
    }

    public void setStatus(CommissionOrderStatus status) {
        this.status = status;
    }

    public String getStatusTitle() {
        return getStatus() == null ? null : getStatus().getTitle();
    }

    public EcomPlat getEcomPlat() {
        return ecomPlat;
    }

    public void setEcomPlat(EcomPlat ecomPlat) {
        this.ecomPlat = ecomPlat;
    }

    public Float getEstimateCommissionFee() {
        return estimateCommissionFee;
    }

    public void setEstimateCommissionFee(Float estimateCommissionFee) {
        this.estimateCommissionFee = estimateCommissionFee;
    }

    public Float getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(Float commissionRate) {
        this.commissionRate = commissionRate;
    }

    public Float getPayFee() {
        return payFee;
    }

    public void setPayFee(Float payFee) {
        this.payFee = payFee;
    }
}
