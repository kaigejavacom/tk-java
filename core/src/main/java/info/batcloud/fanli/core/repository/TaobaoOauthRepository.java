package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.TaobaoOauth;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface TaobaoOauthRepository extends PagingAndSortingRepository<TaobaoOauth, Long>{

    TaobaoOauth findByTaobaoUserIdAndSubTaobaoUserId(String taobaoUserId, String subTaobaoUserId);

    TaobaoOauth findByUserIdAndTaobaoUserId(long userId, String taobaoUserId);

    List<TaobaoOauth> findByUserId(long userId);

}
