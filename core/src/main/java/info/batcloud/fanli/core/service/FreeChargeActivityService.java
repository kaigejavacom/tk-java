package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.FreeChargeActivityDTO;
import info.batcloud.fanli.core.domain.Result;
import info.batcloud.fanli.core.enums.ActivityFreq;
import info.batcloud.fanli.core.enums.FreeChargeActivityStatus;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public interface FreeChargeActivityService {

    long createFreeChargeActivity(FreeChargeActivityCreateParam param);

    void updateFreeChargeActivity(long id, FreeChargeActivityUpdateParam param);

    void setStatus(long id, FreeChargeActivityStatus status);

    FreeChargeActivityDTO findById(long id);

    FreeChargeActivityDTO findValidById(long id);

    CheckDrawResult checkUserDraw(long userId, long itemId, long activityId);
    CheckDrawResult checkUserDrawToOrder(long userId, long itemId, long activityId);

    Paging<FreeChargeActivityDTO> search(SearchParam param);

    class CheckDrawResult extends Result {

    }

    class SearchParam extends PagingParam {
        private FreeChargeActivityStatus status;

        public FreeChargeActivityStatus getStatus() {
            return status;
        }

        public void setStatus(FreeChargeActivityStatus status) {
            this.status = status;
        }
    }

    class FreeChargeActivityCreateParam {

        private String name;

        private String description;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date startTime;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date endTime;

        private FreeChargeActivityStatus status;

        private ActivityFreq freq;

        private Long crowdId;

        private String helpUrl;

        private String markIcon;

        private Long materialId;

        private String btnTitle;

        private String confirmDescription;

        private int maxUserDrawNum;

        public int getMaxUserDrawNum() {
            return maxUserDrawNum;
        }

        public void setMaxUserDrawNum(int maxUserDrawNum) {
            this.maxUserDrawNum = maxUserDrawNum;
        }

        public String getConfirmDescription() {
            return confirmDescription;
        }

        public void setConfirmDescription(String confirmDescription) {
            this.confirmDescription = confirmDescription;
        }

        public String getBtnTitle() {
            return btnTitle;
        }

        public void setBtnTitle(String btnTitle) {
            this.btnTitle = btnTitle;
        }

        public Long getMaterialId() {
            return materialId;
        }

        public void setMaterialId(Long materialId) {
            this.materialId = materialId;
        }

        public String getMarkIcon() {
            return markIcon;
        }

        public void setMarkIcon(String markIcon) {
            this.markIcon = markIcon;
        }

        public String getHelpUrl() {
            return helpUrl;
        }

        public void setHelpUrl(String helpUrl) {
            this.helpUrl = helpUrl;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Date getStartTime() {
            return startTime;
        }

        public void setStartTime(Date startTime) {
            this.startTime = startTime;
        }

        public Date getEndTime() {
            return endTime;
        }

        public void setEndTime(Date endTime) {
            this.endTime = endTime;
        }

        public FreeChargeActivityStatus getStatus() {
            return status;
        }

        public void setStatus(FreeChargeActivityStatus status) {
            this.status = status;
        }

        public ActivityFreq getFreq() {
            return freq;
        }

        public void setFreq(ActivityFreq freq) {
            this.freq = freq;
        }

        public Long getCrowdId() {
            return crowdId;
        }

        public void setCrowdId(Long crowdId) {
            this.crowdId = crowdId;
        }
    }

    class FreeChargeActivityUpdateParam {

        private String name;

        private String description;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date startTime;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date endTime;

        private FreeChargeActivityStatus status;

        private ActivityFreq freq;

        private Long crowdId;

        private String helpUrl;

        private String markIcon;

        private Long materialId;

        private String btnTitle;

        private String confirmDescription;

        private int maxUserDrawNum;

        public int getMaxUserDrawNum() {
            return maxUserDrawNum;
        }

        public void setMaxUserDrawNum(int maxUserDrawNum) {
            this.maxUserDrawNum = maxUserDrawNum;
        }

        public String getConfirmDescription() {
            return confirmDescription;
        }

        public void setConfirmDescription(String confirmDescription) {
            this.confirmDescription = confirmDescription;
        }

        public String getBtnTitle() {
            return btnTitle;
        }

        public void setBtnTitle(String btnTitle) {
            this.btnTitle = btnTitle;
        }

        public Long getMaterialId() {
            return materialId;
        }

        public void setMaterialId(Long materialId) {
            this.materialId = materialId;
        }

        public String getMarkIcon() {
            return markIcon;
        }

        public void setMarkIcon(String markIcon) {
            this.markIcon = markIcon;
        }

        public String getHelpUrl() {
            return helpUrl;
        }

        public void setHelpUrl(String helpUrl) {
            this.helpUrl = helpUrl;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Date getStartTime() {
            return startTime;
        }

        public void setStartTime(Date startTime) {
            this.startTime = startTime;
        }

        public Date getEndTime() {
            return endTime;
        }

        public void setEndTime(Date endTime) {
            this.endTime = endTime;
        }

        public FreeChargeActivityStatus getStatus() {
            return status;
        }

        public void setStatus(FreeChargeActivityStatus status) {
            this.status = status;
        }

        public ActivityFreq getFreq() {
            return freq;
        }

        public void setFreq(ActivityFreq freq) {
            this.freq = freq;
        }

        public Long getCrowdId() {
            return crowdId;
        }

        public void setCrowdId(Long crowdId) {
            this.crowdId = crowdId;
        }
    }

}
