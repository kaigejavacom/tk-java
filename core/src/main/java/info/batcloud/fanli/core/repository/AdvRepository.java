package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.Adv;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;

public interface AdvRepository extends PagingAndSortingRepository<Adv, Long>, JpaSpecificationExecutor<Adv> {

    List<Adv> findByIdIn(List<Long> ids);

    List<Adv> findByIdInAndValidAndDeleted(List<Long> ids, boolean valid, boolean deleted);

    List<Adv> findByIdInAndValidAndStartTimeLessThanAndEndTimeGreaterThanAndDeleted(List<Long> ids, boolean valid, Date startTime, Date endTime, boolean deleted);

    Adv findByIdAndValidAndStartTimeLessThanAndEndTimeGreaterThanAndDeleted(long id, boolean valid, Date startTime, Date endTime, boolean deleted);

}
