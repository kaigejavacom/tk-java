package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.SellerPromotionItemDepositOrderDTO;
import info.batcloud.fanli.core.enums.OrderPayType;
import info.batcloud.fanli.core.enums.SellerPromotionItemDepositOrderStatus;

public interface SellerPromotionItemDepositOrderService {

    void createTransferOrder(TransferOrderCreateParam param);

    void verify(long id, VerifyParam param);

    Paging<SellerPromotionItemDepositOrderDTO> search(SearchParam param);

    class VerifyParam {
        private boolean success;
        private String verifyRemark;

        public String getVerifyRemark() {
            return verifyRemark;
        }

        public void setVerifyRemark(String verifyRemark) {
            this.verifyRemark = verifyRemark;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }
    }

    class SearchParam extends PagingParam {
        private SellerPromotionItemDepositOrderStatus status;
        private String alipayTransferTradeNo;
        private Long userId;

        private Long sellerPromotionItemId;

        private OrderPayType payType;

        public OrderPayType getPayType() {
            return payType;
        }

        public void setPayType(OrderPayType payType) {
            this.payType = payType;
        }

        public SellerPromotionItemDepositOrderStatus getStatus() {
            return status;
        }

        public void setStatus(SellerPromotionItemDepositOrderStatus status) {
            this.status = status;
        }

        public String getAlipayTransferTradeNo() {
            return alipayTransferTradeNo;
        }

        public void setAlipayTransferTradeNo(String alipayTransferTradeNo) {
            this.alipayTransferTradeNo = alipayTransferTradeNo;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Long getSellerPromotionItemId() {
            return sellerPromotionItemId;
        }

        public void setSellerPromotionItemId(Long sellerPromotionItemId) {
            this.sellerPromotionItemId = sellerPromotionItemId;
        }
    }

    class TransferOrderCreateParam {
        private String alipayTransferTradeNo;
        private Long sellerPromotionItemId;
        private Long userId;
        private int num;

        public String getAlipayTransferTradeNo() {
            return alipayTransferTradeNo;
        }

        public void setAlipayTransferTradeNo(String alipayTransferTradeNo) {
            this.alipayTransferTradeNo = alipayTransferTradeNo;
        }

        public Long getSellerPromotionItemId() {
            return sellerPromotionItemId;
        }

        public void setSellerPromotionItemId(Long sellerPromotionItemId) {
            this.sellerPromotionItemId = sellerPromotionItemId;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }

    }

}
