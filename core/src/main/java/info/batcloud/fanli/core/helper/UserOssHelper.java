package info.batcloud.fanli.core.helper;

public class UserOssHelper {

    public static String prefix(long userId) {
        return "user/" + userId + "/";
    }

    public static boolean checkUserPrefix(long userId, String s) {
        return s.startsWith(prefix(userId));
    }

}
