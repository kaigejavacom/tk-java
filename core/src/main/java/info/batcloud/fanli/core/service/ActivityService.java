package info.batcloud.fanli.core.service;

public interface ActivityService {

    /**
     * 检查commissionItem和userId是否已经参与活动
     * */
    boolean isCommissionItemJoinFlashSaleWithUser(long userId, long commissionItemId);

}
