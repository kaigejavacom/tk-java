package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.ShopcatStatus;
import info.batcloud.fanli.core.helper.OSSImageHelper;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Entity
@Cacheable
public class Shopcat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private String icon;

    @Enumerated(EnumType.STRING)
    private ShopcatStatus status;

    private int idx;

    private String path;

    private long parentId;

    //查询关键词
    private String keyword;

    private boolean searchByKeyword;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @Fetch(FetchMode.JOIN)
    private Shopcat ref;

    public Shopcat getRef() {
        return ref;
    }

    public void setRef(Shopcat ref) {
        this.ref = ref;
    }


    public boolean isSearchByKeyword() {
        return searchByKeyword;
    }

    public void setSearchByKeyword(boolean searchByKeyword) {
        this.searchByKeyword = searchByKeyword;
    }


    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public ShopcatStatus getStatus() {
        return status;
    }

    public String getStatusTitle() {
        return getStatus() == null ? null : getStatus().getTitle();
    }

    public void setStatus(ShopcatStatus status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getIconUrl() {
        return OSSImageHelper.toUrl(getIcon());
    }

}
