package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.SellerPromotionItemOrder;
import info.batcloud.fanli.core.enums.EcomPlat;
import info.batcloud.fanli.core.enums.SellerPromotionItemOrderStatus;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;

public interface SellerPromotionItemOrderRepository extends PagingAndSortingRepository<SellerPromotionItemOrder, Long>, JpaSpecificationExecutor<SellerPromotionItemOrder> {

    SellerPromotionItemOrder findTopByUserIdAndSellerPromotionItemIdOrderByIdDesc(long userId, long sellerPromotionItemId);

    SellerPromotionItemOrder findByUserIdAndId(long userId, long id);

    int countByEcomPlatAndOutTradeNo(EcomPlat plat, String outTradeNo);

    int countByUserIdAndSellerPromotionItemIdAndStatus(long userId, long sellerPromotionItemId, SellerPromotionItemOrderStatus status);

    int countByUserIdAndStatus(long userId, SellerPromotionItemOrderStatus status);

    int countByUserIdAndStatusIn(long userId, List<SellerPromotionItemOrderStatus> statusList);

    List<SellerPromotionItemOrder> findByStatusAndCreateTimeBefore(SellerPromotionItemOrderStatus status, Date beforeTime);
    List<SellerPromotionItemOrder> findByStatusAndVerifyDeadlineBefore(SellerPromotionItemOrderStatus status, Date beforeTime);
}
