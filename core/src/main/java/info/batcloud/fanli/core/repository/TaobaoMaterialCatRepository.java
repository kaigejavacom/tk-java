package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.TaobaoMaterialCat;
import info.batcloud.fanli.core.enums.TaobaoMaterialCatStatus;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TaobaoMaterialCatRepository extends CrudRepository<TaobaoMaterialCat, Long> {

    List<TaobaoMaterialCat> findByStatusNot(TaobaoMaterialCatStatus status);
    List<TaobaoMaterialCat> findByStatus(TaobaoMaterialCatStatus status);

}
