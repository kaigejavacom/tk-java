package info.batcloud.fanli.core.enums;

/**
 * 账号授权平台
 * */
public enum AuthPlatform {

    SELF("自有"), TAOBAO("淘宝"), JD("京东"), WEIXIN("微信"), SMS("短信");

    public String title;
    AuthPlatform(String title) {
        this.title = title;
    }

}
