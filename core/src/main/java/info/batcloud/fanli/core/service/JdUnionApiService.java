package info.batcloud.fanli.core.service;

import com.jd.open.api.sdk.JdException;
import info.batcloud.laxiaoke.open.request.jd.union.*;
import info.batcloud.laxiaoke.open.response.jd.union.*;

public interface JdUnionApiService {

    CouponGetCodeBySubUnionIdResponse getCodeBySubUnionId(CouponGetCodeBySubUnionIdRequest request) throws JdException;

    CouponGetCodeByUnionIdResponse getCodeByUnionId(CouponGetCodeByUnionIdRequest request) throws JdException;

    WxsqGetCodeBySubUnionIdResponse getCodeBySubUnionId(WxsqGetCodeBySubUnionIdRequest request) throws JdException;

    QueryCouponGoodsResponse queryCouponGoodsResult(QueryCouponGoodsRequest reques) throws JdException;

    QueryOrderResponse queryOrderResult(QueryOrderRequest request) throws JdException;

    GoodsInfoResponse getGoodsInfo(GoodsInfoRequest request) throws JdException;

    GoodsCategoryQueryResponse queryGoodsCategory(GoodsCategoryQueryRequest request) throws JdException;

    CreatePromotionSiteBatchResponse createPromotionSiteBatch(CreatePromotionSiteBatchRequest request) throws JdException;
}
