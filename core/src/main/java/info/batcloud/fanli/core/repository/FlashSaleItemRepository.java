package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.FlashSaleItem;
import info.batcloud.fanli.core.enums.FlashSaleItemStatus;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;

public interface FlashSaleItemRepository extends PagingAndSortingRepository<FlashSaleItem, Long>, JpaSpecificationExecutor<FlashSaleItem> {

    int countByDateAndSeasonAndItemId(String date, String season, Long itemId);

    List<FlashSaleItem> findByIdGreaterThanAndStartTimeGreaterThanAndStatusOrderByIdDesc(long id, Date startTime, FlashSaleItemStatus status);
}
