package info.batcloud.fanli.core.entity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import info.batcloud.fanli.core.enums.EcomPlat;
import org.apache.commons.lang.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type")
@Entity
public abstract class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String title;

    @NotNull
    private String picUrl;

    private String whitePicUrl;

    @NotNull
    private Float price;

    @NotNull
    private Float originPrice;

    private String shopTitle;

    @NotNull
    private Date createTime;

    private Date updateTime;

    private Date listTime; //上架时间

    private Date delistTime;//下架时间

    private String description;

    private String summary;

    private String imgs;

    @ManyToOne(cascade = CascadeType.REFRESH)
    private Shopcat shopcat;

    //来源电商平台
    @Enumerated(EnumType.STRING)
    private EcomPlat ecomPlat;

    private Long sourceItemId;

    @NotNull
    private String sourceItemUrl;

    private boolean freeShipment;

    public String getWhitePicUrl() {
        return whitePicUrl;
    }

    public void setWhitePicUrl(String whitePicUrl) {
        this.whitePicUrl = whitePicUrl;
    }

    public void setImgList(List<String> list) {
        this.setImgs(JSON.toJSONString(list));
    }

    public List<String> getImgList() {
        if (StringUtils.isBlank(this.getImgs())) {
            return new ArrayList<>();
        }
        return JSON.parseObject(this.getImgs(), new TypeReference<List<String>>() {
        });
    }

    public String getImgs() {
        return imgs;
    }

    public void setImgs(String imgs) {
        this.imgs = imgs;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getOriginPrice() {
        return originPrice;
    }

    public void setOriginPrice(Float originPrice) {
        this.originPrice = originPrice;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public void setShopTitle(String shopTitle) {
        this.shopTitle = shopTitle;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getListTime() {
        return listTime;
    }

    public void setListTime(Date listTime) {
        this.listTime = listTime;
    }

    public Date getDelistTime() {
        return delistTime;
    }

    public void setDelistTime(Date delistTime) {
        this.delistTime = delistTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Shopcat getShopcat() {
        return shopcat;
    }

    public void setShopcat(Shopcat shopcat) {
        this.shopcat = shopcat;
    }

    public EcomPlat getEcomPlat() {
        return ecomPlat;
    }

    public void setEcomPlat(EcomPlat ecomPlat) {
        this.ecomPlat = ecomPlat;
    }

    public Long getSourceItemId() {
        return sourceItemId;
    }

    public void setSourceItemId(Long sourceItemId) {
        this.sourceItemId = sourceItemId;
    }

    public String getSourceItemUrl() {
        return sourceItemUrl;
    }

    public void setSourceItemUrl(String sourceItemUrl) {
        this.sourceItemUrl = sourceItemUrl;
    }

    public boolean isFreeShipment() {
        return freeShipment;
    }

    public void setFreeShipment(boolean freeShipment) {
        this.freeShipment = freeShipment;
    }

}
