package info.batcloud.fanli.core.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("taobao")
public class TaobaoOauth extends Oauth{

    private int reExpiresIn;

    private int r1ExpiresIn;

    private int r2ExpiresIn;

    private int w1ExpiresIn;

    private int w2ExpiresIn;

    private String taobaoUserNick;

    private String taobaoUserId;

    private String subTaobaoUserId;

    private String subTaobaoUserNick;

    public int getReExpiresIn() {
        return reExpiresIn;
    }

    public void setReExpiresIn(int reExpiresIn) {
        this.reExpiresIn = reExpiresIn;
    }

    public int getR1ExpiresIn() {
        return r1ExpiresIn;
    }

    public void setR1ExpiresIn(int r1ExpiresIn) {
        this.r1ExpiresIn = r1ExpiresIn;
    }

    public int getR2ExpiresIn() {
        return r2ExpiresIn;
    }

    public void setR2ExpiresIn(int r2ExpiresIn) {
        this.r2ExpiresIn = r2ExpiresIn;
    }

    public int getW1ExpiresIn() {
        return w1ExpiresIn;
    }

    public void setW1ExpiresIn(int w1ExpiresIn) {
        this.w1ExpiresIn = w1ExpiresIn;
    }

    public int getW2ExpiresIn() {
        return w2ExpiresIn;
    }

    public void setW2ExpiresIn(int w2ExpiresIn) {
        this.w2ExpiresIn = w2ExpiresIn;
    }

    public String getTaobaoUserNick() {
        return taobaoUserNick;
    }

    public void setTaobaoUserNick(String taobaoUserNick) {
        this.taobaoUserNick = taobaoUserNick;
    }

    public String getTaobaoUserId() {
        return taobaoUserId;
    }

    public void setTaobaoUserId(String taobaoUserId) {
        this.taobaoUserId = taobaoUserId;
    }

    public String getSubTaobaoUserId() {
        return subTaobaoUserId;
    }

    public void setSubTaobaoUserId(String subTaobaoUserId) {
        this.subTaobaoUserId = subTaobaoUserId;
    }

    public String getSubTaobaoUserNick() {
        return subTaobaoUserNick;
    }

    public void setSubTaobaoUserNick(String subTaobaoUserNick) {
        this.subTaobaoUserNick = subTaobaoUserNick;
    }
}
