package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

import java.io.Serializable;

public enum UserAgentApplicationStatus implements EnumTitle, Serializable {

    WAIT_VERIFY, PASSED, REJECT;

    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, "", null);
    }

}
