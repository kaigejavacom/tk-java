package info.batcloud.fanli.core.enums;

public enum VfileStatus {

    ENABLED("可用"), LOCKED("锁定"), DELETED("已删除");

    public String title;

    VfileStatus(String title) {
        this.title = title;
    }

}
