package info.batcloud.fanli.core.enums;
import info.batcloud.fanli.core.context.StaticContext;

public enum SellerPromotionItemDepositOrderStatus {


    WAIT_PAY, TRANSFER_WAIT_VERIFY, VERIFY_FAIL, PAID, DELETED;

    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }

}
