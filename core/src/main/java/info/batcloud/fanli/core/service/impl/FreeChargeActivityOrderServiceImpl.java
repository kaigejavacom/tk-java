package info.batcloud.fanli.core.service.impl;

import com.ctospace.archit.common.pagination.Paging;
import info.batcloud.fanli.core.dto.FreeChargeActivityOrderDTO;
import info.batcloud.fanli.core.entity.CommissionItem;
import info.batcloud.fanli.core.entity.CommissionOrder;
import info.batcloud.fanli.core.entity.FreeChargeActivityOrder;
import info.batcloud.fanli.core.enums.FreeChargeActivityOrderStatus;
import info.batcloud.fanli.core.enums.WalletFlowDetailType;
import info.batcloud.fanli.core.event.CommissionOrderChangeEvent;
import info.batcloud.fanli.core.exception.BizException;
import info.batcloud.fanli.core.helper.PagingHelper;
import info.batcloud.fanli.core.repository.*;
import info.batcloud.fanli.core.service.FreeChargeActivityOrderService;
import info.batcloud.fanli.core.service.WalletService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.context.ApplicationListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class FreeChargeActivityOrderServiceImpl implements FreeChargeActivityOrderService,
        ApplicationListener<CommissionOrderChangeEvent> {

    private static final Logger logger = LoggerFactory.getLogger(FreeChargeActivityOrderService.class);

    @Inject
    private FreeChargeActivityOrderRepository freeChargeActivityOrderRepository;

    @Inject
    private CommissionItemRepository commissionItemRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private FreeChargeActivityRepository freeChargeActivityRepository;

    @Inject
    private CommissionOrderRepository commissionOrderRepository;

    @Inject
    private WalletService walletService;

    @Override
    public long createFreeChargeActivityOrder(FreeChargeActivityOrderCreateParam param) {
        FreeChargeActivityOrder order = new FreeChargeActivityOrder();
        order.setCreateTime(new Date());
        order.setStatus(FreeChargeActivityOrderStatus.WAIT_VERIFY);
        order.setItem(commissionItemRepository.findOne(param.getItemId()));
        order.setUser(userRepository.findOne(param.getUserId()));
        order.setFreeChargeActivity(freeChargeActivityRepository.findOne(param.getFreeChargeActivityId()));
        order.setFreeFee(order.getItem().getPrice());
        freeChargeActivityOrderRepository.save(order);
        return order.getId();
    }

    @Override
    public long countFreeChargeActivityByUserIdAndActivityId(long userId, long freeChargeActivityId) {
        return freeChargeActivityOrderRepository.countByUserIdAndFreeChargeActivityId(userId, freeChargeActivityId);
    }

    @Override
    public long countFreeChargeActivityByUserIdAndActivityIdAndCreateTimeBetween(long userId, long freeChargeActivityId, Date startTime, Date endTime) {
        return freeChargeActivityOrderRepository.countByUserIdAndFreeChargeActivityIdAndCreateTimeBetween(userId, freeChargeActivityId, startTime, endTime);
    }

    @Override
    public boolean checkHasWaitVerifyOrderByUserIdAndItemId(long userId, long itemId) {
        return freeChargeActivityOrderRepository.countByUserIdAndItemIdAndStatus(userId, itemId, FreeChargeActivityOrderStatus.WAIT_VERIFY) > 0;
    }

    @Override
    @Transactional
    public synchronized void settle() {
        logger.info("开始结算免单活动订单");
        List<FreeChargeActivityOrder> orders = freeChargeActivityOrderRepository.findByStatus(FreeChargeActivityOrderStatus.WAIT_SETTLE);
        for (FreeChargeActivityOrder order : orders) {
            this.settleOrder(order);
        }
        logger.info("免单订单结算完成,共结算" + orders.size() + "个订单");
    }

    @Override
    @Transactional
    public void settle(List<Long> ids) {
        logger.info("开始结算免单活动订单");
        Iterable<FreeChargeActivityOrder> orders = freeChargeActivityOrderRepository.findAll(ids);
        orders.forEach( order -> settleOrder(order));
        logger.info("免单订单结算完成,共结算" + ids.size() + "个订单");
    }

    @Override
    @Transactional
    public void deleteById(long id) {
        FreeChargeActivityOrder order = freeChargeActivityOrderRepository.findOne(id);
        if(order.getStatus() != FreeChargeActivityOrderStatus.WAIT_VERIFY) {
            throw new BizException("只有待审核状态的才可以删除！");
        }
        freeChargeActivityOrderRepository.delete(order);
    }

    private void settleOrder(FreeChargeActivityOrder order) {
        if (order.getStatus() != FreeChargeActivityOrderStatus.WAIT_SETTLE) {
            logger.error("该免单活动状态不是待结算状态，无法结算，id:" + order.getId());
            return;
        }
        order.setStatus(FreeChargeActivityOrderStatus.SETTLED);
        order.setUpdateTime(new Date());
        walletService.addMoney(order.getUser().getId(), order.getFreeFee(),
                WalletFlowDetailType.FREE_CHARGE_ACTIVITY_REBATE, order.getId() + "");
        freeChargeActivityOrderRepository.save(order);
    }

    @Override
    public Paging<FreeChargeActivityOrderDTO> search(SearchParam param) {
        Specification<FreeChargeActivityOrder> specification = (root, query, cb) -> {
            if(query.getResultType() != Long.class) {
                root.fetch("user", JoinType.LEFT);
                root.fetch("commissionOrder", JoinType.LEFT);
                root.fetch("freeChargeActivity", JoinType.LEFT);
                root.fetch("item", JoinType.LEFT);
            }
            Predicate predicate = cb.conjunction();
            List<Expression<Boolean>> expressions = predicate.getExpressions();
            if (param.getStatus() != null) {
                expressions.add(cb.equal(root.get("status"), param.getStatus()));
            }
            if (param.getUserId() != null) {
                expressions.add(cb.equal(root.get("user").get("id"), param.getUserId()));
            }
            if (param.getItemId() != null) {
                expressions.add(cb.equal(root.get("item").get("id"), param.getItemId()));
            }
            if (StringUtils.isNotEmpty(param.getOrderNo())) {
                expressions.add(cb.equal(root.get("commissionOrder").get("orderNo"), param.getOrderNo()));
            }
            if (param.getFreeChargeActivityId() != null) {
                expressions.add(cb.equal(root.get("freeChargeActivity").get("id"), param.getFreeChargeActivityId()));
            }
            if (param.getMinCreateTime() != null) {
                expressions.add(cb.greaterThanOrEqualTo(root.get("createTime"), param.getMinCreateTime()));
            }
            if (param.getMaxCreateTime() != null) {
                expressions.add(cb.lessThanOrEqualTo(root.get("createTime"), param.getMaxCreateTime()));
            }
            return predicate;
        };
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        Pageable pageable = new PageRequest(param.getPage() - 1,
                param.getPageSize(), sort);
        Page<FreeChargeActivityOrder> page = freeChargeActivityOrderRepository.findAll(specification, pageable);
        return PagingHelper.of(toBOList(page.getContent()), page.getTotalElements(), param.getPage(), param.getPageSize());
    }

    private List<FreeChargeActivityOrderDTO> toBOList(List<FreeChargeActivityOrder> list) {
        List<FreeChargeActivityOrderDTO> boList = new ArrayList<>();
        for (FreeChargeActivityOrder order : list) {
            FreeChargeActivityOrderDTO bo = new FreeChargeActivityOrderDTO();
            BeanUtils.copyProperties(order, bo);
            bo.setFreeChargeActivityId(order.getFreeChargeActivity().getId());
            bo.setFreeChargeActivityName(order.getFreeChargeActivity().getName());
            bo.setUserId(order.getUser().getId());
            bo.setUserAvatarUrl(order.getUser().getAvatarUrl());
            bo.setUserNickname(order.getUser().getNickname());
            bo.setItemId(order.getItem().getId());
            bo.setItemPicUrl(order.getItem().getPicUrl());
            bo.setItemPrice(order.getItem().getPrice());
            bo.setItemTitle(order.getItem().getTitle());
            if(order.getCommissionOrder() != null) {
                bo.setOrderNo(order.getCommissionOrder().getOrderNo());
            }
            bo.setItemShopTitle(order.getItem().getShopTitle());
            bo.setFreeFee(order.getFreeFee());
            if (order.getUpdateTime() == null) {
                bo.setUpdateTime(bo.getCreateTime());
            } else {
                bo.setUpdateTime(bo.getUpdateTime());
            }
            boList.add(bo);
        }
        return boList;
    }

    @Override
    public void onApplicationEvent(CommissionOrderChangeEvent event) {
        Long commissionOrderId = (Long) event.getSource();
        FreeChargeActivityOrder order = freeChargeActivityOrderRepository.findByCommissionOrderId(commissionOrderId);
        CommissionOrder co = commissionOrderRepository.findOne(commissionOrderId);
        if (co.getCommissionItemId() == null
                || co.getUserId() == null) {
            return;
        }
        if (order == null) {
            CommissionItem ci = commissionItemRepository.findOne(co.getCommissionItemId());
            order = freeChargeActivityOrderRepository.findByUserIdAndItemId(co.getUserId(), ci.getId());
        }
        if (order == null) {
            return;
        }
        switch (co.getStatus()) {
            case PAID:
                if (order.getStatus() == FreeChargeActivityOrderStatus.WAIT_VERIFY) {
                    order.setStatus(FreeChargeActivityOrderStatus.WAIT_RECEIVE_GOODS);
                    order.setCommissionOrder(co);
                }
                break;
            case WAIT_SETTLE:
                if (order.getStatus() == FreeChargeActivityOrderStatus.WAIT_RECEIVE_GOODS) {
                    order.setStatus(FreeChargeActivityOrderStatus.WAIT_SETTLE);
                }
                break;
        }
        freeChargeActivityOrderRepository.save(order);

    }
}
