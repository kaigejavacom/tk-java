$(function() {
    var bg = chrome.extension.getBackgroundPage();
    $('#fetchTbkOrderBtn').click(function() {
        var context = Utils.serializeToObject($('#tbkOrderFetchForm'))
        context.id = new Date().getTime() + ''
        bg.orderGrabber.startContext(context)
        loadTaskList()
    })
    $('#fetchTbkOrderTaskBtn').click(function() {
        var context = Utils.serializeToObject($('#tbkOrderFetchForm'))
        bg.orderGrabber.addContext(context)
        bg.orderGrabber.startContext(context)
        loadTaskList()
    })
    $('#fetchTbkOrderStopBtn').click(function() {
        bg.orderGrabber.stop()
        loadTaskList()
    })
    $('#fetchTbkOrderClearBtn').click(function() {
        bg.orderGrabber.clear()
        loadTaskList()
    })
    $('.datetimepicker').datetimepicker({minView:'hour', format:'yyyy-mm-dd hh:ii:ss'});
    var tplText = $('#taskList').html();
    $('#taskList').empty()
    function loadTaskList() {
        $('#taskList').empty()
        var map = bg.orderGrabber.getContextMap();
        for(var key in map) {
            var tpl = tplText.replace(/{{(.+?)}}/gi, function($0, $1) {
                if($1 == 'payStatus') {
                    return bg.orderGrabber.getPayStatus(map[key][$1])
                }
                if($1 == 'queryType') {
                    return bg.orderGrabber.getQueryType(map[key][$1])
                }
                return map[key][$1]
            })
            tpl = $(tpl)
            tpl.find(".del-btn").click(function() {
                bg.orderGrabber.removeContext(key)
                loadTaskList()
            })
            $('#taskList').append(tpl)
        }
    }
    loadTaskList()
    setInterval(loadTaskList, 10000)
})

chrome.extension.onMessage.addListener(
    function(request, sender, sendResponse) {
        if(request.type == 'orderFetchMsg') {
            $("#resultMsg").val( request.msg + '\n' + $("#resultMsg").val())
        }
    }
);

