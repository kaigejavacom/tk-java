$(function() {
    $('input[data-cache]').change(function() {
        var input = $(this)
        var cache = input.attr("data-cache")
        localStorage.setItem(cache, input.val())
    })
    $('input[data-cache]').each(function() {
        var input = $(this)
        var cache = input.attr("data-cache")
        var val = localStorage.getItem(cache);
        if(val != null) {
            input.val(localStorage.getItem(cache))
        }
    })
})