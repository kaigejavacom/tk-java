package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.api.constants.Constants;
import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.settings.WeixinSetting;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import static info.batcloud.fanli.api.constants.Constants.STATE_INVITATION;

@Controller
@RequestMapping("/weixin")
public class WeixinController {

    @Inject
    private SystemSettingService systemSettingService;

    @GetMapping("/invitation/{invitationCode}")
    public Object invitation(@PathVariable String invitationCode, HttpServletRequest request) {
        request.getSession().setAttribute(Constants.INVITATION_CODE, invitationCode);
        WeixinSetting weixinSetting = systemSettingService.findActiveSetting(WeixinSetting.class);
        return String.format("redirect:https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=%s&state=%s#wechat_redirect", new String[]{weixinSetting.getGzhAppId(), weixinSetting.getGzhRedirectUrl(), "snsapi_userinfo", STATE_INVITATION});
    }

}
