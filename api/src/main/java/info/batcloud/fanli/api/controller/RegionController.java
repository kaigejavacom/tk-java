package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.service.RegionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/region")
public class RegionController {

    @Inject
    private RegionService regionService;

    @GetMapping("/list/{parentId}")
    public Object list(@PathVariable long parentId) {
        return BusinessResponse.ok(regionService.findByParentId(parentId));
    }

    @GetMapping("/level/{level}")
    public Object levelList(@PathVariable int level) {
        return BusinessResponse.ok(regionService.findByLevel(level));
    }

}
