package info.batcloud.fanli.api.vo;

public class CouponItemVo {

    private String picUrl;
    private Long id;
    private String title;
    private Float price;
    private Float couponAmountVal;
    private Float salePrice;

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getCouponAmountVal() {
        return couponAmountVal;
    }

    public void setCouponAmountVal(Float couponAmountVal) {
        this.couponAmountVal = couponAmountVal;
    }

    public Float getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Float salePrice) {
        this.salePrice = salePrice;
    }
}
