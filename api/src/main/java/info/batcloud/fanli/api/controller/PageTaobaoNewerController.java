package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.enums.Authorize;
import info.batcloud.fanli.api.security.annotation.UserLevelRequire;
import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.enums.UserLevel;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.TaobaoNewerService;
import info.batcloud.fanli.core.service.TbPullNewDetailService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

@RestController
@PreAuthorize("hasRole('USER')")
@RequestMapping("/page/taobao-newer")
public class PageTaobaoNewerController {

    @Inject
    private TaobaoNewerService taobaoNewerService;

    @Inject
    private TbPullNewDetailService tbPullNewDetailService;

    @GetMapping("/data")
    @UserLevelRequire(UserLevel.PLUS)
    public Object data() {
        TaobaoNewerService.ShareInfo shareInfo = taobaoNewerService.findUserShareInfo(SecurityHelper.loginUserId());
        Map<String, Object> map = new HashMap<>();
        map.put("shareInfo", shareInfo);
        Calendar cal = Calendar.getInstance();
        map.put("stat", tbPullNewDetailService.statUserByMonth(SecurityHelper.loginUserId(), cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1));
        return BusinessResponse.ok(map);
    }

}
