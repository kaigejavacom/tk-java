package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.TbPullNewDetailService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

@RestController
@PreAuthorize("hasRole('USER')")
@RequestMapping("/page/tb-pull-new-detail-list")
public class PageTbPullNewDetailListController {

    @Inject
    private TbPullNewDetailService tbPullNewDetailService;

    @GetMapping("/data")
    public Object data() {
        Map<String, Object> map = new HashMap<>();
        Calendar cal = Calendar.getInstance();
        map.put("stat", tbPullNewDetailService.statUserByMonth(SecurityHelper.loginUserId(), cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1));
        map.put("year", cal.get(Calendar.YEAR));
        map.put("month", cal.get(Calendar.MONTH) + 1);
        return BusinessResponse.ok(map);
    }

}
