package info.batcloud.fanli.api.exception;

import info.batcloud.fanli.core.enums.UserLevel;

public class UserLevelRequireException extends RuntimeException {
    private UserLevel userLevel;

    public UserLevelRequireException(UserLevel userLevel) {
        this.userLevel = userLevel;
    }

    public UserLevel getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(UserLevel userLevel) {
        this.userLevel = userLevel;
    }
}
