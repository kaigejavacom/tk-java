package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.helper.StringHelper;
import info.batcloud.fanli.core.service.SmsService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/sms")
public class SmsController {

    @Inject
    private SmsService smsService;

    @PostMapping("/register/{phone}")
    public Object sendRegister(@PathVariable String phone) {
        SmsService.Result<String> rs = smsService.sendRegisterMsg(phone);
        rs.setPhone(StringHelper.protect(rs.getPhone()));
        return BusinessResponse.ok(rs);
    }

    @PostMapping("/find-pwd/{phone}")
    public Object sendFindPwd(@PathVariable String phone) {
        SmsService.Result<String> rs = smsService.sendFindPwdMsg(phone);
        rs.setPhone(StringHelper.protect(rs.getPhone()));
        return BusinessResponse.ok(rs);
    }

    @PostMapping("/phone-login/{phone}")
    public Object sendLoginPhone(@PathVariable String phone) {
        SmsService.Result<String> rs = smsService.sendPhoneLoginMsg(phone);
        rs.setPhone(StringHelper.protect(rs.getPhone()));
        return BusinessResponse.ok(rs);
    }

    @PostMapping("/phone-bind/{phone}")
    public Object sendPhoneBind(@PathVariable String phone) {
        SmsService.Result<String> rs = smsService.sendPhoneBindMsg(phone);
        rs.setPhone(StringHelper.protect(rs.getPhone()));
        return BusinessResponse.ok(rs);
    }

    @PostMapping("/withdraw")
    @PreAuthorize("hasRole('USER')")
    public Object sendWithdraw() {
        SmsService.Result<String> rs = smsService.sendWithdrawMsg(SecurityHelper.loginUserId());
        rs.setPhone(StringHelper.protect(rs.getPhone()));
        return BusinessResponse.ok(rs);
    }

}
