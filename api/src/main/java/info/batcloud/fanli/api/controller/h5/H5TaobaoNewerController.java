package info.batcloud.fanli.api.controller.h5;

import info.batcloud.fanli.core.service.TaobaoNewerService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/h5/taobao-newer")
public class H5TaobaoNewerController {

    @Inject
    private TaobaoNewerService taobaoNewerService;

    @GetMapping("/{pid}")
    public String show(@PathVariable String pid, HttpServletRequest request, ModelMap modelMap) {
        String userAgent = request.getHeader("user-agent");
        if (userAgent.indexOf("MicroMessenger") == -1) {
            //说明可能是在浏览器中打开的。
            return "redirect:https://mos.m.taobao.com/activity_newer?from=pub&pid=" + pid;
        }
        TaobaoNewerService.ShareInfo shareInfo = taobaoNewerService.findShareInfoByPid(pid);
        modelMap.put("shareInfo", shareInfo);
        return "h5/tblx";
    }

}
