package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.dto.FlashSaleItemDTO;
import info.batcloud.fanli.core.enums.FlashSaleItemSort;
import info.batcloud.fanli.core.enums.FlashSaleItemStatus;
import info.batcloud.fanli.core.service.FlashSaleItemService;
import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.settings.FlashSaleSetting;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.text.ParseException;
import java.util.*;

@RestController
@RequestMapping("/page/flash-sale")
public class PageFlashSaleController {

    @Inject
    private SystemSettingService systemSettingService;

    @Inject
    private FlashSaleItemService flashSaleItemService;

    @GetMapping("/data")
    public Object data() {
        FlashSaleSetting saleSetting = systemSettingService.findActiveSetting(FlashSaleSetting.class);
        Map<String, Object> map = new HashMap<>();
        Date now = new Date();
        String date = DateFormatUtils.format(now, "yyyy-MM-dd");
        map.put("date", date);
        List<SeasonVo> list = new ArrayList<>();
        Collections.sort(saleSetting.getSeasonList(), Comparator.comparing(FlashSaleSetting.Season::getTime));
        SeasonVo activeSeason = null;
        for (int i = 0; i < saleSetting.getSeasonList().size(); i++) {
            FlashSaleSetting.Season season = saleSetting.getSeasonList().get(i);
            SeasonVo sv = new SeasonVo();
            sv.setTime(season.getTime());
            sv.setTitle(season.getTitle());
            String startTimeStr = date + " " + sv.getTime();
            sv.setDate(date);
            try {
                Date startTime = DateUtils.parseDate(startTimeStr.replaceAll(" +", " "), "yyyy-MM-dd HH:mm");
                sv.setRemainSeconds((startTime.getTime() - now.getTime()) / 1000);
            } catch (ParseException e) {
                e.printStackTrace();
                continue;
            }
            SeasonVo lastSv;
            if (i == 0) {
                lastSv = sv;
            } else {
                lastSv = list.get(list.size() - 1);
            }
            if (sv.getRemainSeconds() > 0 && lastSv.getRemainSeconds() <= 0 && activeSeason == null) {
                lastSv.setActive(true);
                activeSeason = lastSv;
            }
            list.add(sv);
        }
        if(activeSeason == null && list.size() > 0) {
            list.get(0).setActive(true);
        }
        map.put("seasonList", list);
        map.put("flashSaleSetting", saleSetting);
        FlashSaleItemService.SearchParam param = new FlashSaleItemService.SearchParam();
        param.setFinished(true);
        param.setStatus(FlashSaleItemStatus.ONSALE);
        param.setSort(FlashSaleItemSort.ID_DESC);
        param.setPageSize(10);
        List<FlashSaleItemDTO> finishedList = flashSaleItemService.search(param).getResults();
        map.put("finishedList", finishedList);
        return BusinessResponse.ok(map);
    }

    public static class SeasonVo {

        private String title;
        private String date;
        private String time;
        private boolean active;
        private long remainSeconds;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public long getRemainSeconds() {
            return remainSeconds;
        }

        public void setRemainSeconds(long remainSeconds) {
            this.remainSeconds = remainSeconds;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public boolean isActive() {
            return active;
        }

        public void setActive(boolean active) {
            this.active = active;
        }
    }

}
