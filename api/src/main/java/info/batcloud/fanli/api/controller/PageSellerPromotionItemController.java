package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.dto.SellerPromotionItemDTO;
import info.batcloud.fanli.core.dto.SellerPromotionItemOrderDTO;
import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.enums.FootmarkType;
import info.batcloud.fanli.core.enums.SellerPromotionItemOrderStatus;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.*;
import info.batcloud.fanli.core.settings.UrlSchemeSetting;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/page/seller-promotion-item")
public class PageSellerPromotionItemController {

    @Inject
    private SellerPromotionItemService sellerPromotionItemService;

    @Inject
    private SellerPromotionItemOrderService sellerPromotionItemOrderService;

    @Inject
    private UrlMappingService urlMappingService;

    @Inject
    private SystemSettingService systemSettingService;

    @Inject
    private FootmarkService footmarkService;

    @GetMapping("/data")
    public Object data(@RequestParam long id) {
        Long userId = SecurityHelper.loginUserId();
        Map<String, Object> map = new HashMap<>();
        map.put("item", sellerPromotionItemService.findById(id));
        map.put("login", false);
        if (userId != null) {
            map.put("itemLocked", sellerPromotionItemOrderService.ifLockedByUserIdAndSellerPromotionItemId(userId, id));
            SellerPromotionItemOrderDTO order = sellerPromotionItemOrderService.findByUserIdAndPromotionItemId(userId, id);
            map.put("fillTradeNo", order != null && order.getStatus() == SellerPromotionItemOrderStatus.WAIT_FILL_TRADE_NO
                    && StringUtils.isBlank(order.getOutTradeNo()));
            footmarkService.addFootmark(SecurityHelper.loginUserId(), id, FootmarkType.HISTORY);
            map.put("favor", footmarkService.isFavor(userId, id));
        }
        return BusinessResponse.ok(map);
    }

    @GetMapping("/search-buy/{id}")
    public Object searchBuy(@PathVariable long id) {
        Map<String, Object> map = new HashMap<>();
        SellerPromotionItemOrderDTO order = sellerPromotionItemOrderService.findByUserIdAndPromotionItemId(SecurityHelper.loginUserId(), id);
        SellerPromotionItemDTO item = sellerPromotionItemService.findById(id);
        map.put("orderId", order.getId());
        map.put("waitFillTradeNo", order != null
                && StringUtils.isBlank(order.getOutTradeNo())
                && order.getStatus() == SellerPromotionItemOrderStatus.WAIT_FILL_TRADE_NO);
        map.put("searchBuyDescUrl", urlMappingService.getSellerPromotionItemSearchBuyDescUrl(id));
        map.put("ecomPlat", order.getEcomPlat());
        map.put("ecomPlatTitle", order.getEcomPlatTitle());
        UrlSchemeSetting urlSchemeSetting = systemSettingService.findActiveSetting(UrlSchemeSetting.class);
        map.put("urlScheme", urlSchemeSetting.findUrl(order.getEcomPlat(), UrlSchemeSetting.Page.SEARCH, item.getKeywords()));
        return BusinessResponse.ok(map);
    }
}
