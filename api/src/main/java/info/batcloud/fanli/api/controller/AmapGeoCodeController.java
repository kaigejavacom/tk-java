package info.batcloud.fanli.api.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import info.batcloud.fanli.core.domain.BusinessResponse;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URL;

@RestController
@RequestMapping("/amap/geo-code")
public class AmapGeoCodeController {

    @Value("${amap.webApiKey}")
    private String webApiKey;

    @PostMapping("/regeo")
    public Object regeo(RegeoForm form) throws IOException {
        String url = "https://restapi.amap.com/v3/geocode/regeo?key=" + webApiKey + "&location=" + form.getLng() + "," + form.getLat();
        String resText = IOUtils.toString(new URL(url), "utf8");
        JSONObject jsonObject = JSON.parseObject(resText);
        return BusinessResponse.ok(jsonObject);
    }

    public static class RegeoForm {
        private String lat;
        private String lng;

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }
    }

}
