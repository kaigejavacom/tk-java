package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.entity.User;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.helper.StringHelper;
import info.batcloud.fanli.core.repository.UserRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@RestController
@PreAuthorize("hasRole('USER')")
@RequestMapping("/page/settings")
public class PageSettingsController {

    @Inject
    private UserRepository userRepository;

    @GetMapping("/data")
    public Object data() {
        User user = userRepository.findOne(SecurityHelper.loginUserId());
        Map<String, Object> map = new HashMap<>();

        map.put("phone", StringHelper.protect(user.getPhone()));
        map.put("nickname", user.getNickname());
        map.put("weixinBind", StringUtils.isNotBlank(user.getWeixinOpenId()));
        map.put("city", user.getCity());
        map.put("district", user.getDistrict());
        map.put("taobaoSpecialId", user.getTaobaoSpecialId());
        map.put("gender", user.getGender());
        return BusinessResponse.ok(map);
    }

}
