package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.domain.Result;
import info.batcloud.fanli.core.exception.BizException;
import info.batcloud.fanli.core.service.UserService;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/find-pwd")
public class FindPwdController {

    @Inject
    private UserService userService;

    @PostMapping
    public Object findPwd(UserService.UserFindPwdParam param, BindingResult result) {
        if (result.hasErrors()) {
            throw new BizException("参数错误，请检查!");
        }
        Result rs = userService.findPassword(param);
        return BusinessResponse.ok(rs);
    }


}
