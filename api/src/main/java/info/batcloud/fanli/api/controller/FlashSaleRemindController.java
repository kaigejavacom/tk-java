package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.enums.FlashSaleItemSort;
import info.batcloud.fanli.core.enums.FlashSaleItemStatus;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.FlashSaleItemService;
import info.batcloud.fanli.core.service.FlashSaleRemindService;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/flash-sale-remind")
@PreAuthorize("hasRole('USER')")
public class FlashSaleRemindController {

    @Inject
    private FlashSaleRemindService flashSaleRemindService;

    @PutMapping("/{flashSaleItemId}")
    public Object join( @PathVariable long flashSaleItemId) {
        flashSaleRemindService.joinRemind(flashSaleItemId, SecurityHelper.loginUserId());
        return BusinessResponse.ok(true);
    }

    @DeleteMapping("/{flashSaleItemId}")
    public Object exit( @PathVariable long flashSaleItemId) {
        flashSaleRemindService.exitRemind(flashSaleItemId, SecurityHelper.loginUserId());
        return BusinessResponse.ok(true);
    }
}
