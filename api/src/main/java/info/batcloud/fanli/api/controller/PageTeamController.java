package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.TeamService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/page/team")
@PreAuthorize("hasRole('USER')")
public class PageTeamController {

    @Inject
    private TeamService teamService;

    @GetMapping("/data")
    public Object data() {
        Map<String, Object> map = new HashMap<>();
        map.put("memberNumStat", teamService.statUserMemberNum(SecurityHelper.loginUserId()));
        return BusinessResponse.ok(map);
    }

}
