package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.dto.AgentDTO;
import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.AgentService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/page/my-agent")
@RestController
public class PageMyAgentController {

    @Inject
    private AgentService agentService;

    @GetMapping("/data")
    @PreAuthorize("hasRole('USER')")
    public Object data() {
        List<AgentDTO> agentList = agentService.findAgentByUserId(SecurityHelper.loginUserId());
        Map<String, Object> map = new HashMap<>();
        map.put("agentList", agentList);
        return BusinessResponse.ok(map);
    }

}
