package info.batcloud.fanli.api.exception;

import info.batcloud.fanli.core.constants.MessageKeyConstants;
import info.batcloud.fanli.core.context.StaticContext;
import org.springframework.security.core.AuthenticationException;

public class WeixinNeitherRegisterNorBindException extends AuthenticationException {
    public WeixinNeitherRegisterNorBindException() {
        super(StaticContext.messageSource.getMessage(MessageKeyConstants.WEIXIN_NEITHER_REGISTER_NOR_BIND, null, null));
    }
}
