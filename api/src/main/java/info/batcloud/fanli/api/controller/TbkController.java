package info.batcloud.fanli.api.controller;

import com.taobao.api.ApiException;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.TbkActivitylinkGetRequest;
import com.taobao.api.request.TbkTpwdCreateRequest;
import com.taobao.api.response.TbkActivitylinkGetResponse;
import com.taobao.api.response.TbkTpwdCreateResponse;
import info.batcloud.fanli.core.constants.TaobaoClientConstants;
import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.dto.CommissionItemDTO;
import info.batcloud.fanli.core.exception.BizException;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.CommissionItemService;
import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.service.UserService;
import info.batcloud.fanli.core.settings.TaobaoUnionSetting;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("tbk")
public class TbkController {

    @Inject
    @Qualifier(TaobaoClientConstants.TAOBAO_CLIENT_ITEM_TBK)
    private TaobaoClient taobaoClient;

    @Inject
    private CommissionItemService commissionItemService;

    @Inject
    private SystemSettingService systemSettingService;

    @Inject
    private UserService userService;

    //淘口令
    @GetMapping("/tpwd/{itemId}")
    public Object tpwd(@PathVariable long itemId) throws ApiException {
        CommissionItemDTO ci = commissionItemService.findById(itemId);
        TbkTpwdCreateRequest req = new TbkTpwdCreateRequest();
        req.setText(ci.getTitle() + "\n" + ci.getDescription());
        req.setUrl(ci.getCouponClickUrl() + "&pid=mm_44570695_15078581_58442263");
        req.setLogo(ci.getPicUrl());
        TbkTpwdCreateResponse rsp = taobaoClient.execute(req);
        return BusinessResponse.ok(rsp.getData());
    }

    @PostMapping("/activitylink")
    public Object activityLink(@RequestParam long promotionSceneId, @RequestParam(required = false) String klText, @RequestParam(required = false) String logo) throws ApiException {
        TaobaoUnionSetting taobaoUnionSetting = systemSettingService.findActiveSetting(TaobaoUnionSetting.class);
        TbkActivitylinkGetRequest req = new TbkActivitylinkGetRequest();
        req.setPlatform(2L);
        req.setAdzoneId(Long.valueOf(taobaoUnionSetting.getAdzoneId()));
        req.setPromotionSceneId(promotionSceneId);
        String relationId = userService.findTaobaoRelationId(SecurityHelper.loginUserId());
        req.setRelationId(relationId);
        TbkActivitylinkGetResponse rsp = taobaoClient.execute(req);
        String activityUrl = rsp.getData();
        if (StringUtils.isEmpty(activityUrl)) {
            throw new BizException("该活动已经结束");
        }
        TbkTpwdCreateRequest tpwdReq = new TbkTpwdCreateRequest();
        tpwdReq.setText(StringUtils.defaultIfEmpty(klText, "辣小客口令").replace("{klText}", ""));
        tpwdReq.setUrl(activityUrl);
        tpwdReq.setLogo(logo);
        TbkTpwdCreateResponse tpwdRes = taobaoClient.execute(tpwdReq);
        Map<String, String> map = new HashMap<>(2);
        map.put("activityUrl", activityUrl);
        map.put("kl", tpwdRes.getData().getModel());
        return BusinessResponse.ok(map);
    }

}
