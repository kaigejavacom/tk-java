package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.service.IndexPageService;
import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.settings.IndexItemShowSetting;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/page/index")
public class IndexController {

    @Inject
    private IndexPageService indexPageService;

    @Inject
    private SystemSettingService systemSettingService;

    @GetMapping("/data")
    public Object data() {
        Map<String, Object> model = new HashMap<>();

        model.put("banner", indexPageService.banner());
        model.put("quickNavList", indexPageService.quickNavList());
        model.put("tabbarNavList", indexPageService.tabbarNavList());
        model.put("itemShowSetting", systemSettingService.findActiveSetting(IndexItemShowSetting.class));
        model.put("floorList", indexPageService.floorList());
        model.put("quickNavAdvList", indexPageService.quickNavAdvList());
        return BusinessResponse.ok(model);
    }

}
