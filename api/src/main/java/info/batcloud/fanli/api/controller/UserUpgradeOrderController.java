package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.enums.TimeUnit;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.UserUpgradeOrderService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user-upgrade-order")
public class UserUpgradeOrderController {

    @Inject
    private UserUpgradeOrderService userUpgradeOrderService;

    @PostMapping("/{timeUnit}")
    @PreAuthorize("hasRole('USER')")
    public Object order(@PathVariable TimeUnit timeUnit) {
        Map<String, Object> map = new HashMap<>();
        map.put("orderId", userUpgradeOrderService.checkAndCreateUserUpgradeOrder(SecurityHelper.loginUserId(), timeUnit));
        return BusinessResponse.ok(map);
    }

}
