package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.dto.BundleJsPackageDTO;
import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.enums.AppPlatform;
import info.batcloud.fanli.core.enums.UserLogType;
import info.batcloud.fanli.core.helper.HttpServletRequestHelper;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.*;
import info.batcloud.fanli.core.settings.BootPageSetting;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/boot")
public class BootController {

    @Inject
    private IndexPageService indexPageService;

    @Inject
    private BundleJsPackageService bundleJsPackageService;

    @Inject
    private SystemSettingService systemSettingService;

    @Inject
    private UserService userService;

    @Inject
    private UrlMappingService urlMappingService;

    @GetMapping
    public Object execute(BootParams params, HttpServletRequest request) {
        Long userId = SecurityHelper.loginUserId();
        Map<String, Object> map = new HashMap<>();
        map.put("userId", userId);
        map.put("login", userId != null);
        if(userId != null) {
            UserService.LoginTraceInfo loginTraceInfo = new UserService.LoginTraceInfo();
            loginTraceInfo.setIp(HttpServletRequestHelper.getRequestIp(request));
            loginTraceInfo.setType(UserLogType.ACTIVE);
            userService.trace(SecurityHelper.loginUserId(), loginTraceInfo);
        }
        AppPlatform platform = AppPlatform
                .valueOf(params.getPlatform().toUpperCase());
        if(!"WEB".equals(params.getPlatform().toUpperCase())) {
            BundleJsPackageDTO bundleJsPackage = bundleJsPackageService.findLatestByPlatformAndAppVersion(platform, params.getVersion());
            map.put("bundleJsPackage", bundleJsPackage);
            map.put("iosShow", bundleJsPackage.isIosShow());
        }
        BootPageSetting bootPageSetting = systemSettingService.findActiveSetting(BootPageSetting.class);
        map.put("bootPageTimeout", bootPageSetting.getTimeout());
        map.put("bootPagePicList", bootPageSetting.getPicList());
        map.put("bootPageEnterBtnText", bootPageSetting.getEnterBtnText());
        map.put("latestAppVersion", bundleJsPackageService.findLatestByPlatform(platform).getPlatformVersion());
        map.put("tabbarNavList", indexPageService.tabbarNavList());
        map.put("upgradeUrl", urlMappingService.getHomeWebsiteUrl());
        return BusinessResponse.ok(map);
    }

    @GetMapping("/app/config/{platform}")
    public Object appConfig(@PathVariable AppPlatform platform) {
        Map<String, Object> map = new HashMap<>();

        return BusinessResponse.ok(map);
    }

    public static class BootParams {
        private String platform;
        private String version;

        public String getPlatform() {
            return platform;
        }

        public void setPlatform(String platform) {
            this.platform = platform;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }
    }

}
