package info.batcloud.fanli.api.controller.h5;

import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.settings.Taobao1111Setting;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/h5/1111")
public class H520181111Controller {

    @Inject
    private SystemSettingService systemSettingService;

    @GetMapping
    public String execute(ModelMap modelMap, HttpServletRequest request) {
        Taobao1111Setting setting = systemSettingService.findActiveSetting(Taobao1111Setting.class);
        String userAgent = request.getHeader("user-agent");
        if(userAgent.indexOf("MicroMessenger") == -1
                && StringUtils.isNotBlank(setting.getSuperRedPacketClickUrl())) {
            //说明可能是在浏览器中打开的。
            return "redirect:" + setting.getSuperRedPacketClickUrl();
        }
        modelMap.put("taobao1111Setting", setting);
        return "h5/20181111";
    }


}
