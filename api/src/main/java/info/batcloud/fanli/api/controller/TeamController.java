package info.batcloud.fanli.api.controller;

import com.ctospace.archit.common.pagination.Paging;
import info.batcloud.fanli.core.dto.Member;
import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.TeamService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/team")
@PreAuthorize("hasRole('USER')")
public class TeamController {

    @Inject
    private TeamService teamService;

    @GetMapping("/member-list")
    public Object memberList(TeamService.MemberFindParam param) {
        param.setUserId(SecurityHelper.loginUserId());
        Paging<Member> memberPaging = teamService.findUserMemberShip(param);
        return BusinessResponse.ok(memberPaging);
    }

}
