package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.api.constants.Constants;
import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.exception.BizException;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.tomcat.util.bcel.Const;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/register")
public class RegisterController {

    @Inject
    private UserService userService;

    @PostMapping
    public Object register(RegisterForm form, BindingResult result, HttpServletRequest request) {
        if (result.hasErrors()) {
            throw new BizException("注册参数错误，请检查!");
        }
        UserService.UserRegisterParam param = new UserService.UserRegisterParam();
        param.setInvitationCode(form.getInviteCode());
        param.setPassword(form.getPassword());
        param.setPhone(form.getPhone());
        param.setVerifyCode(form.getVerifyCode());
        if(StringUtils.isNotBlank(form.getWeixinBindOpenId())) {
            if(!form.getWeixinBindOpenId().equals(request.getSession()
                    .getAttribute(Constants.WEIXIN_BIND_OPEN_ID))) {
                throw new BizException("绑定用户失败");
            }
        }
        param.setWeixinBindOpenId(form.getWeixinBindOpenId());
        UserService.RegisterResult registerResult = userService.registerUser(param);
        if(registerResult.isSuccess()) {
            request.removeAttribute(Constants.WEIXIN_BIND_OPEN_ID);
        }
        return BusinessResponse.ok(registerResult);
    }

    @GetMapping("/check/step1")
    public Object checkStep1(Step1Form form, BindingResult result) {
        if (result.hasErrors()) {
            throw new BizException("注册参数错误，请检查!");
        }
        Map<String, Object> map = new HashMap<>();
        map.put("invitationCodeValid", userService.isInvitationCodeExists(form.getInviteCode()) || userService.findByPhone(form.getInviteCode()) != null);
        map.put("phoneValid", userService.findByPhone(form.getPhone()) == null);
        return BusinessResponse.ok(map);
    }

    public static class Step1Form {
        @NotNull
        private String phone;
        @NotNull
        private String inviteCode;

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getInviteCode() {
            return inviteCode;
        }

        public void setInviteCode(String inviteCode) {
            this.inviteCode = inviteCode;
        }
    }


    public static class RegisterForm {
        @NotNull
        private String phone;
        @NotNull
        private String verifyCode;
        @NotNull
        private String inviteCode;
        @NotNull
        private String password;

        private String weixinBindOpenId;

        public String getWeixinBindOpenId() {
            return weixinBindOpenId;
        }

        public void setWeixinBindOpenId(String weixinBindOpenId) {
            this.weixinBindOpenId = weixinBindOpenId;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getVerifyCode() {
            return verifyCode;
        }

        public void setVerifyCode(String verifyCode) {
            this.verifyCode = verifyCode;
        }

        public String getInviteCode() {
            return inviteCode;
        }

        public void setInviteCode(String inviteCode) {
            this.inviteCode = inviteCode;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

}
