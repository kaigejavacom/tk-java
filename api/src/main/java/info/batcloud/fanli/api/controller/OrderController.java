package info.batcloud.fanli.api.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.service.OrderService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@RequestMapping("/order")
@RestController
public class OrderController {

    @Inject
    private OrderService orderService;

    @PostMapping("/app-alipay/{id}")
    public Object appAlipay(@PathVariable long id) {
        Map<String, Object> map = new HashMap<>();
        BusinessResponse res = new BusinessResponse();
        try {
            AlipayTradeAppPayResponse alipayResponse = orderService.appAlipay(id);
            map.put("orderInfo", alipayResponse.getBody());
            res.setData(map);
            res.setSuccess(true);
        } catch (AlipayApiException e) {
            res.setSuccess(false);
            res.setErrMsg("支付宝交易创建失败!");
        }
        return res;
    }

}
