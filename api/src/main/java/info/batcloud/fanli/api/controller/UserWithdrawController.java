package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.constants.MessageKeyConstants;
import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.exception.BizException;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.service.UserWithdrawService;
import info.batcloud.fanli.core.settings.WithdrawSetting;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/user-withdraw")
@PreAuthorize("hasRole('USER')")
public class UserWithdrawController {

    @Inject
    private UserWithdrawService userWithdrawService;

    @Inject
    private SystemSettingService systemSettingService;

    @PostMapping
    public Object withdraw(UserWithdrawService.UserWithDrawParams params) {
        WithdrawSetting setting = systemSettingService.findActiveSetting(WithdrawSetting.class);
        if(!setting.isOpenWithdraw()) {
            throw new BizException(MessageKeyConstants.WITHDRAW_IS_CLOSED);
        }
        userWithdrawService.withdraw(SecurityHelper.loginUserId(), params);
        return BusinessResponse.ok(true);
    }

}
