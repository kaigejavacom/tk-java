package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.admin.permission.ManagerPermissions;
import info.batcloud.fanli.admin.permission.annotation.Permission;
import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.operation.center.domain.Page;
import info.batcloud.fanli.core.service.MobilePushService;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/mobile-push")
public class MobilePushController {

    @Inject
    private MobilePushService mobilePushService;

    @PostMapping("/page")
    @Permission(ManagerPermissions.MOBILE_PUSH)
    public Object pushPage(PagePushForm form) {
        if(form.isAndroid()) {
            this.push(form, MobilePushService.Platform.ANDROID);
        } else
        if(form.isIos()) {
            this.push(form, MobilePushService.Platform.IOS);
        }
        return BusinessResponse.ok(true);
    }

    private void push(PagePushForm form, MobilePushService.Platform platform) {
        if (StringUtils.isBlank(form.getAccount())) {
            mobilePushService.pushPage(platform, form.getTitle(), form.getContent(), form.getPage(), form.getParams());
        } else {
            mobilePushService.pushToAccount(form.getAccount(), platform, form.getTitle(), form.getContent(), form.getPage(), form.getParams());
        }
    }

    public static class PagePushForm {
        private String title;
        @NotNull
        private String content;
        @NotNull
        private Page page;
        private String params;
        private boolean android;
        private boolean ios;
        private String account;

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public boolean isAndroid() {
            return android;
        }

        public void setAndroid(boolean android) {
            this.android = android;
        }

        public boolean isIos() {
            return ios;
        }

        public void setIos(boolean ios) {
            this.ios = ios;
        }

        public void setPage(Page page) {
            this.page = page;
        }

        public String getParams() {
            return params;
        }

        public void setParams(String params) {
            this.params = params;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public Page getPage() {
            return page;
        }
    }

}
