package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.admin.permission.ManagerPermissions;
import info.batcloud.fanli.admin.permission.annotation.Permission;
import info.batcloud.fanli.core.service.ArticleService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/article")
public class ArticleController {

    @Inject
    private ArticleService articleService;

    @GetMapping("/search")
    public Object search(ArticleService.SearchParam param) {
        return articleService.search(param);
    }

    @Permission(ManagerPermissions.ARTICLE_MANAGE)
    @PostMapping
    public Object add(ArticleService.ArticleCreateParam param) {
        return articleService.createArticle(param);
    }

    @Permission(ManagerPermissions.ARTICLE_MANAGE)
    @DeleteMapping("/{id}")
    public Object delete(@PathVariable long id) {
        articleService.deleteById(id);
        return 1;
    }

    @Permission(ManagerPermissions.ARTICLE_MANAGE)
    @PutMapping("/{id}")
    public Object update(@PathVariable long id, ArticleService.ArticleUpdateParam param) {
        articleService.updateArticle(id, param);
        return 1;
    }

}
