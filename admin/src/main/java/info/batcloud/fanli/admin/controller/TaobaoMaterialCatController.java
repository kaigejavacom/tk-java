package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.admin.permission.ManagerPermissions;
import info.batcloud.fanli.admin.permission.annotation.Permission;
import info.batcloud.fanli.core.domain.TaobaoMaterial;
import info.batcloud.fanli.core.enums.TaobaoMaterialCatStatus;
import info.batcloud.fanli.core.service.TaobaoMaterialCatService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/taobao-material-cat")
public class TaobaoMaterialCatController {

    @Inject
    private TaobaoMaterialCatService taobaoMaterialCatService;

    @GetMapping("/list")
    public Object list() {
        return taobaoMaterialCatService.findAll();
    }

    @GetMapping("/material/list/{id}")
    public Object materialList(@PathVariable long id) {
        if (id == 0) {
            return Collections.EMPTY_LIST;
        }
        return taobaoMaterialCatService.getMaterialList(id);
    }

    @Permission(ManagerPermissions.CHANNEL_MANAGE)
    @PostMapping
    public Object add(TaobaoMaterialCatAddForm form) {
        return taobaoMaterialCatService.addTaobaoMaterialCat(form.getName(), TaobaoMaterialCatStatus.VALID);
    }

    @Permission(ManagerPermissions.CHANNEL_MANAGE)
    @PutMapping("/{id}")
    public Object update(@PathVariable long id, @RequestParam String name) {
        taobaoMaterialCatService.updateTaobaoMaterialCat(id, name);
        return 1;
    }

    @Permission(ManagerPermissions.CHANNEL_MANAGE)
    @DeleteMapping("/{id}")
    public Object delete(@PathVariable long id) {
        taobaoMaterialCatService.deleteById(id);
        return 1;
    }

    @PostMapping("/material/{id}")
    public Object materialList(@PathVariable long id, MaterialListForm form) {
        List<TaobaoMaterial> list = new ArrayList<>();
        for (int i = 0; i < form.getNameList().size(); i++) {
            TaobaoMaterial material = new TaobaoMaterial();
            material.setName(form.getNameList().get(i));
            material.setMaterialId(form.getMaterialIdList().get(i));
            list.add(material);
        }
        this.taobaoMaterialCatService.setMaterialList(id, list);
        return true;
    }

    public static class MaterialListForm {
        private List<String> nameList;

        private List<String> materialIdList;

        public List<String> getNameList() {
            return nameList;
        }

        public void setNameList(List<String> nameList) {
            this.nameList = nameList;
        }

        public List<String> getMaterialIdList() {
            return materialIdList;
        }

        public void setMaterialIdList(List<String> materialIdList) {
            this.materialIdList = materialIdList;
        }
    }

    public static class TaobaoMaterialCatAddForm {
        private String name;
        private TaobaoMaterialCatStatus status;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public TaobaoMaterialCatStatus getStatus() {
            return status;
        }

        public void setStatus(TaobaoMaterialCatStatus status) {
            this.status = status;
        }
    }
}
