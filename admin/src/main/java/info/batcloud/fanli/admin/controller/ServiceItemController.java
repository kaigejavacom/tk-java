package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.admin.permission.ManagerPermissions;
import info.batcloud.fanli.admin.permission.annotation.Permission;
import info.batcloud.fanli.core.enums.ServiceItemStatus;
import info.batcloud.fanli.core.service.ServiceItemService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/serviceItem")
@Permission(ManagerPermissions.SELLER_SERVICE_MANAGE)
public class ServiceItemController {

    @Inject
    private ServiceItemService serviceItemService;

    @GetMapping("/search")
    public Object search(ServiceItemService.SearchParam param) {
        return serviceItemService.search(param);
    }

    @PostMapping
    public Object save(ServiceItemService.ServiceItemSaveParam param) {

        serviceItemService.saveServiceItem(param);
        return 1;
    }

    @PutMapping("/{id}")
    public Object update(@PathVariable long id, ServiceItemService.ServiceItemUpdateParam param) {

        serviceItemService.updateServiceItem(id, param);
        return 1;
    }

    @PutMapping("/status/{id}-{status}")
    public Object setStatus(@PathVariable long id, @PathVariable ServiceItemStatus status) {

        serviceItemService.setStatus(id, status);
        return 1;
    }

}
