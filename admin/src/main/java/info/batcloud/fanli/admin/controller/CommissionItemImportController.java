package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.core.service.CommissionItemImportService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import java.io.IOException;

@RestController
@RequestMapping("/api/commission-item-import")
public class CommissionItemImportController {

    @Inject
    private CommissionItemImportService commissionItemImportService;

    @PostMapping("/import")
    public Object importFile(@RequestParam MultipartFile file,
                             CommissionItemImportService.ImportConfig config) throws IOException {
        new Thread(() -> {
            try {
                commissionItemImportService.importExcel(file.getInputStream(), config);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
        return 1000;
    }

}
