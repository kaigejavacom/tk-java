package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.core.client.BackgroundTaskClient;
import info.batcloud.fanli.core.dto.CollectionSourceAddDTO;
import info.batcloud.fanli.core.dto.CollectionSourceUpdateDTO;
import info.batcloud.fanli.core.service.CollectionSourceService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/collectionSource")
public class CollectionSourceController {

    @Inject
    private CollectionSourceService collectionSourceService;

    @Inject
    private BackgroundTaskClient backgroundTaskClient;

    @GetMapping("/all")
    public Object find() {
        return collectionSourceService.findAll();
    }

    @PostMapping
    public Object add(CollectionSourceAddDTO collectionSourceAddDTO) {
        collectionSourceService.saveCollectionSource(collectionSourceAddDTO);
        return 1;
    }
    @PutMapping("/{id}")
    public Object edit(@PathVariable long id, CollectionSourceUpdateDTO cs) {
        collectionSourceService.updateCollectionSource(id, cs);
        return 1;
    }

    @GetMapping("/collect/{id}")
    public Object collection(@PathVariable long id) {
        backgroundTaskClient.collectItemBySourceId(id);
        return 1;
    }
}
