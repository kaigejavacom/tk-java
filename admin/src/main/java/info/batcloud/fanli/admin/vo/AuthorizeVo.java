package info.batcloud.fanli.admin.vo;

import info.batcloud.fanli.core.enums.Authorize;

public class AuthorizeVo {

    private Authorize authorize;

    public String getAuthorizeTitle() {
        return getAuthorize() == null ? null : getAuthorize().getTitle();
    }

    public Authorize getAuthorize() {
        return authorize;
    }

    public void setAuthorize(Authorize authorize) {
        this.authorize = authorize;
    }
}
