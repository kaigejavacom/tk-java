package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.core.domain.utam.req.TbkOrderFetchRequest;
import info.batcloud.fanli.core.service.TaobaoUnionService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import java.io.IOException;

@RestController
@RequestMapping("/api/taobao-union")
public class TaobaoUnionController {

    @Inject
    private TaobaoUnionService taobaoUnionService;

    @GetMapping("/fetch-order")
    public Object fetchOrder(TbkOrderFetchRequest request) {
        return taobaoUnionService.fetchOrder(request);
    }

    @PostMapping("/import")
    public Object importOrder(@RequestParam MultipartFile file) throws IOException {
        return taobaoUnionService.importOrder(file.getInputStream());
    }

    @PostMapping("/import-recommend-coupon")
    public Object importRecommendCoupon(@RequestParam MultipartFile file) {
        new Thread(() -> {
            try {
                taobaoUnionService.importRecommendCoupon(file.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
        return true;
    }

}
