package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.admin.permission.ManagerPermissions;
import info.batcloud.fanli.admin.permission.annotation.Permission;
import info.batcloud.fanli.core.service.JdUnionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/jd-union")
public class JdUnionController {

    @Inject
    private JdUnionService jdUnionService;

    @GetMapping("/fetch-order")
    @Permission(ManagerPermissions.COMMISSION_ORDER_MANAGE)
    public Object fetchOrder(OrderFetchForm form) {
        jdUnionService.fetchOrder(form.getTime());
        return 1;
    }

    public static class OrderFetchForm {
        private String time;

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }
    }

}
