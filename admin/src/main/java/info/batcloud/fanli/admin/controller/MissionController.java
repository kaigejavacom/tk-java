package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.core.entity.Mission;
import info.batcloud.fanli.core.enums.MissionStatus;
import info.batcloud.fanli.core.enums.MissionType;
import info.batcloud.fanli.core.repository.MissionRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.Date;

@RestController
@RequestMapping("/api/mission")
public class MissionController {

    @Inject
    private MissionRepository missionRepository;

    @GetMapping("/list")
    public Object list() {
        return missionRepository.findByStatusNotIn(MissionStatus.DELETED, MissionStatus.SNAPSHOT);
    }

    @PutMapping("/{id}")
    @Transactional
    public Object edit(@PathVariable long id, MissionEditForm form) {

        Mission mission = missionRepository.findOne(id);
        Mission newMission = new Mission();
        BeanUtils.copyProperties(mission, newMission);
        mission.setStatus(MissionStatus.SNAPSHOT);
        newMission.setTotalTimes(form.getTotalTimes());
        newMission.setIntegral(form.getIntegral());
        newMission.setStatus(form.getStatus());
        missionRepository.save(mission);
        missionRepository.save(newMission);
        return 1;
    }

    @DeleteMapping("/{id}")
    public Object delete(@PathVariable long id) {
        Mission mission = missionRepository.findOne(id);
        mission.setStatus(MissionStatus.DELETED);
        missionRepository.save(mission);
        return 1;
    }

    @PostMapping
    public Object add(MissionAddForm form) {

        Mission mission = new Mission();
        mission.setTotalTimes(form.getTotalTimes());
        mission.setIntegral(form.getIntegral());
        mission.setStatus(form.getStatus());
        mission.setCreateTime(new Date());
        mission.setType(form.getType());
        missionRepository.save(mission);
        return 1;
    }

    public static class MissionAddForm {

        private MissionType type;
        private int integral;
        private int totalTimes;
        private MissionStatus status;

        public MissionType getType() {
            return type;
        }

        public void setType(MissionType type) {
            this.type = type;
        }

        public int getIntegral() {
            return integral;
        }

        public void setIntegral(int integral) {
            this.integral = integral;
        }

        public int getTotalTimes() {
            return totalTimes;
        }

        public void setTotalTimes(int totalTimes) {
            this.totalTimes = totalTimes;
        }

        public MissionStatus getStatus() {
            return status;
        }

        public void setStatus(MissionStatus status) {
            this.status = status;
        }
    }

    public static class MissionEditForm {
        private int integral;
        private int totalTimes;
        private MissionStatus status;

        public int getIntegral() {
            return integral;
        }

        public void setIntegral(int integral) {
            this.integral = integral;
        }

        public int getTotalTimes() {
            return totalTimes;
        }

        public void setTotalTimes(int totalTimes) {
            this.totalTimes = totalTimes;
        }

        public MissionStatus getStatus() {
            return status;
        }

        public void setStatus(MissionStatus status) {
            this.status = status;
        }
    }

}
