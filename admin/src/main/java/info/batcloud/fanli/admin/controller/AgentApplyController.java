package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.core.enums.AgentApplyStatus;
import info.batcloud.fanli.core.service.AgentApplyService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/agent-apply")
public class AgentApplyController {

    @Inject
    private AgentApplyService agentApplyService;

    @GetMapping("/search")
    public Object search(AgentApplyService.SearchParam param) {

        return agentApplyService.search(param);
    }

    @PutMapping("/verify/{id}-{status}")
    public Object verify(@PathVariable long id, @PathVariable AgentApplyStatus status) {
        agentApplyService.verify(id, status);
        return 1;
    }

}
