package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.admin.permission.ManagerPermissions;
import info.batcloud.fanli.admin.permission.annotation.Permission;
import info.batcloud.fanli.core.service.DistrictAgentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/districtAgent")
public class DistrictAgentController {

    @Inject
    private DistrictAgentService agentService;

    @GetMapping("/search")
    public Object search(DistrictAgentService.SearchParam param) {

        return agentService.search(param);
    }

    @PostMapping()
    @Permission(ManagerPermissions.DISTRICT_AGENT_MANAGE)
    public Object add(DistrictAgentService.AgentAddParam param) {
        agentService.addAgent(param);
        return 1;
    }

}
