package info.batcloud.fanli.admin.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import info.batcloud.fanli.admin.permission.ManagerPermissions;
import info.batcloud.fanli.admin.permission.annotation.Permission;
import info.batcloud.fanli.core.dto.CommissionItemDTO;
import info.batcloud.fanli.core.enums.CommissionItemStatus;
import info.batcloud.fanli.core.enums.EcomPlat;
import info.batcloud.fanli.core.service.CommissionItemService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/commissionItem")
public class CommissionItemController {

    @Inject
    private CommissionItemService commissionItemService;


    @GetMapping("/search")
    public Object search(CommissionItemService.SearchParam param) {

        return commissionItemService.search(param);
    }

  /*  @GetMapping("/findById")
    public Object findById(Long id){
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        CommissionItemDTO byId = commissionItemService.findById(id);
        System.out.println(byId);
        return byId;
    }*/

    @PostMapping("/rePushDocument")
    public Object rePushDocument() {
        commissionItemService.rePushDocument();
        return 1;
    }

    @Permission(ManagerPermissions.COMMISSION_ITEM_MANAGE)
    @PutMapping("/clean")
    public Object clean(CommissionItemService.SearchParam param) {
        commissionItemService.clean(param);
        return 1;
    }

    @Permission(ManagerPermissions.COMMISSION_ITEM_MANAGE)
    @PutMapping("/status/{status}")
    public Object setStatus(@PathVariable CommissionItemStatus status, @RequestParam List<Long> ids) {
        this.commissionItemService.setStatus(status, ids);
        return 1;
    }


    @PostMapping("/fetch")
    public Object fetch(FetchForm fetchForm) {
        List<CommissionItemDTO> list = new ArrayList<>();
        List<FetchItem> itemList = JSON.parseObject(fetchForm.getItemList(), new TypeReference<List<FetchItem>>() {
        });
        for (FetchItem fetchItem : itemList) {
            CommissionItemDTO bo = commissionItemService.findOrFetchBySourceItemIdAndEcomPlat(fetchItem.getSourceItemId(), fetchItem.getEcomPlat());
            list.add(bo);
        }
        return list;
    }

    public static class FetchForm {
        private String itemList;

        public String getItemList() {
            return itemList;
        }

        public void setItemList(String itemList) {
            this.itemList = itemList;
        }
    }

    public static class FetchItem {
        private Long sourceItemId;
        private EcomPlat ecomPlat;

        public Long getSourceItemId() {
            return sourceItemId;
        }

        public void setSourceItemId(Long sourceItemId) {
            this.sourceItemId = sourceItemId;
        }

        public EcomPlat getEcomPlat() {
            return ecomPlat;
        }

        public void setEcomPlat(EcomPlat ecomPlat) {
            this.ecomPlat = ecomPlat;
        }
    }
}
