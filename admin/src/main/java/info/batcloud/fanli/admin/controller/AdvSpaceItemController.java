package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.core.service.AdvSpaceItemService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

@RestController
@RequestMapping("/api/adv-space-item")
public class AdvSpaceItemController {

    @Inject
    private AdvSpaceItemService advSpaceItemService;

    @PostMapping("/{advSpaceId}")
    public Object add(@PathVariable long advSpaceId,
                      @RequestParam List<Long> advIdList) {
        AdvSpaceItemService.AdvSpaceItemAddParam param = new AdvSpaceItemService.AdvSpaceItemAddParam();
        param.setAdvIdList(advIdList);
        param.setAdvSpaceId(advSpaceId);
        advSpaceItemService.add(param);
        return 1;
    }

    @DeleteMapping("/{id}")
    public Object delete(@PathVariable long id) {
        advSpaceItemService.deleteById(id);
        return 1;
    }

    @GetMapping("/list/{advSpaceId}")
    public Object listByAdvSpaceId(@PathVariable long advSpaceId) {
        return advSpaceItemService.findByAdvSpaceId(advSpaceId);
    }
}
