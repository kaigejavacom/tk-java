package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.core.enums.FreeChargeActivityStatus;
import info.batcloud.fanli.core.service.FreeChargeActivityService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/freeChargeActivity")
public class FreeChargeActivityController {

    @Inject
    private FreeChargeActivityService redPacketService;

    @GetMapping("/search")
    public Object search(FreeChargeActivityService.SearchParam searchParam) {
        return redPacketService.search(searchParam);
    }

    @PostMapping
    public Object save(FreeChargeActivityService.FreeChargeActivityCreateParam param) {
        redPacketService.createFreeChargeActivity(param);
        return 1;
    }

    @PutMapping("/{id}")
    public Object update(@PathVariable long id, FreeChargeActivityService.FreeChargeActivityUpdateParam param) {
        redPacketService.updateFreeChargeActivity(id, param);
        return 1;
    }

    @PutMapping("/status/{id}/{status}")
    public Object setStatus(@PathVariable long id, @PathVariable FreeChargeActivityStatus status) {
        redPacketService.setStatus(id, status);
        return 1;
    }

}
