package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.core.service.FlashSaleOrderService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

@RestController
@RequestMapping("/api/flash-sale-order")
public class FlashSaleOrderController {

    @Inject
    private FlashSaleOrderService flashSaleOrderService;

    @GetMapping("/search")
    public Object search(FlashSaleOrderService.SearchParam param) {
        return flashSaleOrderService.search(param);
    }

    @PostMapping("/settle")
    public Object settle(SettleForm form) {
        flashSaleOrderService.settle(form.getIdList());
        return true;
    }

    @DeleteMapping("/{id}")
    public Object delete(@PathVariable long id) {
        flashSaleOrderService.deleteById(id);
        return true;
    }

    public static class SettleForm {
        private List<Long> idList;

        public List<Long> getIdList() {
            return idList;
        }

        public void setIdList(List<Long> idList) {
            this.idList = idList;
        }
    }
}
