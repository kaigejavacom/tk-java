package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.admin.vo.AuthorizeVo;
import info.batcloud.fanli.core.enums.Authorize;
import info.batcloud.fanli.core.service.UserAuthorizeService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/user-authorize")
public class UserAuthorizeController {

    @Inject
    private UserAuthorizeService userAuthorizeService;

    @GetMapping("/list/{userId}")
    public Object listByUserId(@PathVariable long userId) {
        List<Authorize> authorizeList = userAuthorizeService.findByUserId(userId);
        return authorizeList.stream().map(o -> {
            AuthorizeVo av = new AuthorizeVo();
            av.setAuthorize(o);
            return av;
        }).collect(Collectors.toList());
    }

    @PostMapping()
    public Object add(long userId, Authorize authorize) {
        userAuthorizeService.addAuthorize(userId, authorize);
        return true;
    }

    @DeleteMapping("/{userId}-{authorize}")
    public Object delete(@PathVariable long userId, @PathVariable Authorize authorize) {
        userAuthorizeService.deleteAuthorize(userId, authorize);
        return true;
    }

}
