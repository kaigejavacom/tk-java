package info.batcloud.fanli.admin.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import info.batcloud.fanli.core.service.OutCatToShopcatService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

@RestController
@RequestMapping("/api/out-cat-to-shopcat")
public class OutCatToShopcatController {

    @Inject
    private OutCatToShopcatService outCatToShopcatService;

    @GetMapping("/list/shopcat/{shopcatId}")
    public Object listByShopcatId(@PathVariable long shopcatId) {
        return outCatToShopcatService.listByShopcatId(shopcatId);
    }

    @PostMapping()
    public Object add(@RequestParam String params) {
        List<OutCatToShopcatService.AddParam> addParams = JSON.parseObject(params, new TypeReference<List<OutCatToShopcatService.AddParam>>(){});
        for (OutCatToShopcatService.AddParam addParam : addParams) {
            outCatToShopcatService.addOutCatToShopcat(addParam);
        }
        return true;
    }

    @DeleteMapping("/{id}")
    public Object deleteById(@PathVariable long id) {
        outCatToShopcatService.deleteById(id);
        return true;
    }

}
