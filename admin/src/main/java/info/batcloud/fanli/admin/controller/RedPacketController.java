package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.core.enums.RedPacketStatus;
import info.batcloud.fanli.core.service.RedPacketService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/red-packet")
public class RedPacketController {

    @Inject
    private RedPacketService redPacketService;

    @GetMapping("/search")
    public Object search(RedPacketService.SearchParam searchParam) {
        return redPacketService.search(searchParam);
    }

    @PostMapping
    public Object save(RedPacketService.RedPacketCreateParam param) {
        redPacketService.createRedPacket(param);
        return 1;
    }

    @PutMapping("/{id}")
    public Object update(@PathVariable long id, RedPacketService.RedPacketUpdateParam param) {
        redPacketService.updateRedPacket(id, param);
        return 1;
    }

    @PutMapping("/status/{id}/{status}")
    public Object setStatus(@PathVariable long id, @PathVariable RedPacketStatus status) {
        redPacketService.setStatus(id, status);
        return 1;
    }

}
