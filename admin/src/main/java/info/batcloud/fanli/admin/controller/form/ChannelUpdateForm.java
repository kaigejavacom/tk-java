package info.batcloud.fanli.admin.controller.form;

public class ChannelUpdateForm {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
