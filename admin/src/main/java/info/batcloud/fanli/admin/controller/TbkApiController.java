package info.batcloud.fanli.admin.controller;

import com.taobao.api.ApiException;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.TbkDgOptimusMaterialRequest;
import com.taobao.api.request.TbkScInvitecodeGetRequest;
import com.taobao.api.request.TbkUatmFavoritesGetRequest;
import com.taobao.api.response.TbkDgOptimusMaterialResponse;
import com.taobao.api.response.TbkScInvitecodeGetResponse;
import com.taobao.api.response.TbkUatmFavoritesGetResponse;
import info.batcloud.fanli.core.constants.TaobaoClientConstants;
import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.settings.TaobaoAuthSetting;
import info.batcloud.fanli.core.settings.TaobaoUnionSetting;
import info.batcloud.fanli.core.top.TbkConfig;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/tbk-api")
public class TbkApiController {

    @Inject
    @Qualifier(TaobaoClientConstants.TAOBAO_CLIENT_ITEM_TBK)
    private TaobaoClient taobaoClient;

    @Inject
    private SystemSettingService systemSettingService;

    @Inject
    private TbkConfig tbkConfig;

    @GetMapping("/favorites")
    public Object favorites() throws ApiException {
        TbkDgOptimusMaterialRequest req = new TbkDgOptimusMaterialRequest();
        TaobaoUnionSetting unionSetting = systemSettingService.findActiveSetting(TaobaoUnionSetting.class);
        req.setAdzoneId(Long.valueOf(unionSetting.getAdzoneId()));
        req.setMaterialId(31519L);
        req.setPageSize(100L);
        long page = 1;
        List<Object> list = new ArrayList<>();
        TbkDgOptimusMaterialResponse rsp = taobaoClient.execute(req);
        TbkDgOptimusMaterialResponse.FavoritesInfo favoritesInfo = rsp.getResultList().get(0).getFavoritesInfo();
        return favoritesInfo.getFavoritesList();
    }

    @GetMapping("/favorites1")
    public Object favorites1() throws ApiException {
        TbkUatmFavoritesGetRequest req = new TbkUatmFavoritesGetRequest();

        req.setPageSize(200L);
        long page = 1;
        List<Object> list = new ArrayList<>();
        req.setFields("favorites_title,favorites_id,type");
        while (true) {
            req.setPageNo(page++);
            TbkUatmFavoritesGetResponse rsp = taobaoClient.execute(req);
            if (rsp.getResults() == null) {
                break;
            }
            list.addAll(rsp.getResults());
            if (page == 3) {
                break;
            }
        }
        return list;
    }

    @GetMapping("/invite-code")
    public Object inviteCode(@RequestParam(required = false, defaultValue = "3") long codeType,
                             @RequestParam(required = false) Long relationId) throws ApiException {
        TaobaoAuthSetting setting = systemSettingService.findActiveSetting(TaobaoAuthSetting.class);
        TbkScInvitecodeGetRequest request = new TbkScInvitecodeGetRequest();
        request.setCodeType(codeType);
        request.setRelationId(relationId);
        request.setRelationApp("common");
        TbkScInvitecodeGetResponse res = taobaoClient.execute(request, setting.getAccessToken());
        return res.getData();
    }

}
