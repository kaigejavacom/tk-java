package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.core.enums.CrowdStatus;
import info.batcloud.fanli.core.service.CrowdService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/crowd")
public class CrowdController {

    @Inject
    private CrowdService crowdService;

    @PostMapping
    public Object create(CrowdService.CrowdCreateParam param) {
        crowdService.createCrowd(param);
        return 1;
    }

    @PutMapping("/{id}")
    public Object update(@PathVariable long id, CrowdService.CrowdUpdateParam param) {
        crowdService.updateCrowd(id, param);
        return 1;
    }

    @PutMapping("/status/{id}/{status}")
    public Object setStatus(@PathVariable long id, @PathVariable CrowdStatus status) {
        crowdService.setStatus(id, status);
        return 1;
    }

    @GetMapping("/list")
    public Object list() {
        return crowdService.listAll();
    }

    @GetMapping("/list/{status}")
    public Object list(@PathVariable CrowdStatus status) {
        return crowdService.listAll();
    }
}
