package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.admin.permission.ManagerPermissions;
import info.batcloud.fanli.admin.permission.annotation.Permission;
import info.batcloud.fanli.core.service.AdvSpaceService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/adv-space")
@Permission(ManagerPermissions.ADV_MANAGE)
public class AdvSpaceController {

    @Inject
    private AdvSpaceService advSpaceService;

    @GetMapping("/search")
    public Object search(AdvSpaceService.SearchParam param) {
        return advSpaceService.search(param);
    }

    @GetMapping("/list")
    public Object list() {
        return advSpaceService.findList();
    }

    @PostMapping("")
    public Object save(AdvSpaceService.AdvSpaceAddParam param) {
        advSpaceService.saveAdvSpace(param);
        return 1;
    }

    @PutMapping("/{id}")
    public Object update(@PathVariable long id, AdvSpaceService.AdvSpaceUpdateParam param) {
        param.setId(id);
        advSpaceService.updateAdvSpace(param);
        return 1;
    }

    @PutMapping("/valid/{id}-{flag}")
    public Object valid(@PathVariable long id, @PathVariable boolean flag) {
        if(flag) {
            advSpaceService.validById(id);
        } else {
            advSpaceService.invalidById(id);
        }
        return 1;
    }

    @DeleteMapping("/{id}")
    public Object delete(@PathVariable long id) {
        advSpaceService.deleteById(id);
        return 1;
    }

    @GetMapping("/adv-list/{id}")
    public Object advList(@PathVariable long id) {
        return advSpaceService.advListByAdvSpaceId(id);
    }

}
