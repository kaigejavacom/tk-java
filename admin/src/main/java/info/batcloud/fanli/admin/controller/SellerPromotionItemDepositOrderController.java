package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.admin.permission.ManagerPermissions;
import info.batcloud.fanli.admin.permission.annotation.Permission;
import info.batcloud.fanli.core.service.SellerPromotionItemDepositOrderService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/sellerPromotionItemDepositOrder")
@Permission(ManagerPermissions.SELLER_PROMOTION_ITEM_DEPOSIT_ORDER_MANAGE)
public class SellerPromotionItemDepositOrderController {

    @Inject
    private SellerPromotionItemDepositOrderService sellerPromotionItemDepositOrderService;

    @GetMapping("/search")
    public Object search(SellerPromotionItemDepositOrderService.SearchParam param) {
        return sellerPromotionItemDepositOrderService.search(param);
    }

    @PutMapping("/verify/{id}")
    public Object verify(@PathVariable long id, SellerPromotionItemDepositOrderService.VerifyParam param) {
        sellerPromotionItemDepositOrderService.verify(id, param);
        return 1;
    }

}
