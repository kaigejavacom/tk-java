package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.core.service.TaobaoItemCatService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/taobao-item-cat")
public class TaobaoItemCatController {

    @Inject
    private TaobaoItemCatService taobaoItemCatService;

    @GetMapping("/sync")
    public Object syncFromTaobao() {
        taobaoItemCatService.syncFromTaobao();
        return 1;
    }

    @GetMapping("/search")
    public Object search(TaobaoItemCatService.SearchParam param) {
        return taobaoItemCatService.search(param);
    }

}
