package info.batcloud.pdd.sdk.domain;

import com.alibaba.fastjson.annotation.JSONField;

public class GoodsCat {

    @JSONField(name = "cat_id")
    private Long catId;

    @JSONField(name = "cat_name")
    private String catName;

    private int level;

    @JSONField(name = "parent_cat_id")
    private int parentCatId;

    public Long getCatId() {
        return catId;
    }

    public void setCatId(Long catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getParentCatId() {
        return parentCatId;
    }

    public void setParentCatId(int parentCatId) {
        this.parentCatId = parentCatId;
    }
}
