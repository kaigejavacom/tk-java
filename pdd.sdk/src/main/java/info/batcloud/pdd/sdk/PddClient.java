package info.batcloud.pdd.sdk;

public interface PddClient {

    <T extends PddResponse> T execute(PddRequest<T> request);

    <T extends PddResponse> T execute(PddRequest<T> request, String accessToken);

}
