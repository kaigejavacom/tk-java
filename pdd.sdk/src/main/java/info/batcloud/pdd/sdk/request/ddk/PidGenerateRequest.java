package info.batcloud.pdd.sdk.request.ddk;

import info.batcloud.pdd.sdk.PddRequest;
import info.batcloud.pdd.sdk.annotation.Param;
import info.batcloud.pdd.sdk.response.ddk.PidGenerateResponse;

import java.util.List;

public class PidGenerateRequest implements PddRequest<PidGenerateResponse> {

    @Param("number")
    private int number;

    @Param("p_id_name_list")
    private List<String> pidNameList;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public List<String> getPidNameList() {
        return pidNameList;
    }

    public void setPidNameList(List<String> pidNameList) {
        this.pidNameList = pidNameList;
    }

    @Override
    public String getType() {
        return "pdd.ddk.oauth.goods.pid.generate";
    }

    @Override
    public Class<PidGenerateResponse> getResponseClass() {
        return PidGenerateResponse.class;
    }
}
