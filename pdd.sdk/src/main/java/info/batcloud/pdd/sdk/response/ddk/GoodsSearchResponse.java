package info.batcloud.pdd.sdk.response.ddk;

import com.alibaba.fastjson.annotation.JSONField;
import info.batcloud.pdd.sdk.PddResponse;
import info.batcloud.pdd.sdk.domain.ddk.Goods;

import java.util.List;

public class GoodsSearchResponse extends PddResponse {

    @JSONField(name = "goods_search_response")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        @JSONField(name = "total_count")
        private int totalCount;

        @JSONField(name = "goods_list")
        private List<Goods> goodsList;

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public List<Goods> getGoodsList() {
            return goodsList;
        }

        public void setGoodsList(List<Goods> goodsList) {
            this.goodsList = goodsList;
        }
    }
}
