package info.batcloud.pdd.sdk.domain.ddk;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

public class Goods {

    @JSONField(name = "merchant_type")
    private int merchantType;

    @JSONField(name = "avg_serv")
    private long avgServ;

    @JSONField(name = "avg_lgst")
    private long avgLgst;

    @JSONField(name = "avg_desc")
    private long avgDesc;

    @JSONField(name = "opt_name")
    private String optName;

    @JSONField(name = "opt_id")
    private int optId;

    @JSONField(name = "cat_ids")
    private List<Long> catIds;

    @JSONField(name = "goods_eval_count")
    private String goodsEvalCount;

    @JSONField(name = "goods_eval_score")
    private String goodsEvalScore;

    /**
     * 千分比
     * */
    @JSONField(name = "promotion_rate")
    private float promotionRate;

    @JSONField(name = "coupon_end_time")
    private long couponEndTime;

    @JSONField(name = "coupon_start_time")
    private long couponStartTime;

    @JSONField(name = "coupon_remain_quantity")
    private long couponRemainQuantity;

    @JSONField(name = "coupon_total_quantity")
    private long couponTotalQuantity;

    @JSONField(name = "coupon_discount")
    private long couponDiscount;

    @JSONField(name = "coupon_min_order_amount")
    private long couponMinOrderAmount;

    @JSONField(name = "mall_name")
    private String mallName;

    @JSONField(name = "sold_quantity")
    private int soldQuantity;

    @JSONField(name = "goods_image_url")
    private String goodsImageUrl;

    @JSONField(name = "goods_thumbnail_url")
    private String goodsThumbnailUrl;

    @JSONField(name = "goods_name")
    private String goodsName;

    @JSONField(name = "goods_id")
    private Long goodsId;

    @JSONField(name = "min_normal_price")
    private long minNormalPrice;

    @JSONField(name = "min_group_price")
    private long minGroupPrice;
    @JSONField(name = "goods_desc")
    private String goodsDesc;

    @JSONField(name = "goods_gallery_urls")
    private List<String> goodsGalleryUrls;

    public String getGoodsDesc() {
        return goodsDesc;
    }

    public void setGoodsDesc(String goodsDesc) {
        this.goodsDesc = goodsDesc;
    }

    public List<String> getGoodsGalleryUrls() {
        return goodsGalleryUrls;
    }

    public void setGoodsGalleryUrls(List<String> goodsGalleryUrls) {
        this.goodsGalleryUrls = goodsGalleryUrls;
    }

    public long getMinNormalPrice() {
        return minNormalPrice;
    }

    public void setMinNormalPrice(long minNormalPrice) {
        this.minNormalPrice = minNormalPrice;
    }

    public long getMinGroupPrice() {
        return minGroupPrice;
    }

    public void setMinGroupPrice(long minGroupPrice) {
        this.minGroupPrice = minGroupPrice;
    }

    public int getMerchantType() {
        return merchantType;
    }

    public void setMerchantType(int merchantType) {
        this.merchantType = merchantType;
    }

    public long getAvgServ() {
        return avgServ;
    }

    public void setAvgServ(long avgServ) {
        this.avgServ = avgServ;
    }

    public long getAvgLgst() {
        return avgLgst;
    }

    public void setAvgLgst(long avgLgst) {
        this.avgLgst = avgLgst;
    }

    public long getAvgDesc() {
        return avgDesc;
    }

    public void setAvgDesc(long avgDesc) {
        this.avgDesc = avgDesc;
    }

    public String getOptName() {
        return optName;
    }

    public void setOptName(String optName) {
        this.optName = optName;
    }

    public int getOptId() {
        return optId;
    }

    public void setOptId(int optId) {
        this.optId = optId;
    }

    public List<Long> getCatIds() {
        return catIds;
    }

    public void setCatIds(List<Long> catIds) {
        this.catIds = catIds;
    }

    public String getGoodsEvalCount() {
        return goodsEvalCount;
    }

    public void setGoodsEvalCount(String goodsEvalCount) {
        this.goodsEvalCount = goodsEvalCount;
    }

    public String getGoodsEvalScore() {
        return goodsEvalScore;
    }

    public void setGoodsEvalScore(String goodsEvalScore) {
        this.goodsEvalScore = goodsEvalScore;
    }

    public float getPromotionRate() {
        return promotionRate;
    }

    public void setPromotionRate(float promotionRate) {
        this.promotionRate = promotionRate;
    }

    public long getCouponEndTime() {
        return couponEndTime;
    }

    public void setCouponEndTime(long couponEndTime) {
        this.couponEndTime = couponEndTime;
    }

    public long getCouponStartTime() {
        return couponStartTime;
    }

    public void setCouponStartTime(long couponStartTime) {
        this.couponStartTime = couponStartTime;
    }

    public long getCouponRemainQuantity() {
        return couponRemainQuantity;
    }

    public void setCouponRemainQuantity(long couponRemainQuantity) {
        this.couponRemainQuantity = couponRemainQuantity;
    }

    public long getCouponTotalQuantity() {
        return couponTotalQuantity;
    }

    public void setCouponTotalQuantity(long couponTotalQuantity) {
        this.couponTotalQuantity = couponTotalQuantity;
    }

    public long getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(long couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public long getCouponMinOrderAmount() {
        return couponMinOrderAmount;
    }

    public void setCouponMinOrderAmount(long couponMinOrderAmount) {
        this.couponMinOrderAmount = couponMinOrderAmount;
    }

    public String getMallName() {
        return mallName;
    }

    public void setMallName(String mallName) {
        this.mallName = mallName;
    }

    public int getSoldQuantity() {
        return soldQuantity;
    }

    public void setSoldQuantity(int soldQuantity) {
        this.soldQuantity = soldQuantity;
    }

    public String getGoodsImageUrl() {
        return goodsImageUrl;
    }

    public void setGoodsImageUrl(String goodsImageUrl) {
        this.goodsImageUrl = goodsImageUrl;
    }

    public String getGoodsThumbnailUrl() {
        return goodsThumbnailUrl;
    }

    public void setGoodsThumbnailUrl(String goodsThumbnailUrl) {
        this.goodsThumbnailUrl = goodsThumbnailUrl;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }
}
