package info.batcloud.pdd.sdk.request.ddk;

import info.batcloud.pdd.sdk.PddRequest;
import info.batcloud.pdd.sdk.annotation.Param;
import info.batcloud.pdd.sdk.response.ddk.OrderListIncrementGetResponse;

public class OrderListIncrementGetRequest implements PddRequest<OrderListIncrementGetResponse> {

    @Param("start_update_time")
    private long startUpdateTime;

    @Param("end_update_time")
    private long endUpdateTime;

    @Param("p_id")
    private String pid;

    @Param("page_size")
    private int pageSize;

    @Param("page")
    private int page;

    public long getStartUpdateTime() {
        return startUpdateTime;
    }

    public void setStartUpdateTime(long startUpdateTime) {
        this.startUpdateTime = startUpdateTime;
    }

    public long getEndUpdateTime() {
        return endUpdateTime;
    }

    public void setEndUpdateTime(long endUpdateTime) {
        this.endUpdateTime = endUpdateTime;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    @Override
    public String getType() {
        return "pdd.ddk.order.list.increment.get";
    }

    @Override
    public Class<OrderListIncrementGetResponse> getResponseClass() {
        return OrderListIncrementGetResponse.class;
    }
}
